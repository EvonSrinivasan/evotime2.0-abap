class /EVORA/CL_ET2_PERSIST_TCONFIRM definition
  public
  inheriting from /EVORA/CL_EA_PERSISTENCE_BASE
  create public .

public section.

  methods /EVORA/IF_EA_PERSISTENCE~CREATE
    redefinition .
  methods /EVORA/IF_EA_PERSISTENCE~DELETE
    redefinition .
  methods /EVORA/IF_EA_PERSISTENCE~INFORM_APPROVAL_BOX
    redefinition .
  methods /EVORA/IF_EA_PERSISTENCE~SELECT
    redefinition .
  methods /EVORA/IF_EA_PERSISTENCE~UPDATE
    redefinition .
  methods /EVORA/IF_EA_PERSIST_CHNGE_LOG~CREATE_CHANGE_LOG
    redefinition .
protected section.

  types:
    tyt_time_approval TYPE STANDARD TABLE OF /evora/et2_t_a00 WITH NON-UNIQUE DEFAULT KEY .
  types:
      "! type definition for a list of object keys
    BEGIN OF tys_approval_keys,
      request_guid type /evora/et2_e_request_guid,
    End of tys_approval_keys .
  types:
*        request_id TYPE /evora/et2_e_requestid,
      "! type definition for a list of object keys
    tyt_approval_keys TYPE STANDARD TABLE OF tys_approval_keys .

  methods CONVERT_TO_TABLES
    importing
      !IT_DATA type STANDARD TABLE
    exporting
      !ET_TIME_APPROVAL type TYT_TIME_APPROVAL .
  methods PAGING_LOGIC
    importing
      !IS_PAGING type /EVORA/ET2_S_PAGING
    changing
      !CT_APPROVAL_KEYS type TYT_APPROVAL_KEYS optional
      !CT_DATA type DATA optional .
  methods BUILD_RANGE_TABLE_FOR_GUIDS
    importing
      !IT_OBJECT_KEYS type /EVORA/IF_EA_OBJECT=>TYT_OBJECT_KEYS
    exporting
      !ET_GUIDS_RANGE type /EVORA/EA_TT_FILTER_LINE .
  PRIVATE SECTION.
ENDCLASS.



CLASS /EVORA/CL_ET2_PERSIST_TCONFIRM IMPLEMENTATION.


  METHOD /evora/if_ea_persistence~create.

    CLEAR rv_success.
    convert_to_tables(
      EXPORTING
        it_data      = it_create
      IMPORTING
        et_time_approval = DATA(lt_time_approval)
    ).

    CHECK lt_time_approval IS NOT INITIAL.
    TRY.
        rv_success = /evora/if_ea_persist_chnge_log~create_change_log( iv_update_indicator = /evora/if_ea_constants=>cs_db_operations-insert
                                                                       it_data             =  lt_time_approval ).
        IF rv_success = abap_false.
          RETURN.
        ENDIF.

        INSERT /evora/et2_t_a00 FROM TABLE lt_time_approval.
        rv_success = COND #( WHEN sy-subrc = 0
                             THEN /evora/if_ea_persistence~inform_approval_box(
                                    it_data = lt_time_approval
                                  )
                             ELSE abap_false ).

      CATCH cx_sy_open_sql_db INTO DATA(lo_sql_exception).
        /evora/cx_ea_api_exception=>raise( is_textid = /evora/cx_ea_api_exception=>cs_db_operation_create_failure
                                iv_var1   = |{ '/EVORA/ET2_T_A00' }|
                                 ).
    ENDTRY.

  ENDMETHOD.


  METHOD /evora/if_ea_persistence~delete.
    convert_to_tables(
      EXPORTING
        it_data      = it_delete
      IMPORTING
        et_time_approval = DATA(lt_time_approval)
    ).
    CHECK lt_time_approval IS NOT INITIAL.
    TRY.
        rv_success = /evora/if_ea_persist_chnge_log~create_change_log( iv_update_indicator = /evora/if_ea_constants=>cs_db_operations-delete
                                                                       it_data             =  lt_time_approval ).
        IF rv_success = abap_false.
          RETURN.
        ENDIF.

        DELETE /evora/et2_t_a00 FROM TABLE lt_time_approval.
        rv_success = COND #( WHEN sy-subrc = 0
                             THEN abap_true
                             ELSE abap_false ).

      CATCH cx_sy_open_sql_db INTO DATA(lo_sql_exception).
        /evora/cx_ea_api_exception=>raise( is_textid = /evora/cx_ea_api_exception=>cs_db_operation_delete_failure
                                iv_var1   = |{ '/EVORA/ET2_T_A00' }|
                                 ).
    ENDTRY.
  ENDMETHOD.


  METHOD /evora/if_ea_persistence~get_changes_of_object.

  ENDMETHOD.


  METHOD /evora/if_ea_persistence~inform_approval_box.

    DATA lr_data TYPE REF TO data.

    CLEAR rv_result.

    TRY .

        DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( iv_internal_combo_key = /evora/if_ea_constants=>cv_internal_approval_combo_key ).
        CREATE DATA lr_data TYPE HANDLE lo_line_type.
        ASSIGN lr_data->* TO FIELD-SYMBOL(<ls_approval_data>).

        DATA(lo_data_buffer) = /evora/cl_ea_service_facade=>get_data_buffer( iv_internal_combo_key = /evora/if_ea_constants=>cv_internal_approval_combo_key ).
        LOOP AT it_data ASSIGNING FIELD-SYMBOL(<ls_data>).
          ASSIGN COMPONENT /evora/if_ea_approval=>cs_field_names-request_guid OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<lv_request_guid>).
          DATA(lo_object) = lo_data_buffer->get_by_key( iv_object_key = <lv_request_guid> ).

          lo_object->get_details(
            IMPORTING
              es_data                    = <ls_approval_data>
          ).
          MOVE-CORRESPONDING <ls_data> TO <ls_approval_data>.

**          fill in internal combo key
*          ASSIGN COMPONENT /evora/if_ea_approval=>cs_field_names-internal_combo_key
*            OF STRUCTURE <ls_approval_data> TO FIELD-SYMBOL(<lv_internal_combo_key>).
*          <lv_internal_combo_key> = /evora/if_ea_persistence~gv_internal_combo_key.

          DATA(ls_object_key_info) = lo_object->update(
                is_data                  = <ls_approval_data>
            ).

          IF ls_object_key_info-operation_success = abap_false.
            RETURN.
          ENDIF.
        ENDLOOP.
        rv_result = abap_true.

      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex).
        /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_exception( io_exception = lo_ex ).
        RETURN.
    ENDTRY.

  ENDMETHOD.


  METHOD /evora/if_ea_persistence~record_changes_to_object.

  ENDMETHOD.


  METHOD /evora/if_ea_persistence~select.
    DATA: lv_select      TYPE string,
          lv_where       TYPE string,
          lv_order_by    TYPE string,
          lv_up_to_rows  TYPE i,
          lr_search      TYPE REF TO data,
          lr_line_type   TYPE REF TO data,
          lt_guids_range TYPE /evora/ea_tt_filter_line.

    FIELD-SYMBOLS <lt_data> TYPE ANY TABLE.

    REFRESH et_object_keys.
    CLEAR ev_num_rows.
    CLEAR er_data.

*    Prepare where clause
    prepare_where_clause(
      EXPORTING
        iv_internal_combo_key = /evora/if_ea_persistence~gv_internal_combo_key     " Evoapproval: Object type & Approval Scenario
        it_conditions         = it_conditions    " EvoTime: Filter conditions
        it_sorting            = it_sorting    " EvoTime: Sort Conditions
      IMPORTING
        ev_where              = lv_where
        ev_select             = lv_select
        er_search             = lr_search
        ev_order_by           = lv_order_by
        ev_success            = DATA(lv_success) "Proceed to fetch
    ).
    IF lv_success = abap_false.
      RETURN.
    ENDIF.

*    Get RTTS details
    ASSIGN lr_search->* TO FIELD-SYMBOL(<ls_search>).
    DATA(lv_keyfield) = me->gs_rtts_details-key_field.
    DATA(lo_line_type) = me->gs_rtts_details-line_type.
    CREATE DATA lr_line_type TYPE HANDLE lo_line_type.
    ASSIGN lr_line_type->* TO FIELD-SYMBOL(<ls_data>).

*   condense the order by criteria
    CONDENSE lv_order_by.

*   ok here it get's now a bit tricky, as we need to fetch all data from the connected tables
*   and respect the query values in a generic way
    IF iv_count_only = abap_true.
      SELECT COUNT( * )
        FROM ( ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
          INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
          INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr  )
          LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
                                 AND jest~inact = @abap_false )
          LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
                                   AND tj30t~estat = jest~stat
                                   AND tj30t~spras = @sy-langu )
        INTO @ev_num_rows
       WHERE (lv_where).                               "#EC CI_DYNWHERE
    ELSE.

      IF is_paging IS NOT INITIAL.
        lv_up_to_rows = iv_max_rows + is_paging-start_row + is_paging-offset.
      ELSE.
        lv_up_to_rows = iv_max_rows.
      ENDIF.
      IF lv_up_to_rows IS INITIAL.
        lv_up_to_rows = /evora/if_ea_constants=>cv_max_rows.
      ENDIF.
      IF lv_up_to_rows IS NOT INITIAL.
        SELECT /evora/et2_t_a00~request_guid AS object_key
           FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
             INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
             INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
             LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
                                   AND jest~inact = @abap_false )
             LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
                                   AND tj30t~estat = jest~stat
                                   AND tj30t~spras = @sy-langu )
             UP TO @lv_up_to_rows ROWS
             INTO CORRESPONDING FIELDS OF TABLE @et_object_keys
             WHERE (lv_where)
             ORDER BY (lv_order_by).                  "#EC CI_DYNWHERE.

      ELSE.
        SELECT /evora/et2_t_a00~request_guid AS object_key
         FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
           INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
           INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
           LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
                                 AND jest~inact = @abap_false )
           LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
                                 AND tj30t~estat = jest~stat
                                 AND tj30t~spras = @sy-langu )
           UP TO @lv_up_to_rows ROWS
           INTO CORRESPONDING FIELDS OF TABLE @et_object_keys
           WHERE (lv_where)
           ORDER BY (lv_order_by).                    "#EC CI_DYNWHERE.
      ENDIF.
      IF is_paging IS NOT INITIAL.
        /evora/if_ea_persistence~paging_logic(
          EXPORTING
            is_paging        = is_paging
          CHANGING
            ct_object_keys   = et_object_keys
        ).
      ENDIF.
      IF et_object_keys IS NOT INITIAL.
        build_range_table_for_guids(
          EXPORTING
            it_object_keys = et_object_keys
          IMPORTING
            et_guids_range = lt_guids_range
        ).
      ENDIF.
      IF er_data IS REQUESTED.
        DATA(lo_table_type) = gs_rtts_details-table_type.
        CREATE DATA er_data TYPE HANDLE lo_table_type.
**   early build of the result table
        ASSIGN er_data->* TO <lt_data>.
        CLEAR <lt_data>.

        IF et_object_keys IS NOT INITIAL.
          SELECT (lv_select)
           FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
             INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
             INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
             LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
                                   AND jest~inact = @abap_false )
             LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
                                   AND tj30t~estat = jest~stat
                                   AND tj30t~spras = @sy-langu )
             INTO CORRESPONDING FIELDS OF TABLE @<lt_data>
             WHERE /evora/et2_t_a00~request_guid IN @lt_guids_range
             ORDER BY (lv_order_by).
        ENDIF.
      ELSE.
        IF et_object_keys IS NOT INITIAL.
          SELECT (lv_select)
           FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
             INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
             INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
             LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
                                   AND jest~inact = @abap_false )
             LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
                                   AND tj30t~estat = jest~stat
                                   AND tj30t~spras = @sy-langu )
             INTO CORRESPONDING FIELDS OF TABLE @<lt_data>
             WHERE /evora/et2_t_a00~request_guid IN @lt_guids_range
             ORDER BY (lv_order_by).
        ENDIF.
*          SELECT (lv_select)
*            FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
*              INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
*              INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
*              LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
*                                          AND jest~inact = @abap_false )
*              LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
*                                            AND tj30t~estat = jest~stat
*                                            AND tj30t~spras = @sy-langu )
*            INTO CORRESPONDING FIELDS OF TABLE @<lt_data>
*           WHERE (lv_where)
*           ORDER BY (lv_order_by).                     "#EC CI_DYNWHERE
      ENDIF.
    ENDIF.
    /evora/cl_ea_utilities=>delete_dead_codes( ).
*        IF et_object_keys IS REQUESTED.
*          LOOP AT <lt_data> ASSIGNING <ls_data>.
*            ASSIGN COMPONENT lv_keyfield OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<lv_object_key>).
**            INSERT INITIAL LINE INTO TABLE et_object_keys ASSIGNING FIELD-SYMBOL(<ls_object_keys>).
**            <ls_object_keys>-object_key = <lv_object_key>.
*            INSERT <lv_object_key> INTO TABLE et_object_keys.
*          ENDLOOP.
*        ENDIF.
*    ELSE.
*      IF lv_up_to_rows IS NOT INITIAL.
*        SELECT /evora/et2_t_a00~request_guid AS object_key
*           FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
*             INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
*             INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
*             LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
*                                   AND jest~inact = @abap_false )
*             LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
*                                   AND tj30t~estat = jest~stat
*                                   AND tj30t~spras = @sy-langu )
*             UP TO @lv_up_to_rows ROWS
*             INTO CORRESPONDING FIELDS OF TABLE @et_object_keys
*             WHERE (lv_where)
*             ORDER BY (lv_order_by).                  "#EC CI_DYNWHERE.
*
*        IF is_paging IS NOT INITIAL.
*          /evora/if_ea_persistence~paging_logic(
*            EXPORTING
*              is_paging        = is_paging
*            CHANGING
*              ct_object_keys = et_object_keys
*          ).
*        ENDIF.
*        IF et_object_keys IS NOT INITIAL.
*          SELECT (lv_select)
*           FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
*             INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
*             INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
*             LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
*                                   AND jest~inact = @abap_false )
*             LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
*                                   AND tj30t~estat = jest~stat
*                                   AND tj30t~spras = @sy-langu )
*             INTO CORRESPONDING FIELDS OF TABLE @<lt_data>
*             FOR ALL ENTRIES IN @et_object_keys
*             WHERE /evora/et2_t_a00~request_guid = @et_object_keys-object_key.
*        ENDIF.
*
*
*      ELSE.
*        SELECT /evora/et2_t_a00~request_guid AS /evora/et2_t_a00
*          FROM ( ( /evora/et2_t_a00 AS /evora/et2_t_a00
*            INNER JOIN /evora/ea_t_a000 AS /evora/ea_t_a000 ON /evora/ea_t_a000~request_guid = /evora/et2_t_a00~request_guid
*            INNER JOIN jsto AS jsto ON jsto~objnr = /evora/ea_t_a000~objnr
*            LEFT OUTER JOIN jest AS jest ON jest~objnr = /evora/ea_t_a000~objnr
*                                        AND jest~inact = @abap_false )
*            LEFT OUTER JOIN tj30t AS tj30t ON tj30t~stsma = jsto~stsma "#EC CI_BUFFJOIN
*                                          AND tj30t~estat = jest~stat
*                                          AND tj30t~spras = @sy-langu )
*          INTO TABLE @et_object_keys
*         WHERE (lv_where)
*         ORDER BY (lv_order_by).                       "#EC CI_DYNWHERE
*      ENDIF.
*    ENDIF.
*  ENDIF.

  ENDMETHOD.


  METHOD /evora/if_ea_persistence~update.

    convert_to_tables(
      EXPORTING
        it_data      = it_update
      IMPORTING
        et_time_approval = DATA(lt_time_approval)
    ).

    CHECK lt_time_approval IS NOT INITIAL.
    TRY.
        rv_success = /evora/if_ea_persist_chnge_log~create_change_log( iv_update_indicator = /evora/if_ea_constants=>cs_db_operations-update
                                                                       it_data             =  lt_time_approval ).
        IF rv_success = abap_false.
          RETURN.
        ENDIF.

        MODIFY /evora/et2_t_a00 FROM TABLE lt_time_approval.
        rv_success = cond #( WHEN sy-subrc = 0
                             THEN /evora/if_ea_persistence~inform_approval_box( it_data = lt_time_approval )
                             ELSE abap_false ).

      CATCH cx_sy_open_sql_db INTO DATA(lo_sql_exception).
        /evora/cx_ea_api_exception=>raise( is_textid = /evora/cx_ea_api_exception=>cs_db_operation_update_failure
                      iv_var1   = |{ '/EVORA/ET2_T_A00' }|
                       ).
    ENDTRY.
  ENDMETHOD.


  METHOD /evora/if_ea_persist_chnge_log~create_change_log.

    DATA: lt_change       TYPE STANDARD TABLE OF cdtxt,
          lt_conditions   TYPE /evora/ea_tt_filter_cond,
          lt_new_data     TYPE STANDARD TABLE OF /evora/yet2_t_a00,
          lt_old_data     TYPE STANDARD TABLE OF /evora/yet2_t_a00,
          lt_all_new_data TYPE STANDARD TABLE OF /evora/yet2_t_a00,
          lt_all_old_data TYPE STANDARD TABLE OF /evora/yet2_t_a00,
          ls_change       TYPE cdtxt,
          lr_data         TYPE REF TO data,
          lv_objectid     TYPE cdobjectv.

    FIELD-SYMBOLS <lt_old_data> TYPE ANY TABLE.

    CLEAR rv_success.
    MOVE-CORRESPONDING it_data TO lt_all_new_data.

    IF iv_update_indicator NE /evora/if_ea_constants=>cs_db_operations-insert.
*      get old data to update the logs accordingly
      get_old_data(
        EXPORTING
          it_data               = lt_all_new_data
          iv_internal_combo_key = /evora/if_ea_persistence~gv_internal_combo_key    " EvoApproval: Business Scenario & Object Type Key Combination
        IMPORTING
          et_data               = lt_all_old_data
      ).
    ENDIF.

*    check whether data is changed to update change documents
    IF is_data_changed(
       CHANGING
         ct_new_data     = lt_all_new_data
         ct_old_data     = lt_all_old_data
     ) = abap_false.
      rv_success = abap_true.
      RETURN.
    ENDIF.

    LOOP AT lt_all_new_data ASSIGNING FIELD-SYMBOL(<ls_new_data>).
      lt_new_data = lt_all_new_data.
      DELETE lt_new_data WHERE request_guid <> <ls_new_data>-request_guid.

      lt_old_data = lt_all_old_data.
      DELETE lt_old_data WHERE request_guid <> <ls_new_data>-request_guid.
* *Populate change tables
      REFRESH lt_change.
      CLEAR ls_change.
      ls_change-teilobjid = <ls_new_data>-request_guid.
      ls_change-textart = 'TCNF'.   "Time Confirmation
      ls_change-textspr = sy-langu.
      ls_change-updkz = iv_update_indicator.
      APPEND ls_change TO lt_change.
      lv_objectid  = <ls_new_data>-request_guid.

*      call the change log fm for logging
      CALL FUNCTION '/EVORA/ET2_CHLG_WRITE_DOCUMENT'
        EXPORTING
          objectid                = lv_objectid
          tcode                   = sy-tcode
          utime                   = sy-uzeit
          udate                   = sy-datum
          username                = sy-uname
          object_change_indicator = iv_update_indicator
          upd_icdtxt_et2_chlg     = iv_update_indicator
          upd_evora_et2_t_a00     = iv_update_indicator
        TABLES
          icdtxt_et2_chlg         = lt_change
          xevora_et2_t_a00        = lt_new_data
          yevora_et2_t_a00        = lt_old_data.
    ENDLOOP.
    rv_success = abap_true.

  ENDMETHOD.


  METHOD build_range_table_for_guids.
    REFRESH: et_guids_range.
    LOOP AT it_object_keys ASSIGNING FIELD-SYMBOL(<ls_object_keys>).
      APPEND INITIAL LINE TO et_guids_range ASSIGNING FIELD-SYMBOL(<ls_guids_range>).
      <ls_guids_range>-sign   = /evora/if_et2_constants=>cs_range_sign-including.
      <ls_guids_range>-option = /evora/if_et2_constants=>cs_range_option-equals.
      <ls_guids_range>-low    = <ls_object_keys>-object_key.
    ENDLOOP.
  ENDMETHOD.


  METHOD convert_to_tables.
    MOVE-CORRESPONDING it_data TO et_time_approval.
    LOOP AT et_time_approval ASSIGNING FIELD-SYMBOL(<ls_time_approval>).
      <ls_time_approval>-mandt = sy-mandt.
    ENDLOOP.
  ENDMETHOD.


  METHOD paging_logic.
    DATA lv_pos TYPE i.
    DATA lv_start TYPE i.
    FIELD-SYMBOLS: <lt_data> type ANY TABLE.

    IF ct_approval_keys IS SUPPLIED.

*     calculate the cut off line
      lv_pos = is_paging-start_row - 1 + is_paging-offset.

      IF lv_pos > 0 AND lv_pos <= lines( ct_approval_keys ).
        DELETE ct_approval_keys FROM 1 TO lv_pos.
      ELSE.
*       we do not have sufficent rows - make a clean result
        CLEAR ct_approval_keys.
      ENDIF.

    ENDIF.

    IF ct_data IS SUPPLIED.
*      data lv_guid TYPE fieldname VALUE 'REQUEST_GUID'.
*      ASSIGN ct_data to <lt_data>.
*      delete <lt_data> WHERE (lv_guid) =
*     calculate the cut off line
      lv_pos = is_paging-start_row - 1 + is_paging-offset.

      IF lv_pos > 0 AND lv_pos <= lines( ct_approval_keys ).
        DELETE ct_approval_keys FROM 1 TO lv_pos.
      ELSE.
*       we do not have sufficent rows - make a clean result
        CLEAR ct_approval_keys.
      ENDIF.

    ENDIF.
  ENDMETHOD.
ENDCLASS.
