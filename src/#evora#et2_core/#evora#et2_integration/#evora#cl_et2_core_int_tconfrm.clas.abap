class /EVORA/CL_ET2_CORE_INT_TCONFRM definition
  public
  inheriting from /EVORA/CL_EA_CORE_INT_BASE
  create public .

public section.

  methods /EVORA/IF_EA_CORE_INTEGRATION~PROCESS_MESSAGE
    redefinition .
  methods /EVORA/IF_EA_CORE_INTEGRATION~PUSH_DATA_TO_OUTBOUND
    redefinition .
protected section.

  methods READ_DATA_FROM_MESSAGE
    redefinition .
  methods PROCESS_INBOUND_MAPPING
    redefinition .
private section.
ENDCLASS.



CLASS /EVORA/CL_ET2_CORE_INT_TCONFRM IMPLEMENTATION.


  METHOD /evora/if_ea_core_integration~process_message.

    DATA: lt_conditions         TYPE /evora/ea_tt_filter_cond,
          lv_message            TYPE bapiret2-message,
          lr_data               TYPE REF TO data,
          ls_integration_status LIKE LINE OF rt_integration_status.

    FIELD-SYMBOLS: <lt_data>        TYPE ANY TABLE,
                   <ls_target_data> TYPE any.

    REFRESH: rt_integration_status.

    READ TABLE it_xml_messages INTO DATA(ls_xml_message) INDEX 1.
    IF sy-subrc = 0.
*    Prepare the target line type
      TRY .
          DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( iv_internal_combo_key = ls_xml_message-internal_combo_key ).
          DATA(lv_key_field) = /evora/cl_ea_rtts_service=>get_primary_key_field_name( iv_internal_combo_key = ls_xml_message-internal_combo_key ).
          DATA(lt_components) = lo_line_type->get_components( ).
          CREATE DATA lr_data TYPE HANDLE lo_line_type.
          ASSIGN lr_data->* TO <ls_target_data>.
        CATCH cx_sy_ref_is_initial INTO DATA(lo_ex_ref_initial).
          /evora/cx_ea_api_exception=>raise( is_textid = /evora/cx_ea_api_exception=>cs_type_creation_failure
                         iv_var1   = |{ ls_xml_message-internal_combo_key }|
                          ).
      ENDTRY.

*    Get the mapping details for the object type
      DATA(lt_source_to_target_mapping) =
        go_customizing->get_source_target_mapping(
          iv_internal_combo_key = ls_xml_message-internal_combo_key
      ).
      IF lt_source_to_target_mapping IS INITIAL.
        MESSAGE e011(/evora/ea_msg_cls) INTO lv_message.
        /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_sy_msg( ).
        RETURN.
      ENDIF.
      SORT lt_source_to_target_mapping BY source_field.

    ENDIF.

*    For EvoTime it's always new data; so always create new orecord
    LOOP AT it_xml_messages ASSIGNING FIELD-SYMBOL(<ls_message>).
      TRY.
          DATA lo_object_instance TYPE REF TO /evora/if_ea_object.
          lo_object_instance = /evora/cl_ea_service_facade=>get_data_buffer( iv_internal_combo_key = <ls_message>-internal_combo_key )->create_object( ).
        CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex).
*           hard return at this stage as if the object creation fails there is a general issue
          /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_exception( lo_ex ).
          RETURN.
      ENDTRY.
      ls_integration_status-operation_performed = /evora/if_ea_constants=>cs_modification_status-created.

*     distinguish the cases we need to run
*      as per standard, it's always create
      CASE ls_integration_status-operation_performed.
        WHEN /evora/if_ea_constants=>cs_modification_status-created.
          handle_inbound_create(
            EXPORTING
              is_message            = <ls_message>
              io_object_instance    = lo_object_instance
              it_field_mapping      = lt_source_to_target_mapping
            CHANGING
              cs_data               = <ls_target_data>
              cs_integration_status = ls_integration_status
          ).
        WHEN OTHERS.
      ENDCASE.

      TRY .
          ls_integration_status-internal_combo_key = <ls_message>-internal_combo_key.
          ls_integration_status-external_reference_value = <ls_message>-source_reference_value.
          INSERT ls_integration_status INTO TABLE rt_integration_status.

        CATCH cx_sy_itab_duplicate_key .
          /evora/cx_ea_api_exception=>raise(
            is_textid = /evora/cx_ea_api_exception=>cs_duplicate_key
            iv_var1   = |{ ls_integration_status-internal_combo_key }|
            iv_var2   = |{ ls_integration_status-external_reference_value }|
          ).
      ENDTRY.
    ENDLOOP.

  ENDMETHOD.


  METHOD /EVORA/IF_EA_CORE_INTEGRATION~PUSH_DATA_TO_OUTBOUND.

*    DATA lr_data TYPE REF TO data.
*
**    Get field mappings
*    DATA(lt_field_mapping) =
*      /evora/cl_ea_service_facade=>get_int_customizing( )->get_source_target_mapping( iv_internal_combo_key ).
*
*    ASSIGN ir_data->* TO FIELD-SYMBOL(<ls_data>).
*
*    DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( iv_internal_combo_key ).
*    CREATE DATA ir_line TYPE HANDLE lo_line_type.
*    ASSIGN lr_line->* TO FIELD-SYMBOL(<ls_target_data>).

  ENDMETHOD.


  METHOD process_inbound_mapping.

    LOOP AT it_field_mapping ASSIGNING FIELD-SYMBOL(<ls_source_to_table_mapping>)
      WHERE source_field = iv_source_field.

      ASSIGN COMPONENT <ls_source_to_table_mapping>-target_field OF STRUCTURE cs_data TO FIELD-SYMBOL(<lv_target_data>).
      IF <lv_target_data> IS ASSIGNED.
        IF is_persisted_data IS SUPPLIED AND is_persisted_data IS NOT INITIAL.
          ASSIGN COMPONENT <ls_source_to_table_mapping>-target_field OF STRUCTURE is_persisted_data TO FIELD-SYMBOL(<lv_db_data>).
          IF sy-subrc <> 0.
            ev_data_changed = abap_true.
          ELSE.
            IF <lv_db_data> <> iv_source_value.
              ev_data_changed = abap_true.
            ENDIF.
          ENDIF.
        ELSE.
          ev_data_changed = abap_true.
        ENDIF.

*                           special handling required for date fields as they're in ISO format in the XML
        DESCRIBE FIELD <lv_target_data> TYPE DATA(lv_field_type).
        IF lv_field_type = 'D' AND iv_source_value IS NOT INITIAL.
          <lv_target_data> = |{ iv_source_value+0(4) }| & |{ iv_source_value+5(2) }| & |{ iv_source_value+8(2) }|.
        ELSEIF lv_field_type = 'T' AND iv_source_value IS NOT INITIAL.
          <lv_target_data> = |{ iv_source_value+0(2) }| & |{ iv_source_value+3(2) }| & |{ iv_source_value+6(2) }|.
        ELSE.
          <lv_target_data> = iv_source_value.
        ENDIF.
      ENDIF.
    ENDLOOP.
    UNASSIGN <lv_target_data>.
  ENDMETHOD.


  METHOD read_data_from_message.

    TRY.
        super->read_data_from_message(
          EXPORTING
            is_message        = is_message
            it_field_mapping  = it_field_mapping
            is_persisted_data = is_persisted_data
          IMPORTING
            es_data           = es_data
            ev_data_changed   = ev_data_changed
            ).
      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex).
        /evora/cx_ea_api_exception=>raise( is_textid = /evora/cx_ea_api_exception=>cs_xml_parsing_failure ).
    ENDTRY.
*
*    ASSIGN COMPONENT /evora/if_et2_constants=>cs_field_names-effort OF STRUCTURE es_data TO FIELD-SYMBOL(<lv_effort>).
*    ASSIGN COMPONENT /evora/if_et2_constants=>cs_field_names-effort_unit OF STRUCTURE es_data TO FIELD-SYMBOL(<lv_effort_unit>).
*    /evora/cl_ea_utilities=>convert_duration_in_hrs_min(
*      EXPORTING
*       iv_effort      = <lv_effort>
*       iv_effort_unit = <lv_effort_unit>
*      IMPORTING
*       ev_effort      = <lv_effort>
*       ev_effort_unit = <lv_effort_unit>
*    ).

  ENDMETHOD.
ENDCLASS.
