*"* use this source file for your ABAP unit test classes
CLASS ltd_core_int_manager DEFINITION DEFERRED.
CLASS /evora/cl_et2_core_int_manager DEFINITION LOCAL FRIENDS ltd_core_int_manager .

CLASS ltd_int_customizing DEFINITION FOR TESTING.

  PUBLIC SECTION.

    INTERFACES /evora/if_ea_int_customizing PARTIALLY IMPLEMENTED.

    CLASS-DATA:
      gt_int_handler    TYPE /evora/if_ea_int_customizing=>ltyt_handler_config,
      gt_int_manager    TYPE /evora/if_ea_int_customizing=>ltyt_int_manager,
      gt_field_mappoing TYPE /evora/if_ea_int_customizing=>ltyt_field_mapping,
      gt_system_mapping TYPE /evora/if_ea_int_customizing=>ltyt_system_mapping.

    METHODS set_handler_config
      IMPORTING
        !it_int_handler_config TYPE /evora/if_ea_int_customizing=>ltyt_handler_config. .

    METHODS set_int_manager
      IMPORTING
        !it_int_manager TYPE /evora/if_ea_int_customizing=>ltyt_int_manager.

    METHODS set_int_field_mapping
      IMPORTING
        !it_int_field_mapping TYPE /evora/if_ea_int_customizing=>ltyt_field_mapping.

    METHODS set_system_mapping
      IMPORTING
        !it_system_mapping TYPE /evora/if_ea_int_customizing=>ltyt_system_mapping.


  PROTECTED SECTION.

  PRIVATE SECTION.

ENDCLASS.

CLASS ltd_int_customizing IMPLEMENTATION.

  METHOD set_handler_config.

    gt_int_handler = it_int_handler_config.

  ENDMETHOD.

  METHOD set_int_manager.

    gt_int_manager = it_int_manager.

  ENDMETHOD.

  METHOD set_int_field_mapping.

    gt_field_mappoing = it_int_field_mapping.

  ENDMETHOD.

  METHOD set_system_mapping.

    gt_system_mapping = it_system_mapping.

  ENDMETHOD.

  METHOD /evora/if_ea_int_customizing~get_integration_handler_info.

    rs_integration_handler_info = gt_int_handler[ internal_combo_key = iv_internal_combo_key ].

  ENDMETHOD.


  METHOD /evora/if_ea_int_customizing~get_integration_manager.

    ASSIGN gt_int_manager[ internal_combo_key = iv_internal_combo_key ] TO FIELD-SYMBOL(<ls_int_manager>).
    IF <ls_int_manager> IS ASSIGNED.
      rv_classname = <ls_int_manager>-integration_manager.
    ENDIF.

  ENDMETHOD.


  METHOD /evora/if_ea_int_customizing~get_source_target_mapping.

    rt_source_to_target_mapping = gt_field_mappoing.

  ENDMETHOD.

  METHOD /evora/if_ea_int_customizing~get_source_system.

    IF iv_logical_system IS SUPPLIED.

      ASSIGN gt_system_mapping[ internal_combo_key    = iv_internal_combo_key
                                source_logical_system = iv_logical_system ] TO FIELD-SYMBOL(<ls_system_mapping>).

    ELSE.
      ASSIGN gt_system_mapping[ internal_combo_key = iv_internal_combo_key
                                target_system      = iv_target_system ] TO <ls_system_mapping>.
    ENDIF.

    rv_source_system = COND #( WHEN <ls_system_mapping> IS ASSIGNED
                               THEN <ls_system_mapping>-source_system ).

  ENDMETHOD.

ENDCLASS.

CLASS ltd_core_int_manager DEFINITION FOR TESTING
                           DURATION MEDIUM
                           RISK LEVEL HARMLESS.

  PUBLIC SECTION.

    INTERFACES /evora/if_ea_core_int_manager PARTIALLY IMPLEMENTED.

  PROTECTED SECTION.

  PRIVATE SECTION.

    DATA: go_cut         TYPE REF TO /evora/if_ea_core_int_manager,  "class under test
          go_int_manager TYPE REF TO /evora/cl_et2_core_int_manager,
          go_int_config  TYPE REF TO ltd_int_customizing.

    "!  framework method to setup a test case
    METHODS: setup.

    "!  helper method to inject integration handler config
    METHODS inject_handler_config
      IMPORTING
        it_int_handler_config TYPE /evora/if_ea_int_customizing=>ltyt_handler_config.

    "!  helper method to inject integration manager config
    METHODS inject_manager_config
      IMPORTING
        it_int_manager_config TYPE /evora/if_ea_int_customizing=>ltyt_int_manager.

    "!  helper method to inject field mapping
    METHODS inject_field_mapping_config
      IMPORTING
        it_field_mapping TYPE /evora/if_ea_int_customizing=>ltyt_field_mapping.

    "!  helper method to inject system mapping
    METHODS inject_system_mapping_config
      IMPORTING
        it_system_mapping TYPE /evora/if_ea_int_customizing=>ltyt_system_mapping.

    "!  method to prepare xml data
    METHODS prepare_xml_data IMPORTING iv_internal_combo_key TYPE /evora/ea_e_internal_combo_key
                             RETURNING
                                       VALUE(rt_messages)    TYPE /evora/ea_tt_core_xml_messages.

    "!  test method to get processing type
    METHODS get_processing_type FOR TESTING.

    "!  test method to get integration handler
    METHODS get_integration_handler FOR TESTING.

    "!  test method to integrate objects inbound
    METHODS integrate_objects_inbound FOR TESTING.

    "!  test method to integrate objects inbound
    METHODS integrate_objects_outbound FOR TESTING.

    "!  test method to push data out
    METHODS push_messages FOR TESTING.

    "!  test method to get instance
    METHODS get_instance FOR TESTING.

    "!  test method to get RFC details
    METHODS get_rfc FOR TESTING.

ENDCLASS.

CLASS ltd_core_int_manager IMPLEMENTATION.

  METHOD setup.

    go_int_config = NEW ltd_int_customizing( ).
    inject_manager_config( it_int_manager_config = VALUE #( ( internal_combo_key  = 'EVOTIME-TCONFIRM'
                                                              integration_manager = '/EVORA/CL_ET2_CORE_INT_MANAGER' ) ) ).

    inject_system_mapping_config( it_system_mapping = VALUE #( ( internal_combo_key    = 'EVOTIME-TCONFIRM'
                                                                 source_logical_system = 'LOCAL'
                                                                 source_system         = 'SAP-WM'
                                                                 target_logical_system = 'LOCAL'
                                                                 target_system         = 'ECC'  ) ) ).

    go_cut = NEW /evora/cl_et2_core_int_manager( iv_internal_combo_key = 'EVOTIME-TCONFIRM' ).

  ENDMETHOD.


  METHOD inject_manager_config.

    go_int_config->set_int_manager( it_int_manager = it_int_manager_config ).
    /evora/th_ea_service_injector=>set_int_customizing( io_customizing = NEW ltd_int_customizing( ) ).

  ENDMETHOD.


  METHOD inject_handler_config.

    go_int_config->set_handler_config( it_int_handler_config = it_int_handler_config ).
    /evora/th_ea_service_injector=>set_int_customizing( io_customizing = NEW ltd_int_customizing( ) ).

  ENDMETHOD.

  METHOD inject_field_mapping_config.

    go_int_config->set_int_field_mapping( it_int_field_mapping = it_field_mapping ).
    /evora/th_ea_service_injector=>set_int_customizing( io_customizing = NEW ltd_int_customizing( ) ).

  ENDMETHOD.

  METHOD inject_system_mapping_config.

    go_int_config->set_system_mapping( it_system_mapping = it_system_mapping ).
    /evora/th_ea_service_injector=>set_int_customizing( io_customizing = NEW ltd_int_customizing( ) ).

  ENDMETHOD.

  METHOD get_instance.

*    invalid integration manager instance for invalid data
    cl_abap_unit_assert=>assert_not_bound(
        act              = /evora/cl_ea_core_int_manager=>get_instance( iv_internal_combo_key = 'DUMMY' )
        msg              = |Invalid instance of integration manager for invalid data|
        level            = if_aunit_constants=>fatal
        quit             = if_aunit_constants=>method
    ).


    cl_abap_unit_assert=>assert_bound(
        act              = /evora/cl_ea_core_int_manager=>get_instance( iv_internal_combo_key = 'EVOTIME-TCONFIRM' )
        msg              = |Instance of Integration manager is not found|
        level            = if_aunit_constants=>fatal
        quit             = if_aunit_constants=>method
    ).


  ENDMETHOD.

  METHOD get_processing_type.

    TRY.

        cl_abap_unit_assert=>assert_initial(
            act              = go_cut->get_handler_processing_type( iv_source_system = 'SAP-WM' )
            msg              = |Invalid Integration handler processing type|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

*    proper integration handler config
*    no data expected because, handler data is not yet fetched form the db
        inject_handler_config(
            it_int_handler_config = VALUE #( ( internal_combo_key  = 'EVOTIME-TCONFIRM'
                                               integration_handler = '/EVORA/CL_ET2_CORE_INT_TCONFRM'
                                               processing_type     = 'S'
                                               source_system       = 'SAP-WM' ) ) ).
        cl_abap_unit_assert=>assert_equals(
            act              = go_cut->get_handler_processing_type( iv_source_system = 'SAP-WM' )
            exp              = ' '
            msg              = |Invalid Integration handler processing type|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

        go_cut->get_integration_handler(
          EXPORTING
            iv_system_name             = 'SAP-WM'    " EvoApproval: Source/Target System Name
            iv_integration_type        = 'INBOUND'    " EvoApproval: Integration type (Inbound/Outbound)
        ).

        cl_abap_unit_assert=>assert_equals(
            act              = go_cut->get_handler_processing_type( iv_source_system = 'SAP-WM' )
            exp              = 'S'
            msg              = |Invalid Integration handler processing type|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

      CATCH /evora/cx_ea_api_exception.    "

        cl_abap_unit_assert=>fail(
            msg              = |Failed while getting Integration handler processing type|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

    ENDTRY.

  ENDMETHOD.

  METHOD get_integration_handler.

    TRY.
*    no integration handler configuration
        inject_handler_config(
            it_int_handler_config = VALUE #( ( internal_combo_key  = 'EVOTIME-TCONFIRM'
                                               integration_handler = ' '
                                               processing_type     = 'S'
                                               source_system       = 'SAP-WM' ) ) ).

        TRY.
            DATA(lo_integration_handler) = go_cut->get_integration_handler(
                                         iv_system_name             = 'SAP-WM'
                                         iv_integration_type        = 'INBOUND'
                                     ).
            cl_abap_unit_assert=>assert_not_bound(
                act              = lo_integration_handler
                msg              = |Invalid Integration handler instance|
                level            = if_aunit_constants=>fatal
                quit             = if_aunit_constants=>method
            ).
          CATCH /evora/cx_ea_api_exception.
*            if exception is raised, then it's good
        ENDTRY.

*    proper integration handler config
        inject_handler_config(
            it_int_handler_config = VALUE #( ( internal_combo_key  = 'EVOTIME-TCONFIRM'
                                               integration_handler = '/EVORA/CL_ET2_CORE_INT_TCONFRM'
                                               processing_type     = 'S'
                                               source_system       = 'SAP-WM' ) ) ).

        lo_integration_handler = go_cut->get_integration_handler(
                                     iv_system_name             = 'SAP-WM'
                                     iv_integration_type        = 'INBOUND'
                                 ).

        cl_abap_unit_assert=>assert_bound(
            act              = lo_integration_handler
            msg              = |Failed to get Integration handler instance|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

        cl_abap_unit_assert=>assert_equals(
            act              = lo_integration_handler
            exp              = go_cut->get_integration_handler(
                                   iv_system_name             = 'SAP-WM'
                                   iv_integration_type        = 'INBOUND'
                               )
            msg              = |Failed to get correct Integration handler instance|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

      CATCH /evora/cx_ea_api_exception.  "

        cl_abap_unit_assert=>fail(
            msg              = |Failed while getting Integration handler instance|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

    ENDTRY.

  ENDMETHOD.


  METHOD integrate_objects_inbound.

    DATA(lt_xml_messages) = prepare_xml_data( iv_internal_combo_key = 'EVOTIME-TCONFIRM' ).
    TRY.
*    no handler configuration is available
        DATA(lt_integration_status) = go_cut->integrate_objects_inbound(
                                      it_messages        = lt_xml_messages
                                      iv_simulation_mode = abap_true
                                  ).

      CATCH /evora/cx_ea_api_exception.    "
*            if exception is raised, then it's good
    ENDTRY.

    TRY.
*    proper integration handler config
        inject_handler_config(
            it_int_handler_config = VALUE #( ( internal_combo_key  = 'EVOTIME-TCONFIRM'
                                               integration_handler = '/EVORA/CL_ET2_CORE_INT_TCONFRM'
                                               processing_type     = 'S'
                                               source_system       = 'SAP-WM' ) ) ).

        lt_integration_status = go_cut->integrate_objects_inbound(
                                  it_messages        = lt_xml_messages
                                  iv_simulation_mode = abap_true
                              ).
*    no field mapping configuration
        cl_abap_unit_assert=>assert_not_initial(
          EXPORTING
            act              = /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages( )
            msg              = |Failed to get the proper messages of inbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

        inject_field_mapping_config( it_field_mapping = VALUE #( ( internal_combo_key = 'EVOTIME-TCONFIRM'
                                                                   source_field       = 'ORDERID'
                                                                   target_field       = 'ORDER_ID'
                                                                   )
                                                                 ( internal_combo_key = 'EVOTIME-TCONFIRM'
                                                                   source_field       = 'OPERATION'
                                                                   target_field       = 'OPERATION'
                                                                   )
                                                                 ( internal_combo_key = 'EVOTIME-TCONFIRM'
                                                                   source_field       = 'PERSON_NO'
                                                                   target_field       = 'PERSON_NUMBER'
                                                                   ) ) ).

        lt_integration_status = go_cut->integrate_objects_inbound(
                                  it_messages        = lt_xml_messages
                                  iv_simulation_mode = abap_true
                              ).
        cl_abap_unit_assert=>assert_not_initial(
          EXPORTING
            act              = lt_integration_status
            msg              = |Failed to get the proper status of inbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

        cl_abap_unit_assert=>assert_equals(
          EXPORTING
            act              = lt_integration_status[ 1 ]-operation_status
            exp              = 'S'
            msg              = |Failed to get the proper status of inbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

      CATCH /evora/cx_ea_api_exception.    "
        cl_abap_unit_assert=>fail(
            msg              = |Failed in inbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).
    ENDTRY.

  ENDMETHOD.

  METHOD integrate_objects_outbound.

    TRY.
        go_int_manager ?= go_cut.
*        invalid records to process
        cl_abap_unit_assert=>assert_initial(
            act              = go_int_manager->gt_outbound_info
            msg              = |Invalid records to be processed|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

*        integrate objects outbound
        cl_abap_unit_assert=>assert_not_initial(
            act              = go_cut->integrate_objects_outbound(
                                   iv_target_system = 'ECC' )
            msg              = |Failed in outbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).


        cl_abap_unit_assert=>assert_not_initial(
            act              = go_int_manager->gt_outbound_info
            msg              = |Failed to get the records|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

        cl_abap_unit_assert=>assert_equals(
            act              = go_int_manager->gt_outbound_info[ 1 ]-target_system
            exp              = 'ECC'
            msg              = |Failed to get the records|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

*        need to test for inserted data
      CATCH /evora/cx_ea_api_exception.    "
        cl_abap_unit_assert=>fail(
            msg              = |Failed in outbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).
    ENDTRY.

  ENDMETHOD.

  METHOD push_messages.

    go_int_manager ?= go_cut.
    TRY.
*    no records to process
        cl_abap_unit_assert=>assert_initial(
            act              = go_cut->push_messages( )
            msg              = |Failed to push data for outbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).
*        integrate objects outbound
        cl_abap_unit_assert=>assert_not_initial(
            act              = go_cut->integrate_objects_outbound(
                                   iv_object_key    = /evora/cl_ea_service_facade=>get_data_buffer(
                                   iv_internal_combo_key = 'EVOTIME-TCONFIRM' )->create_object( )->get_key( )
*                                                        CATCH /evora/cx_ea_api_exception.  "
                                   iv_target_system = 'ECC' )
            msg              = |Failed in outbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

        cl_abap_unit_assert=>assert_not_initial(
            act              = go_cut->push_messages( )
            msg              = |Failed to push data for outbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

        cl_abap_unit_assert=>assert_initial(
            act              = go_int_manager->gt_outbound_info
            msg              = |Invalid records to be processed|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).

      CATCH /evora/cx_ea_api_exception.    "
        cl_abap_unit_assert=>fail(
            msg              = |Failed in outbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).
    ENDTRY.

  ENDMETHOD.


  METHOD get_rfc.

    TRY.
*    get RFC details
        DATA(ls_rfc_details) = go_cut->get_rfc_details(
                               iv_internal_combo_key      = 'EVOTIME-TCONFIRM'
                               iv_target_system           = 'ECC'
                           ).
*    no implementation exists and hence no result expected
        cl_abap_unit_assert=>assert_initial(
            act              = ls_rfc_details
            msg              = |Failed to push data for outbound integration|
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).
      CATCH /evora/cx_ea_api_exception.  " .
        cl_abap_unit_assert=>fail(
            msg              = |Failed to get RFC details |
            level            = if_aunit_constants=>fatal
            quit             = if_aunit_constants=>method
        ).
    ENDTRY.

  ENDMETHOD.

  METHOD prepare_xml_data.

    DATA lt_messages    TYPE /evora/ea_tt_core_xml_messages.
    DATA ls_messages    TYPE /evora/ea_s_core_xml_messages.
    DATA lr_line        TYPE REF TO data.
    DATA lv_xml_message TYPE xstring.

    TRY .
        DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( iv_internal_combo_key = 'EVOTIME-TCONFIRM' ).
      CATCH /evora/cx_ea_api_exception.
        RETURN.
    ENDTRY.
    CREATE DATA lr_line TYPE HANDLE lo_line_type.
    ASSIGN lr_line->* TO FIELD-SYMBOL(<ls_target_data>).
    ASSIGN COMPONENT 'ORDER_ID' OF STRUCTURE <ls_target_data> TO FIELD-SYMBOL(<lv_data>).
    <lv_data> =  '000000822447'.
    ASSIGN COMPONENT 'OPERATION' OF STRUCTURE <ls_target_data> TO <lv_data>.
    <lv_data> =  '0010'.

*   Generate XML from the dynamic target structure
    CALL TRANSFORMATION id
    SOURCE data = <ls_target_data>
    RESULT XML lv_xml_message.

    ls_messages-internal_combo_key = iv_internal_combo_key.
    ls_messages-logical_system     = 'SAP-WM'.
    ls_messages-source_reference_value = 'TCONFIRM  LOCAL     0000008223690010'.
    ls_messages-rawmessage         = lv_xml_message.
    APPEND ls_messages TO rt_messages.

  ENDMETHOD.

ENDCLASS.
