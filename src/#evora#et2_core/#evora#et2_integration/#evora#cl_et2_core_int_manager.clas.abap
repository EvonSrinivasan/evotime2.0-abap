CLASS /evora/cl_et2_core_int_manager DEFINITION
  PUBLIC
  INHERITING FROM /evora/cl_ea_core_int_manager
  CREATE PUBLIC .

  PUBLIC SECTION.

    METHODS /evora/if_ea_core_int_manager~integrate_objects_outbound
        REDEFINITION .
    METHODS /evora/if_ea_core_int_manager~push_messages
        REDEFINITION .
  PROTECTED SECTION.

    DATA gt_outbound_info TYPE tyt_outbound_int_info.

  PRIVATE SECTION.
ENDCLASS.



CLASS /EVORA/CL_ET2_CORE_INT_MANAGER IMPLEMENTATION.


  METHOD /evora/if_ea_core_int_manager~integrate_objects_outbound.

    DATA: ls_outbound_info TYPE tys_outbound_int_info.

    ls_outbound_info-internal_combo_key = /evora/if_ea_core_int_manager~gv_internal_combo_key.
    ls_outbound_info-object_key         = iv_object_key.
    ls_outbound_info-target_system      = iv_target_system.
    ls_outbound_info-simulation_mode    = iv_simulation_mode.
    INSERT ls_outbound_info INTO TABLE gt_outbound_info.

    IF sy-subrc = 0.
      rv_result = abap_true.
    ENDIF.

  ENDMETHOD.


  METHOD /evora/if_ea_core_int_manager~push_messages.

    DATA lr_data TYPE REF TO data.

    TYPES: ltyt_alm_return TYPE STANDARD TABLE OF bapi_alm_return.

    DATA: lt_time_confirmation   TYPE bapi_alm_timeconfirmation_t,
          lt_confirmation_return TYPE STANDARD TABLE OF bapi_alm_return,
          ls_time_confirmation   TYPE bapi_alm_timeconfirmation,
          ls_target_data         TYPE /evora/et2_s_cor_int_time_flds,
          ls_return              TYPE bapiret2.

    FIELD-SYMBOLS: <lt_table>  TYPE ANY TABLE.

    rv_result = abap_false .

    LOOP AT gt_outbound_info ASSIGNING FIELD-SYMBOL(<ls_outbound_info>).

      TRY.
          AT FIRST.

*        get line type
            DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type(
                iv_internal_combo_key = <ls_outbound_info>-internal_combo_key ).
            CREATE DATA lr_data TYPE HANDLE lo_line_type.
            ASSIGN lr_data->* TO FIELD-SYMBOL(<ls_source_data>).
*                       CATCH /evora/cx_ea_api_exception.  "

*    Get field mappings
            DATA(lt_gen_struct_field_mapping) =
              /evora/cl_ea_service_facade=>get_int_customizing( )->get_source_target_mapping(
                iv_internal_combo_key = /evora/if_ea_core_int_manager~gv_internal_combo_key ).

            DATA(lt_src_struct_field_mapping) =
              /evora/cl_ea_src_int_manager=>get_instance( )->get_int_customizing( )->get_exchange_structure_mapping(
                iv_internal_combo_key = /evora/if_ea_core_int_manager~gv_internal_combo_key ).
            IF lt_gen_struct_field_mapping IS INITIAL
              OR lt_src_struct_field_mapping IS INITIAL.
              EXIT.
            ENDIF.

          ENDAT.

          REFRESH:  lt_time_confirmation,
                    lt_confirmation_return.
          CLEAR ls_return.

          IF <ls_outbound_info>-object_key IS INITIAL.
            CONTINUE.
          ENDIF.

          DATA(lo_object_instance) = /evora/cl_ea_service_facade=>get_data_buffer(
              iv_internal_combo_key = <ls_outbound_info>-internal_combo_key )->get_by_key(
                  iv_object_key = <ls_outbound_info>-object_key ).

          lo_object_instance->get_details(
              IMPORTING
                es_data = <ls_source_data>
          ).

*          ASSIGN ir_data->* TO FIELD-SYMBOL(<ls_source_data>).

*  map the vlaues of EvoTime to generic integration structure
          LOOP AT /evora/cl_ea_service_facade=>get_int_customizing( )->get_source_target_mapping(
              iv_internal_combo_key = /evora/if_ea_core_int_manager~gv_internal_combo_key )
            ASSIGNING FIELD-SYMBOL(<ls_field_mapping>).
            ASSIGN COMPONENT <ls_field_mapping>-source_field OF STRUCTURE ls_target_data TO FIELD-SYMBOL(<lv_target_field>).
            IF sy-subrc = 0
              AND <ls_field_mapping>-integration_relevant = abap_true.
              ASSIGN COMPONENT <ls_field_mapping>-target_field OF STRUCTURE <ls_source_data>
                  TO FIELD-SYMBOL(<lv_source_field>).
              IF sy-subrc EQ 0.
                <lv_target_field> = <lv_source_field>.
              ENDIF.
            ENDIF.
          ENDLOOP.

*    map the values of generic integration to source structure and process the data
          LOOP AT lt_src_struct_field_mapping
            ASSIGNING FIELD-SYMBOL(<ls_src_struct_field_mapping>)
            WHERE mapping_class IS INITIAL.
            ASSIGN COMPONENT <ls_src_struct_field_mapping>-target_field OF STRUCTURE ls_target_data TO <lv_source_field>.
            IF sy-subrc = 0.
              ASSIGN COMPONENT <ls_src_struct_field_mapping>-source_structure_field OF STRUCTURE ls_time_confirmation
                  TO <lv_target_field>.
              IF sy-subrc EQ 0.
                <lv_target_field> = <lv_source_field>.
              ENDIF.
            ENDIF.
          ENDLOOP.
          APPEND ls_time_confirmation TO lt_time_confirmation.

*    create time confirmation
          CALL FUNCTION '/EVORA/EA_CORE_ALM_CONF_CREATE'
*      IN UPDATE TASK
            EXPORTING
              iv_testrun       = iv_simulation_mode
            IMPORTING
              es_return        = ls_return
            TABLES
              it_timetickets   = lt_time_confirmation
              et_detail_return = lt_confirmation_return.


*            get the results
*            ASSIGN lr_return_data->* TO <lt_return_data>.
          LOOP AT lt_confirmation_return ASSIGNING FIELD-SYMBOL(<ls_return_data>).
            ASSIGN COMPONENT 'CONF_NO' OF STRUCTURE <ls_return_data> TO FIELD-SYMBOL(<lv_conf_no>).
            ASSIGN COMPONENT 'CONF_CNT' OF STRUCTURE <ls_return_data> TO FIELD-SYMBOL(<lv_conf_cnt>).
            IF <lv_conf_no> IS NOT ASSIGNED
              OR <lv_conf_cnt> IS NOT ASSIGNED.
              IF <lv_conf_no> IS INITIAL.
                RETURN.
              ENDIF.
            ENDIF.

            ls_return = CORRESPONDING #( <ls_return_data> ).
            ASSIGN COMPONENT 'MESSAGE_ID' OF STRUCTURE <ls_return_data> TO FIELD-SYMBOL(<lv_data>).
            ls_return-id = <lv_data>.
            ASSIGN COMPONENT 'MESSAGE_NO' OF STRUCTURE <ls_return_data> TO <lv_data>.
            ls_return-number = <lv_data>.
            /evora/cl_ea_service_facade=>get_message_buffer( )->add_message( is_message = ls_return ).
            EXIT.
          ENDLOOP.

*            update confirmation number and counter
          ASSIGN COMPONENT /evora/if_et2_timeconfirm=>cs_field_names-confirmation OF STRUCTURE <ls_source_data> TO FIELD-SYMBOL(<lv_confirmation_number>).
          ASSIGN COMPONENT /evora/if_et2_timeconfirm=>cs_field_names-confirmation_counter OF STRUCTURE <ls_source_data> TO FIELD-SYMBOL(<lv_confirmation_counter>).
          IF <lv_confirmation_counter> IS NOT ASSIGNED
            OR <lv_conf_cnt> IS NOT ASSIGNED.
            lo_object_instance->execute_function(
                iv_function = /evora/if_et2_constants=>cs_functions-pmbookfailed ).
            RETURN.
          ENDIF.
          <lv_confirmation_counter> = <lv_conf_cnt>.

          <lv_confirmation_number> = COND /evora/et2_e_confirmation( WHEN <lv_conf_no> NE '0000000000'
                                                                     THEN <lv_conf_no>
                                                                      ).
          DATA(lv_success) = lo_object_instance->update(
                       is_data                    = <ls_source_data>
                   )-operation_success.

          lv_success = lo_object_instance->execute_function(
            iv_function = /evora/if_et2_constants=>cs_functions-pmbooked ).

          /evora/cl_ea_service_facade=>save( ).

          AT LAST.
            rv_result = abap_true.
          ENDAT.
        CATCH /evora/cx_ea_api_exception.
          /evora/cl_ea_utilities=>todo( ).
      ENDTRY.

    ENDLOOP.
    REFRESH gt_outbound_info.

  ENDMETHOD.
ENDCLASS.
