class /EVORA/CL_ET2_SRC_INT_TCONFIRM definition
  public
  inheriting from /EVORA/CL_EA_SRC_INT_BASE
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !IV_INTERNAL_COMBO_KEY type /EVORA/EA_E_SRC_INT_COMBO_KEY .

  methods /EVORA/IF_EA_SRC_INTEGRATION~GENERATE_MESSAGE
    redefinition .
protected section.
private section.
ENDCLASS.



CLASS /EVORA/CL_ET2_SRC_INT_TCONFIRM IMPLEMENTATION.


  METHOD /evora/if_ea_src_integration~generate_message.

    DATA: lt_return       TYPE bapiret2_t,
          ls_return       TYPE bapiret2,
          ls_xml_messages TYPE /evora/ea_s_src_xml_messages,
          lr_table        TYPE REF TO data,
          lr_line         TYPE REF TO data.

    FIELD-SYMBOLS <lt_data>       TYPE ANY TABLE.
    FIELD-SYMBOLS <lt_data_table> TYPE ANY TABLE.

*   ensure all other things are done
    super->/evora/if_ea_src_integration~generate_message(
      EXPORTING
        iv_source_logical_system = iv_source_logical_system
        ir_data                  = ir_data
      IMPORTING
        et_generate_message      = et_generate_message
    ).

    REFRESH et_generate_message.

    TRY.

*       obtain the mapping maintained in the customizing
        DATA(lt_field_mapping) = go_customizing->/evora/if_ea_src_customizing~get_exchange_structure_mapping(
                             iv_internal_combo_key = /evora/if_ea_src_integration~gv_internal_combo_key" iv_internal_combo_key
                         ) .
        ASSIGN ir_data->* TO <lt_data>.

        DATA(lo_table_type) = /evora/cl_ea_src_rtts_service=>get_table_type( iv_internal_combo_key = /evora/if_ea_src_integration~gv_internal_combo_key ). "iv_internal_combo_key ).
        CREATE DATA lr_table TYPE HANDLE lo_table_type.
        ASSIGN lr_table->* TO <lt_data_table>.

        DATA(lo_line_type) = /evora/cl_ea_src_rtts_service=>get_line_type( iv_internal_combo_key = /evora/if_ea_src_integration~gv_internal_combo_key ). "iv_internal_combo_key ).
        CREATE DATA lr_line TYPE HANDLE lo_line_type.
        ASSIGN lr_line->* TO FIELD-SYMBOL(<ls_target_data>).

*       Generate an XML message for each data...
        LOOP AT <lt_data> ASSIGNING FIELD-SYMBOL(<ls_data>).

*         ensure a clean state
          CLEAR <ls_target_data>.
          CLEAR ls_xml_messages.

*         Message should be generated only if the object is relevant for EvoApplication
          IF is_object_relevant(
              is_data               = <ls_data>
            ) = abap_false.
          ENDIF.

*         Map the values of the source field to the target field...
          LOOP AT lt_field_mapping ASSIGNING FIELD-SYMBOL(<ls_field_mapping>).

*           map each field
            process_field_mapping_outbound(
              EXPORTING
                is_field_mapping      = <ls_field_mapping>
                is_source_data        = <ls_data>
              CHANGING
                cs_target_data        = <ls_target_data>
            ).

          ENDLOOP.

          CHECK <ls_target_data> IS NOT INITIAL.

*         set all the common fields in the message and the message header
*         as this includes the external key we can only do this after all field mappings are done
          set_common_flds_in_msg_and_hdr(
            EXPORTING
              iv_source_logical_system = iv_source_logical_system
              is_target_data           = <ls_target_data>
              is_source_data           = <ls_data>
            CHANGING
              cs_message_data   = <ls_target_data>
              cs_message_header = ls_xml_messages
          ).

*         Generate XML from the dynamic target structure
          CALL TRANSFORMATION id
            SOURCE
              data = <ls_target_data>
            RESULT
              XML ls_xml_messages-rawmessage.

          INSERT ls_xml_messages INTO TABLE et_generate_message.
        ENDLOOP.

      CATCH cx_sy_assign_cast_illegal_cast
            cx_sy_assign_out_of_range
            cx_sy_assign_cast_unknown_type
            cx_sy_create_data_error
            cx_transformation_error.

        CALL FUNCTION 'BAPI_MESSAGE_GETDETAIL'
          EXPORTING
            id         = sy-msgid
            number     = sy-msgno
            language   = sy-langu
            textformat = 'ASC'
            message_v1 = sy-msgv1
            message_v2 = sy-msgv2
            message_v3 = sy-msgv3
            message_v4 = sy-msgv4
          IMPORTING
            return     = ls_return.

        APPEND ls_return TO lt_return.

*       Send all the messages to message buffer
        IF lt_return IS NOT INITIAL.
          /evora/cl_ea_src_int_manager=>get_instance( )->get_message_buffer( )->add_messages( it_messages = lt_return ).

        ENDIF.

        RETURN.
    ENDTRY.
  ENDMETHOD.


  METHOD constructor.

*   be a good citizen call super
    super->constructor( iv_internal_combo_key = iv_internal_combo_key ).

  ENDMETHOD.
ENDCLASS.
