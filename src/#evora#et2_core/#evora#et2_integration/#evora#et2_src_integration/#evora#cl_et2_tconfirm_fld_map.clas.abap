CLASS /evora/cl_et2_tconfirm_fld_map DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES /evora/if_ea_src_int_field_map .

    METHODS constructor
      IMPORTING
        !iv_internal_combo_key TYPE /evora/ea_e_src_int_combo_key OPTIONAL
        !iv_target_field       TYPE fieldname OPTIONAL
        !iv_source_field       TYPE fieldname OPTIONAL
        !iv_source_value       TYPE data OPTIONAL
        !is_data               TYPE data OPTIONAL .
protected section.

  class-data SR_DATA type ref to DATA .
  class-data SV_SOURCE_FIELD type FIELDNAME .
  class-data SV_TARGET_FIELD type FIELDNAME .
  class-data SV_SOURCE_VALUE type STRING .
  class-data SV_INTERNAL_COMBO_KEY type /EVORA/EA_E_SRC_INT_COMBO_KEY .

  class-methods PROVIDE_FIRST_NAME
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_LAST_NAME
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_ORDER_DESCRIPTION
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_OPERATION_DESCRIPTION
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_WORK_CENTER
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_START_TIMESTAMP
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_END_TIMESTAMP
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_EFFORT
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_ACTUAL_WORK
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_UNIT_OF_ACTUAL_WORK
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_ACTUAL_DURATION
    exporting
      !EV_TARGET_VALUE type ANY .
  class-methods PROVIDE_UNIT_OF_ACTUAL_DUR
    exporting
      !EV_TARGET_VALUE type ANY .
  PRIVATE SECTION.
ENDCLASS.



CLASS /EVORA/CL_ET2_TCONFIRM_FLD_MAP IMPLEMENTATION.


  METHOD /evora/if_ea_src_int_field_map~fill_target_integration_field.

    DATA: lo_classdescr TYPE REF TO cl_abap_classdescr.

*    get class name from the instance
    DATA(lv_classname) = cl_abap_classdescr=>get_class_name( me ).

*    determine the method name to be called
    DATA(lv_method_name) = |PROVIDE_{ sv_target_field }|.

*    validate whether classname and method exists
    lo_classdescr ?= cl_abap_typedescr=>describe_by_name( lv_classname ).
    IF line_exists( lo_classdescr->methods[ name = lv_method_name ] ) .
*      if exists, call it
      CALL METHOD (lv_classname)=>(lv_method_name)
        IMPORTING
          ev_target_value = ev_target_value.
    ENDIF.

  ENDMETHOD.


  METHOD constructor.

    /evora/cl_ea_src_rtts_service=>get_time_confirmation_type(
      IMPORTING
        eo_line_type  = DATA(lo_line_type)    " Runtime Type Services
*        eo_table_type =     " Runtime Type Services
    ).

    CREATE DATA sr_data TYPE HANDLE lo_line_type.
    ASSIGN sr_data->* TO FIELD-SYMBOL(<ls_data>).
    <ls_data> = is_data.

    sv_target_field = iv_target_field.
    sv_source_field = iv_source_field.
    sv_source_value = iv_source_value.
    sv_internal_combo_key = iv_internal_combo_key.

  ENDMETHOD.


  method PROVIDE_ACTUAL_DURATION.
    ASSIGN sr_data->* TO FIELD-SYMBOL(<ls_data>).
    ASSIGN COMPONENT 'BAPI_ALM_TIMECONFIRMATION' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<ls_tconfirm_data>).
    IF <ls_tconfirm_data> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'ACTUAL_DUR' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort>).
    IF <lv_effort> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'UN_ACT_DUR' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort_unit>).
    IF <lv_effort_unit> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    DATA(lv_effort) = conv /evora/ea_e_effort( <lv_effort> ).
    ev_target_value = conv /evora/ea_e_effort(
    COND /evora/ea_e_effort( WHEN <lv_effort_unit> = 'MIN'
                          then lv_effort / 60
                          WHEN <lv_effort_unit> = 'DAY'
                          THEN lv_effort * 24
                          WHEN <lv_effort_unit> = 'H'
                          THEN lv_effort
                          ELSE lv_effort ) ).

  endmethod.


  METHOD PROVIDE_ACTUAL_WORK.

    ASSIGN sr_data->* TO FIELD-SYMBOL(<ls_data>).
    ASSIGN COMPONENT 'BAPI_ALM_TIMECONFIRMATION' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<ls_tconfirm_data>).
    IF <ls_tconfirm_data> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'ACT_WORK' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort>).
    IF <lv_effort> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'UN_WORK' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort_unit>).
    IF <lv_effort_unit> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    DATA(lv_effort) = conv /evora/ea_e_effort( <lv_effort> ).
    ev_target_value = conv /evora/ea_e_effort(
    COND /evora/ea_e_effort( WHEN <lv_effort_unit> = 'MIN'
                          then lv_effort / 60
                          WHEN <lv_effort_unit> = 'DAY'
                          THEN lv_effort * 24
                          WHEN <lv_effort_unit> = 'H'
                          THEN lv_effort
                          ELSE lv_effort ) ).

*    IF <lv_effort_unit> = 'MIN'.
*      <lv_effort> = <lv_effort> / 60.
*    ELSE.
*      <lv_effort> = <lv_effort>.
*    ENDIF.

*    SELECT SINGLE aufpl
*      FROM afko
*      INTO @DATA(lv_operation_task)
*      WHERE aufnr  = @<lv_orderid>.
*    IF sy-subrc <> 0.
*      RETURN.
*    ENDIF.


  ENDMETHOD.


  METHOD provide_effort.

    ASSIGN sr_data->* TO FIELD-SYMBOL(<ls_data>).
    ASSIGN COMPONENT 'BAPI_ALM_TIMECONFIRMATION' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<ls_tconfirm_data>).
    IF <ls_tconfirm_data> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'EFFORT' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort>).
    IF <lv_effort> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'EFFORT_UNIT' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort_unit>).
    IF <lv_effort_unit> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    <lv_effort> = COND /evora/ea_e_effort( WHEN <lv_effort_unit> = 'MIN'
                          then <lv_effort> / 60
                          WHEN <lv_effort_unit> = 'DAY'
                          THEN <lv_effort> * 24
                          ELSE <lv_effort> ).

*    IF <lv_effort_unit> = 'MIN'.
*      <lv_effort> = <lv_effort> / 60.
*    ELSE.
*      <lv_effort> = <lv_effort>.
*    ENDIF.

*    SELECT SINGLE aufpl
*      FROM afko
*      INTO @DATA(lv_operation_task)
*      WHERE aufnr  = @<lv_orderid>.
*    IF sy-subrc <> 0.
*      RETURN.
*    ENDIF.


  ENDMETHOD.


  METHOD provide_end_timestamp.

    ASSIGN sr_data->* TO   FIELD-SYMBOL(<ls_data>).
    ASSIGN COMPONENT 'BAPI_ALM_TIMECONFIRMATION' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<ls_tconfirm_data>).
    IF <ls_tconfirm_data> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'EXEC_FIN_TIME' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_end_time>).
    IF <lv_end_time> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    CONVERT DATE sv_source_value TIME <lv_end_time> INTO TIME STAMP ev_target_value TIME ZONE 'UTC'.

  ENDMETHOD.


  METHOD provide_first_name.

    SELECT SINGLE vorna
      FROM pa0002
      INTO ev_target_value
      WHERE pernr = sv_source_value. "iv_source_value.

  ENDMETHOD.


  METHOD provide_last_name.

*    SELECT SINGLE adrp~name_last
*      FROM adrp AS adrp
*      INNER JOIN usr21 AS usr21
*      ON usr21~persnumber = adrp~persnumber
*      INTO ev_target_value
*      WHERE usr21~bname = iv_source_value.


    SELECT SINGLE nachn
      FROM pa0002
      INTO ev_target_value
      WHERE pernr = sv_source_value.

  ENDMETHOD.


  METHOD provide_operation_description.

    ASSIGN sr_data->* TO FIELD-SYMBOL(<ls_data>).
    ASSIGN COMPONENT 'BAPI_ALM_TIMECONFIRMATION' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<ls_tconfirm_data>).
    IF <ls_tconfirm_data> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'ORDERID' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_orderid>).
    IF <lv_orderid> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    SELECT SINGLE aufpl
      FROM afko
      INTO @DATA(lv_operation_task)
      WHERE aufnr  = @<lv_orderid>.
    IF sy-subrc <> 0.
      RETURN.
    ENDIF.

    SELECT SINGLE ltxa1
      FROM afvc
      INTO ev_target_value
      WHERE aufpl = lv_operation_task
        AND vornr = sv_source_value.

  ENDMETHOD.


  METHOD provide_order_description.

    SELECT SINGLE ktext
      FROM aufk
      INTO ev_target_value
      WHERE aufnr = sv_source_value.

  ENDMETHOD.


  METHOD provide_start_timestamp.

    ASSIGN sr_data->* TO FIELD-SYMBOL(<ls_data>).
    ASSIGN COMPONENT 'BAPI_ALM_TIMECONFIRMATION' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<ls_tconfirm_data>).
    IF <ls_tconfirm_data> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    ASSIGN COMPONENT 'EXEC_START_TIME' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_start_time>).
    IF <lv_start_time> IS NOT ASSIGNED.
      RETURN.
    ENDIF.

    CONVERT DATE sv_source_value TIME <lv_start_time> INTO TIME STAMP ev_target_value TIME ZONE 'UTC'.

  ENDMETHOD.


  method PROVIDE_UNIT_OF_ACTUAL_DUR.
     ev_target_value = 'H'.
  endmethod.


  METHOD PROVIDE_UNIT_OF_ACTUAL_WORK.

    ev_target_value = 'H'.
*    ASSIGN sr_data->* TO FIELD-SYMBOL(<ls_data>).
*    ASSIGN COMPONENT 'BAPI_ALM_TIMECONFIRMATION' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<ls_tconfirm_data>).
*    IF <ls_tconfirm_data> IS NOT ASSIGNED.
*      RETURN.
*    ENDIF.
*
*    ASSIGN COMPONENT 'ACT_WORK' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort>).
*    IF <lv_effort> IS NOT ASSIGNED.
*      RETURN.
*    ENDIF.
*
*    ASSIGN COMPONENT 'UN_WORK' OF STRUCTURE <ls_tconfirm_data> TO FIELD-SYMBOL(<lv_effort_unit>).
*    IF <lv_effort_unit> IS NOT ASSIGNED.
*      RETURN.
*    ENDIF.
*
*    ev_target_value = conv /evora/ea_e_effort(
*    COND #( WHEN <lv_effort_unit> = 'MIN'
*                          then <lv_effort> / 60
*                          WHEN <lv_effort_unit> = 'DAY'
*                          THEN <lv_effort> * 24
*                          ELSE <lv_effort> ) ).
*
**    IF <lv_effort_unit> = 'MIN'.
**      <lv_effort> = <lv_effort> / 60.
**    ELSE.
**      <lv_effort> = <lv_effort>.
**    ENDIF.
*
**    SELECT SINGLE aufpl
**      FROM afko
**      INTO @DATA(lv_operation_task)
**      WHERE aufnr  = @<lv_orderid>.
**    IF sy-subrc <> 0.
**      RETURN.
**    ENDIF.



  ENDMETHOD.


  METHOD provide_work_center.

    SELECT SINGLE vaplz
      FROM aufk
      INTO ev_target_value
      WHERE aufnr = sv_source_value.

  ENDMETHOD.
ENDCLASS.
