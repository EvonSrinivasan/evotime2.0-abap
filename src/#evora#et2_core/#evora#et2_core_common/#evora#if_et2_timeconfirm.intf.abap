INTERFACE /evora/if_et2_timeconfirm
  PUBLIC .

  CONSTANTS:
    BEGIN OF cs_field_names,
      request_guid          TYPE fieldname VALUE 'REQUEST_GUID',
      request_id            TYPE fieldname VALUE 'REQUEST_ID',
      confirmation          TYPE fieldname VALUE 'CONFIRMATION',
      request_description   TYPE fieldname VALUE 'REQUEST_DESCRIPTION',
      confirmation_counter  TYPE fieldname VALUE 'CONFIRMATION_COUNTER',
      person_number         TYPE fieldname VALUE 'PERSON_NUMBER',
      first_name            TYPE fieldname VALUE 'FIRST_NAME',
      last_name             TYPE fieldname VALUE 'LAST_NAME',
      order_id              TYPE fieldname VALUE 'ORDER_ID',
      order_description     TYPE fieldname VALUE 'ORDER_DESCRIPTION',
      operation             TYPE fieldname VALUE 'OPERATION',
      operation_description TYPE fieldname VALUE 'OPERATION_DESCRIPTION',
      work_center           TYPE fieldname VALUE 'WORK_CENTER',
      effort                TYPE fieldname VALUE 'EFFORT',
      effort_unit           TYPE fieldname VALUE 'EFFORT_UNIT',
      start_timestamp       TYPE fieldname VALUE 'START_TIMESTAMP',
      end_timestamp         TYPE fieldname VALUE 'END_TIMESTAMP',
      start_date            TYPE fieldname VALUE 'START_DATE',
      start_time            TYPE fieldname VALUE 'START_TIME',
      end_date              TYPE fieldname VALUE 'END_DATE',
      end_time              TYPE fieldname VALUE 'END_TIME',
    END OF cs_field_names.

ENDINTERFACE.
