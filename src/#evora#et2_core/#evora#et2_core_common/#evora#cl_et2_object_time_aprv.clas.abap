CLASS /evora/cl_et2_object_time_aprv DEFINITION
  PUBLIC
  INHERITING FROM /evora/cl_ea_object_base
  CREATE PUBLIC .

  PUBLIC SECTION.

    METHODS /evora/if_ea_object_int~before_save
        REDEFINITION .
    METHODS /evora/if_ea_object~execute_function
        REDEFINITION .
    METHODS /evora/if_ea_object~get_description
        REDEFINITION .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS /EVORA/CL_ET2_OBJECT_TIME_APRV IMPLEMENTATION.


  METHOD /evora/if_ea_object_int~before_save.

  ENDMETHOD.


  METHOD /evora/if_ea_object~execute_function.

    TRY.
        rv_success = COND #( WHEN iv_function IS NOT INITIAL
                             THEN super->/evora/if_ea_object~execute_function( iv_function = iv_function )
                             ELSE abap_false ).

        CASE iv_function.
          WHEN /evora/if_et2_constants=>cs_functions-approve.

            DATA(rv_result) =
            /evora/cl_ea_core_int_manager=>get_instance(
                iv_internal_combo_key = /evora/if_ea_object~gv_internal_combo_key )->integrate_objects_outbound(
              EXPORTING
                iv_target_system           = /evora/cl_ea_service_facade=>get_int_customizing( )->get_target_system(
                                                                                                   iv_internal_combo_key = /evora/if_ea_object~gv_internal_combo_key
                                                                                                   iv_source_system      = /evora/if_ea_object~get_source( )
                                                                                               )    " EvoApproval: Target system
              iv_simulation_mode         = 'X'    " Boolean Variable (X=true, -=false, space=unknown)
              iv_object_key         = /evora/if_ea_object~gv_object_key   " EvoApproval: Core Object Interface
            ).
          WHEN /evora/if_et2_constants=>cs_functions-reapprove.
            rv_result =
            /evora/cl_ea_core_int_manager=>get_instance(
              iv_internal_combo_key = /evora/if_ea_object~gv_internal_combo_key )->integrate_objects_outbound(
            EXPORTING
              iv_target_system           = /evora/cl_ea_service_facade=>get_int_customizing( )->get_target_system(
                                                                                    iv_internal_combo_key = /evora/if_ea_object~gv_internal_combo_key
                                                                                    iv_source_system      = /evora/if_ea_object~get_source( )
                                                                                )    " EvoApproval: Target system
              iv_simulation_mode         = 'X'    " Boolean Variable (X=true, -=false, space=unknown)
              iv_object_key         = /evora/if_ea_object~gv_object_key   " EvoApproval: Core Object Interface
            ).
          WHEN OTHERS.
        ENDCASE.
      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex).
        /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_exception( io_exception = lo_ex ).
        RETURN.
    ENDTRY.

  ENDMETHOD.


  METHOD /evora/if_ea_object~get_description.

    TRY.
        super->/evora/if_ea_object~get_description(
          RECEIVING
            rv_description = rv_description
               ).
      CATCH /evora/cx_ea_api_exception .
    ENDTRY.

    ASSIGN gr_persisted_image->* TO FIELD-SYMBOL(<ls_data>).
    ASSIGN COMPONENT /evora/if_et2_constants=>cs_field_names-request_description OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<lv_conf_text>).
    IF <lv_conf_text> IS ASSIGNED
      AND <lv_conf_text> IS NOT INITIAL.
      rv_description = <lv_conf_text>.
    ENDIF.

  ENDMETHOD.
ENDCLASS.
