class /EVORA/CL_ET2_ODATA_TCONF_HNDL definition
  public
  inheriting from /EVORA/CL_EA_ODATA_HANDLR_BASE
  create public .

public section.

  methods /EVORA/IF_EA_ODATA_SRV_HANDLER~MAP_ODATA_TO_CORE
    redefinition .
  methods /EVORA/IF_EA_ODATA_SRV_HANDLER~MAP_TO_ODATA_ENTITY
    redefinition .
protected section.
private section.
ENDCLASS.



CLASS /EVORA/CL_ET2_ODATA_TCONF_HNDL IMPLEMENTATION.


  METHOD /evora/if_ea_odata_srv_handler~map_odata_to_core.
    " Convert time to single unit of measure for analytical table
    DATA: lr_data TYPE REF TO data.
    TRY.
*        DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( gv_internal_combo_key  ).
*        CREATE DATA lr_data TYPE HANDLE lo_line_type.
*        ASSIGN lr_data->* TO FIELD-SYMBOL(<ls_data>).
*        MOVE-CORRESPONDING cs_data TO <ls_data>.
        ASSIGN COMPONENT /evora/if_et2_timeconfirm=>cs_field_names-effort_unit OF STRUCTURE cs_data TO FIELD-SYMBOL(<lv_effort_unit>).
        IF sy-subrc = 0.
          ASSIGN COMPONENT /evora/if_et2_timeconfirm=>cs_field_names-effort OF STRUCTURE cs_data TO FIELD-SYMBOL(<lv_effort>).
          IF sy-subrc = 0.
            DATA(lv_effort) = CONV /evora/ea_e_effort( <lv_effort> ).

            <lv_effort> =   COND /evora/ea_e_effort( WHEN <lv_effort_unit> = 'MIN'
                                  THEN lv_effort / 60
                                  WHEN <lv_effort_unit> = 'DAY'
                                  THEN lv_effort * 24
                                  WHEN <lv_effort_unit> = 'H'
                                  THEN lv_effort
                                  ELSE lv_effort ) .

          ENDIF.
        ENDIF.
        IF <lv_effort_unit> IS ASSIGNED.
          <lv_effort_unit> = 'H'.
        ENDIF.
      CATCH /evora/cx_ea_api_exception.
    ENDTRY.
  ENDMETHOD.


  METHOD /evora/if_ea_odata_srv_handler~map_to_odata_entity.
    DATA: lr_data         TYPE REF TO data.
    FIELD-SYMBOLS: <lv_request_guid> TYPE /evora/et2_e_request_guid.


    TRY.
        DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( iv_internal_combo_key = iv_internal_combo_key  ).
        CREATE DATA lr_data TYPE HANDLE lo_line_type.
        ASSIGN lr_data->* TO FIELD-SYMBOL(<ls_confirmation_data>).
        MOVE-CORRESPONDING is_data TO <ls_confirmation_data>.
        ASSIGN COMPONENT /evora/if_et2_timeconfirm=>cs_field_names-request_guid OF STRUCTURE is_data TO <lv_request_guid>.

        super->/evora/if_ea_odata_srv_handler~map_to_odata_entity(
              EXPORTING
                is_data               = is_data
              IMPORTING
                es_entity            =  es_entity
                ).
        "Booking date and Booking time
*        ASSIGN COMPONENT /evora/if_ea_constants=>cs_common_fields-created_at OF STRUCTURE is_data TO FIELD-SYMBOL(<lv_created_at>).
*        /evora/cl_ea_utilities=>convert_timestamp_to_date_time(
*            EXPORTING
*              iv_timezone   = sy-zonlo
*              iv_timestamp  = <lv_created_at>
*            IMPORTING
*              ev_date       = DATA(lv_booking_date)
*              ev_time       = DATA(lv_booking_time)
*        ).
*        ASSIGN COMPONENT 'BOOKING_DATE' OF STRUCTURE es_entity TO FIELD-SYMBOL(<lv_date>).
*        IF <lv_date> IS ASSIGNED.
*          <lv_date> = lv_booking_date.
*        ENDIF.
*
*        ASSIGN COMPONENT 'BOOKING_TIME' OF STRUCTURE es_entity TO FIELD-SYMBOL(<lv_time>).
*        IF <lv_time> IS ASSIGNED.
*          <lv_time> = lv_booking_time.
*        ENDIF.

        DATA(lo_time_apprvl_buffer) = /evora/cl_ea_service_facade=>get_data_buffer( iv_internal_combo_key = iv_internal_combo_key ).
        DATA(lo_time_approval) = lo_time_apprvl_buffer->get_by_key( iv_object_key = <lv_request_guid> ).
        "get allowed functions
        DATA(lt_allowed_functions) = lo_time_approval->get_allowed_functions( ).
        " set true for allowed functions
        LOOP AT lt_allowed_functions ASSIGNING FIELD-SYMBOL(<lv_functions>).
          ASSIGN COMPONENT |ALLOW_{ <lv_functions> }| OF STRUCTURE es_entity TO FIELD-SYMBOL(<lv_allowed_function>).
          IF sy-subrc = 0.
            <lv_allowed_function> = abap_true.
          ENDIF.
        ENDLOOP.
        " get Icon Configuration
        /evora/cl_ea_service_facade=>get_ui_customizing( )->get_icon_configuration(
        EXPORTING
          iv_internal_combo_key   = iv_internal_combo_key    " EvoTime: Internal Combo Key
          iv_ui_fieldname         = 'STATUS'
        IMPORTING
          et_icon_configuration   = DATA(lt_ui_icon_config)
          ).
        "Fill the status icon field
        ASSIGN COMPONENT 'STATUS' OF STRUCTURE es_entity TO FIELD-SYMBOL(<lv_status>).
        IF <lv_status> IS ASSIGNED.
          ASSIGN lt_ui_icon_config[ field_config_value = <lv_status> ] TO FIELD-SYMBOL(<ls_ui_icon_config>).
          IF sy-subrc = 0.
            ASSIGN COMPONENT 'STATUS_ICON' OF STRUCTURE es_entity TO FIELD-SYMBOL(<lv_status_icon>).
            IF <lv_status_icon> IS ASSIGNED.
              <lv_status_icon> = <ls_ui_icon_config>-icon.
            ENDIF.
          ENDIF.
        ENDIF.
        if /evora/cl_et2_odata_utility=>gv_entity_call is NOT INITIAL.
        ASSIGN COMPONENT /evora/if_et2_timeconfirm=>cs_field_names-effort_unit OF STRUCTURE es_entity TO FIELD-SYMBOL(<lv_effort_unit>).
        IF sy-subrc = 0.
          ASSIGN COMPONENT /evora/if_et2_timeconfirm=>cs_field_names-effort OF STRUCTURE es_entity TO FIELD-SYMBOL(<lv_effort>).
          IF sy-subrc = 0.

            DATA(lv_effort) = CONV /evora/ea_e_effort( <lv_effort> ).
            IF lv_effort LT '1.00'.
              <lv_effort_unit> = 'MIN'.
            ENDIF.
            <lv_effort> =   COND /evora/ea_e_effort( WHEN lv_effort LT '1.00'
                                  THEN round( val = lv_effort * 60 dec = 1 )
                                  ELSE lv_effort ) .
          ENDIF.
        ENDIF.

          endif.

      CATCH /evora/cx_ea_api_exception.
        /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_sy_msg( ).
        RETURN.
    ENDTRY.
  ENDMETHOD.
ENDCLASS.
