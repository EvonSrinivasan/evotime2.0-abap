class /EVORA/CL_ET2_MAIN_MPC_EXT definition
  public
  inheriting from /EVORA/CL_ET2_MAIN_MPC
  create public .

public section.

  methods DEFINE
    redefinition .
  methods /IWBEP/IF_MGW_MED_LOAD~GET_LAST_MODIFIED
    redefinition .
protected section.
private section.

  methods DEFINE_ANNOTATIONS .
  methods DEFINE_ANNO_APPROVAL .
  methods DEFINE_ANNO_CHANGELOG .
  methods DEFINE_ANNO_UNIT .
  methods DEFINE_ANNO_PLANT .
  methods DEFINE_ANNO_WORKCENTER .
  methods DEFINE_ANNO_STATUS .
  methods DEFINE_ANNO_DETAIL_PAGE .
  methods DEFINE_ANNO_ALV_TABLE .
ENDCLASS.



CLASS /EVORA/CL_ET2_MAIN_MPC_EXT IMPLEMENTATION.


  METHOD /iwbep/if_mgw_med_load~get_last_modified.

    CALL METHOD super->/iwbep/if_mgw_med_load~get_last_modified
      EXPORTING
        iv_version        = iv_version
        iv_technical_name = iv_technical_name
      CHANGING
        cv_last_modified  = cv_last_modified.

    " Add 5 millisecond to time to get the future time to overrite the cache

    " Concatenate the data and time to prepare future timestamp to load the metadat again

    " Change timestamp to future to load metadata everytime

    cv_last_modified = | { sy-datum }{ ( sy-uzeit + 5 ) } |.

  ENDMETHOD.


  METHOD define.
    super->define( ).
    DATA lo_action   TYPE REF TO /iwbep/if_mgw_odata_action.
    DATA lo_property TYPE REF TO /iwbep/if_mgw_odata_property. "#EC NEEDED
    TRY.
        DATA(lo_entity_type) = model->get_entity_type( iv_entity_name = 'SystemInformation' ) ##NO_TEXT.
        lo_entity_type->add_auto_expand_include( iv_include_name   = '/EVORA/ET2_S_UI_SYSTEM_INFO'
                                                 iv_bind_conversions = abap_true ).

        CLEAR lo_entity_type.
        lo_entity_type = model->get_entity_type( iv_entity_name = 'Approval' ) ##NO_TEXT.
        lo_entity_type->add_auto_expand_include( iv_include_name   = '/EVORA/ET2_S_TIMEAPPROVAL_FLDS'
                                                 iv_bind_conversions = abap_true ).
        lo_entity_type->set_creatable(  ).
        lo_entity_type->set_updatable(  ).
        lo_entity_type->set_deletable(  ).

        lo_entity_type->add_auto_expand_include( iv_include_name   = '/EVORA/EA_S_APPRVL_STATUS_FLDS'
                                                 iv_bind_conversions = abap_true ).

        lo_entity_type->add_auto_expand_include( iv_include_name   = '/EVORA/ET2_S_TAPPRVL_ACTN_FLDS'
                                                 iv_bind_conversions = abap_true ).

        lo_entity_type->add_auto_expand_include( iv_include_name   = '/EVORA/EA_S_COREAPPROVAL_FLDS'
                                                 iv_bind_conversions = abap_true ).
        lo_entity_type->add_auto_expand_include( iv_include_name   = '/EVORA/ET2_S_UI_APPRL_CUSTFLDS'
                                                 iv_bind_conversions = abap_true ).


        lo_property = lo_entity_type->get_property( iv_property_name = 'EffortUnit' ).
        lo_property->set_conversion_exit(
            iv_conv_exit  = 'CUNIT'
        ).
      CATCH /iwbep/cx_mgw_med_exception.  "
    ENDTRY.
*   include all annotations for enriching the metadata model
    me->define_annotations( ).
  ENDMETHOD.


  METHOD define_annotations.
    define_anno_approval( ).
    define_anno_plant( ).
    define_anno_status( ).
    define_anno_detail_page( ).
    define_anno_alv_table( ).
  ENDMETHOD.


  METHOD define_anno_alv_table.
    DATA: lo_ann_target  TYPE REF TO /iwbep/if_mgw_vocan_ann_target.   " Vocabulary Annotation Target
    DATA: lo_annotation  TYPE REF TO /iwbep/if_mgw_vocan_annotation.   " Vocabulary Annotation
    DATA: lo_collection  TYPE REF TO /iwbep/if_mgw_vocan_collection.   " Vocabulary Annotation Collection
    DATA: lo_property    TYPE REF TO /iwbep/if_mgw_vocan_property.     " Vocabulary Annotation Property
    DATA: lo_record      TYPE REF TO /iwbep/if_mgw_vocan_record.       " Vocabulary Annotation Record
    DATA: lo_simp_value  TYPE REF TO /iwbep/if_mgw_vocan_simple_val.   " Vocabulary Annotation Simple Value
    DATA: lo_reference   TYPE REF TO /iwbep/if_mgw_vocan_reference.    " Vocabulary Annotation Reference

    TRY.
        lo_reference = vocab_anno_model->create_vocabulary_reference( iv_vocab_id = '/IWBEP/VOC_UI' iv_vocab_version = '0001').
        lo_reference->create_include( iv_namespace = 'com.sap.vocabularies.UI.v1' iv_alias = 'UI' ).
        lo_reference->create_include( iv_namespace = 'com.sap.vocabularies.Common.v1' iv_alias = 'COMMON' ).

*       get the entity and it's expanded properties
        DATA(lo_entity) = model->get_entity_type( iv_entity_name = 'DayDisplay' ) ##NO_TEXT.
        lo_entity->set_semantic( 'aggregate' ).
*        DATA(lt_props) = lo_entity->get_properties(  ).
        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/RequestId' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.

        DATA(lo_entity_property) = lo_entity->get_property( 'RequestId' ).
        DATA(lo_sap_annotation) = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'aggregation-role'
          iv_value    = 'dimension' ).
*
*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/Confirmation' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*        lo_entity_property = lo_entity->get_property( 'Confirmation' ).
*
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
*
*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/RequestDescription' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'RequestDescription' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
**
*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/ConfirmationCounter' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'ConfirmationCounter' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/PersonNumber' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.

        lo_entity_property = lo_entity->get_property( 'PersonNumber' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'aggregation-role'
          iv_value    = 'dimension' ).


*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/FirstName' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'FirstName' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
*
*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/LastName' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'LastName' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
*
*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/OrderId' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'OrderId' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
**
*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/WorkCenter' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'WorkCenter' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/Effort' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.

        lo_entity_property = lo_entity->get_property( 'Effort' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'aggregation-role'
          iv_value    = 'measure' ).

*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/StartDate' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'StartDate' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
*
*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/FinalConfirmation' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'FinalConfirmation' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).

*        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/GeneratedId' ).
*        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property = lo_entity->get_property( 'GeneratedId' ).
*        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
*
*

    ENDTRY.
  ENDMETHOD.


  METHOD define_anno_approval.
    DATA: lo_ann_target  TYPE REF TO /iwbep/if_mgw_vocan_ann_target.   " Vocabulary Annotation Target
    DATA: lo_annotation  TYPE REF TO /iwbep/if_mgw_vocan_annotation.   " Vocabulary Annotation
    DATA: lo_collection  TYPE REF TO /iwbep/if_mgw_vocan_collection.   " Vocabulary Annotation Collection
    DATA: lo_property    TYPE REF TO /iwbep/if_mgw_vocan_property.     " Vocabulary Annotation Property
    DATA: lo_record      TYPE REF TO /iwbep/if_mgw_vocan_record.       " Vocabulary Annotation Record
    DATA: lo_simp_value  TYPE REF TO /iwbep/if_mgw_vocan_simple_val.   " Vocabulary Annotation Simple Value
    DATA: lo_reference   TYPE REF TO /iwbep/if_mgw_vocan_reference.    " Vocabulary Annotation Reference

    TRY.
        lo_reference = vocab_anno_model->create_vocabulary_reference( iv_vocab_id = '/IWBEP/VOC_UI' iv_vocab_version = '0001').
        lo_reference->create_include( iv_namespace = 'com.sap.vocabularies.UI.v1' iv_alias = 'UI' ).
        lo_reference->create_include( iv_namespace = 'com.sap.vocabularies.Common.v1' iv_alias = 'COMMON' ).

*       get the entity and it's expanded properties
        DATA(lo_entity) = model->get_entity_type( iv_entity_name = 'Approval' ) ##NO_TEXT.
        lo_entity->set_semantic( 'aggregate' ).
        DATA(lt_props) = lo_entity->get_properties(  ).

*       Define the annotation target for the whole entity
        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval' ) ##NO_TEXT.
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )  ##NO_TEXT.
        TRY .
            DATA(lv_internal_combo_key) = /evora/cl_ea_service_facade=>get_ready_to_run(
                                          iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                          iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                          ).

          CATCH /evora/cx_ea_api_exception.

        ENDTRY.
        lo_entity->set_creatable( ).
        lo_entity->set_updatable( ).
        lo_entity->set_deletable( ).
        " get Annotation Configuration
        /evora/cl_ea_service_facade=>get_ui_customizing( )->get_annotation_configuration(
          EXPORTING
            iv_internal_combo_key   = lv_internal_combo_key    " EvoTime: Internal Combo Key
          IMPORTING
            et_annotations_config   = DATA(lt_annotations_config)
            ).
*       annotation for table columns
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'com.sap.vocabularies.UI.v1.LineItem' ).
        DATA(lo_ui_line_collection) = lo_annotation->create_collection( ).
*       annotation for selection fields
        lo_annotation = lo_ann_target->create_annotation(  iv_term = 'com.sap.vocabularies.UI.v1.SelectionFields' ).
        DATA(lo_ui_selection_coll) = lo_annotation->create_collection( ).

        LOOP AT lt_annotations_config ASSIGNING FIELD-SYMBOL(<ls_annotation_config>).

          IF <ls_annotation_config>-hide_field = abap_true.
            READ TABLE lt_props ASSIGNING FIELD-SYMBOL(<ls_property>)
              WITH KEY name = <ls_annotation_config>-ui_structure_fieldname.

            lo_ann_target = vocab_anno_model->create_annotations_target( |Approval/{ <ls_property>-external_name }| ).
            lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
            lo_annotation = lo_ann_target->create_annotation( iv_term = 'COMMON.FieldControl' ).
            lo_annotation->create_simple_value( )->set_enum_member_by_name( 'com.sap.vocabularies.Common.v1.FieldControlType/Hidden' ).
            CONTINUE.
          ENDIF.
          CHECK <ls_annotation_config>-show_as_column = abap_true OR
                <ls_annotation_config>-show_as_search_field = abap_true.

*         get the property based on matching ABAP fieldnames
          READ TABLE lt_props ASSIGNING <ls_property>
            WITH KEY name = <ls_annotation_config>-ui_structure_fieldname.
          CHECK sy-subrc = 0.

          IF <ls_annotation_config>-show_as_column = abap_true.
            lo_record = lo_ui_line_collection->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.DataField' ).
            lo_record->create_property( 'Value' )->create_simple_value( )->set_path( |{ <ls_property>-external_name }| ).

            lo_record->create_property( 'Position' )->create_simple_value( )->set_integer( <ls_annotation_config>-column_position ).
          ENDIF.
          IF <ls_annotation_config>-show_as_search_field = abap_true.
            lo_ui_selection_coll->create_simple_value( )->set_property_path( |{ <ls_property>-external_name }| ).
          ENDIF.
        ENDLOOP.
        " get Icon Configuration
        /evora/cl_ea_service_facade=>get_ui_customizing( )->get_icon_configuration(
        EXPORTING
          iv_internal_combo_key   = lv_internal_combo_key    " EvoTime: Internal Combo Key
          iv_ui_fieldname         = 'FUNCTIONS'
        IMPORTING
          et_icon_configuration   = DATA(lt_ui_icon_config)
          ).

            lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval' ).
            lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )   .
        "Presentation variant type Annotation
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'com.sap.vocabularies.UI.v1.PresentationVariant' ).

        lo_record = lo_annotation->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.PresentationVariantType' ).
        lo_property =  lo_record->create_property( 'RequestAtLeast' ).
        DATA(lo_presentation_variant_coll) = lo_property->create_collection( ).
        LOOP AT lt_ui_icon_config ASSIGNING FIELD-SYMBOL(<ls_ui_icon_config>).
          lo_presentation_variant_coll->create_simple_value( )->set_property_path( |ALLOW_{ <ls_ui_icon_config>-field_config_value }| ).
        ENDLOOP.

        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'Guid' ).
        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'RequestDescription' ).
        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'Status' ).
        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'GeneratedId' ).
        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'RequestID' ).


        lo_property =  lo_record->create_property( 'GroupBy' ).
        lo_presentation_variant_coll = lo_property->create_collection( ).
*        LOOP AT lt_ui_icon_config ASSIGNING FIELD-SYMBOL(<ls_ui_icon_config>).
*          lo_presentation_variant_coll->create_simple_value( )->set_property_path( |ALLOW_{ <ls_ui_icon_config>-field_config_value }| ).
*        ENDLOOP.

        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'PersonNumber' ).
*        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'RequestDescription' ).
*        lo_presentation_variant_coll->create_simple_value( )->set_property_path( 'Status' ).



        "UiLineItem with Qualifier Status action Annotation
        lo_annotation = lo_ann_target->create_annotation(
                         iv_term = 'com.sap.vocabularies.UI.v1.LineItem'
                         iv_qualifier = 'StatusActions'
                         ).

        DATA(lo_ui_lineitem_coll) = lo_annotation->create_collection( ).

        LOOP AT lt_ui_icon_config ASSIGNING <ls_ui_icon_config>.
          lo_record = lo_ui_lineitem_coll->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.DataFieldWithAction' ).
          lo_record->create_property( iv_property_name = 'Action' )->create_simple_value( )->set_string( |{ <ls_ui_icon_config>-field_config_value }| )  ##NO_TEXT.
          lo_record->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path( |ALLOW_{ <ls_ui_icon_config>-field_config_value }| )  ##NO_TEXT.
          lo_record->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( |{ <ls_ui_icon_config>-config_value_desc }| )  ##NO_TEXT.
          lo_record->create_property( iv_property_name = 'IconUrl' )->create_simple_value( )->set_string( |{ <ls_ui_icon_config>-icon }| )  ##NO_TEXT.
        ENDLOOP.

        LOOP AT lt_ui_icon_config ASSIGNING <ls_ui_icon_config>.
          lo_ann_target = vocab_anno_model->create_annotations_target( |Approval/ALLOW_{ <ls_ui_icon_config>-field_config_value }| ).
          lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
          lo_annotation = lo_ann_target->create_annotation( iv_term = 'COMMON.FieldControl' ).
          lo_annotation->create_simple_value( )->set_enum_member_by_name( 'com.sap.vocabularies.Common.v1.FieldControlType/Hidden' ).
        ENDLOOP.

*       add additional annotations for special fields
        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/Guid' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'COMMON.Text' ).
        lo_annotation->create_simple_value( )->set_path( 'RequestDescription' ).
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Optional' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/RequestDescription' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Optional' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/Spras' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'UI.Hidden' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/SourceSystem' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.Computed' ).
        lo_annotation->create_simple_value( )->set_boolean( abap_true ).
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Optional' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/SourceReferenceValue' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/Status' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'COMMON.Label' ).
        lo_annotation->create_simple_value( )->set_string( |{ TEXT-l01 }| ).
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'com.sap.vocabularies.Common.v1.IsUpperCase' ).
        lo_annotation->create_simple_value( )->set_boolean(  abap_true ).
        DATA(lo_entity_property) = lo_entity->get_property( 'Status' ).
        DATA(lo_sap_annotation) = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'display-format'
          iv_value    = 'UpperCase' ).

        DATA(lo_annotation_valuelist) = lo_ann_target->create_annotation( iv_term = 'COMMON.ValueList' ).
        lo_record = lo_annotation_valuelist->create_record( iv_record_type = 'COMMON.ValueListType' ).
        lo_record->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( |{ TEXT-l01 }| )  ##NO_TEXT.
        lo_record->create_property( iv_property_name = 'CollectionPath' )->create_simple_value( )->set_string( iv_value = 'StatusSet' ).
        lo_record->create_property( iv_property_name = 'CollectionRoot' )->create_simple_value( )->set_string( iv_value = ' ' ).
        lo_record->create_property( iv_property_name = 'SearchSupported' )->create_simple_value( )->set_boolean( iv_value = abap_false ).
        DATA(lo_collection_valuelist_param) = lo_record->create_property( iv_property_name = 'Parameters' )->create_collection( ) ##NO_TEXT.
        DATA(lo_record_valuelist_out) = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterInOut' ).
        lo_record_valuelist_out->create_property( iv_property_name = 'LocalDataProperty' )->create_simple_value( )->set_property_path( iv_value = 'Status' ).
        lo_record_valuelist_out->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'UserStatus' )  ##NO_TEXT.
        DATA(lo_record_valuelist_display) = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterDisplayOnly' ).
        lo_record_valuelist_display->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'UserStatusDesc' )  ##NO_TEXT.

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/StatusDesc' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'COMMON.Label' ).
        lo_annotation->create_simple_value( )->set_string( |{ TEXT-l06 }| ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/FromDate' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Mandatory' ).
        lo_entity_property = lo_entity->get_property( 'FromDate' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'display-format'
          iv_value    = 'Date' ).
        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/FromTime' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Mandatory' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/ToDate' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Mandatory' ).
        lo_entity_property = lo_entity->get_property( 'ToDate' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'display-format'
          iv_value    = 'Date' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/CreatedOn' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Optional' ).
        lo_entity_property = lo_entity->get_property( 'CreatedOn' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'display-format'
          iv_value    = 'Date' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/BookingDate' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Optional' ).
        lo_entity_property = lo_entity->get_property( 'BookingDate' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'display-format'
          iv_value    = 'Date' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/ToTime' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'Org.OData.Core.V1.FieldControl').
        lo_annotation->create_simple_value( )->set_enum_member_by_name( 'Org.OData.Core.V1.FieldControlType/Mandatory' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/DurationUnit' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation = lo_ann_target->create_annotation( iv_term = 'com.sap.vocabularies.Common.v1.IsUpperCase' ).
        lo_annotation->create_simple_value( )->set_boolean(  abap_true ).
*
        lo_annotation_valuelist = lo_ann_target->create_annotation( iv_term       = 'COMMON.ValueList' ).
        lo_record = lo_annotation_valuelist->create_record( iv_record_type = 'COMMON.ValueListType' ).
        lo_record->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( iv_value = |{ TEXT-l03 }| )  ##NO_TEXT.
        lo_record->create_property( iv_property_name = 'CollectionPath' )->create_simple_value( )->set_string( iv_value = 'DurationUnitSet' ).
        lo_record->create_property( iv_property_name = 'CollectionRoot' )->create_simple_value( )->set_string( iv_value = ' ' ).
        lo_record->create_property( iv_property_name = 'SearchSupported' )->create_simple_value( )->set_boolean( iv_value = abap_false ).
        lo_collection_valuelist_param = lo_record->create_property( iv_property_name = 'Parameters' )->create_collection( ) ##NO_TEXT.
        lo_record_valuelist_out = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterInOut' ).
*        lo_record_valuelist_out->create_property( iv_property_name = 'LocalDataProperty' )->create_simple_value( )->set_property_path( iv_value = 'EFFORT_UNIT' ).
        lo_record_valuelist_out->create_property( iv_property_name = 'LocalDataProperty' )->create_simple_value( )->set_property_path( iv_value = 'DurationUnit' ).
        lo_record_valuelist_out->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'Msehi' )  ##NO_TEXT.
        lo_record_valuelist_display = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterDisplayOnly' ).
        lo_record_valuelist_display->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'Msehl' )  ##NO_TEXT.

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/WorkCenter' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_annotation_valuelist = lo_ann_target->create_annotation( iv_term       = 'COMMON.ValueList' ).
        lo_record = lo_annotation_valuelist->create_record( iv_record_type = 'COMMON.ValueListType' ).
        lo_record->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( iv_value = |{ TEXT-l04 }| )  ##NO_TEXT.
        lo_record->create_property( iv_property_name = 'CollectionPath' )->create_simple_value( )->set_string( iv_value = 'WorkCenterSet' ).
        lo_record->create_property( iv_property_name = 'CollectionRoot' )->create_simple_value( )->set_string( iv_value = ' ' ).
        lo_record->create_property( iv_property_name = 'SearchSupported' )->create_simple_value( )->set_boolean( iv_value = abap_false ).
        lo_collection_valuelist_param = lo_record->create_property( iv_property_name = 'Parameters' )->create_collection( ) ##NO_TEXT.
        lo_record_valuelist_out = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterInOut' ).
        lo_record_valuelist_out->create_property( iv_property_name = 'LocalDataProperty' )->create_simple_value( )->set_property_path( iv_value = 'WorkCenter' ).
        lo_record_valuelist_out->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'WorkCenterCode' )  ##NO_TEXT.
        lo_record_valuelist_display = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterDisplayOnly' ).
        lo_record_valuelist_display->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'WorkCenterDesc' )  ##NO_TEXT.
        lo_record_valuelist_display = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterDisplayOnly' ).
        lo_record_valuelist_display->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'Plant' )  ##NO_TEXT.






*day display
        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/RequestID' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_entity_property = lo_entity->get_property( 'RequestID' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'aggregation-role'
          iv_value    = 'dimension' ).


        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/PersonNumber' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_entity_property = lo_entity->get_property( 'PersonNumber' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'aggregation-role'
          iv_value    = 'dimension' ).

        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/BookingDate' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        lo_entity_property = lo_entity->get_property( 'BookingDate' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'aggregation-role'
          iv_value    = 'dimension' ).


        lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval/Duration' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.

        lo_entity_property = lo_entity->get_property( 'Duration' ).
        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
        lo_sap_annotation->add(
          iv_key      = 'aggregation-role'
          iv_value    = 'measure' ).

        RETURN.

*
**        DATA(lo_ann_target1) = vocab_anno_model->create_annotations_target( 'Approval/RequestID' ).
**        lo_ann_target1->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        DATA(lo_entity_property1) = lo_entity->get_property( 'RequestID' ).
**        DATA(lo_sap_annotation1) = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
**
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/Confirmation' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**        lo_entity_property = lo_entity->get_property( 'Confirmation' ).
**
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
**
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/RequestDescription' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'RequestDescription' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
***
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/ConfirmationCounter' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'ConfirmationCounter' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
*
*        lo_ann_target1 = vocab_anno_model->create_annotations_target( 'Approval/PersonNumber' ).
*        lo_ann_target1->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property1 = lo_entity->get_property( 'PersonNumber' ).
*        lo_sap_annotation1 = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation1->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'dimension' ).
*
*
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/FirstName' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'FirstName' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
**
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/LastName' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'LastName' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
**
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/OrderId' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'OrderId' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
***
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/WorkCenter' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'WorkCenter' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
*
*        lo_ann_target1 = vocab_anno_model->create_annotations_target( 'Approval/Duration' ).
*        lo_ann_target1->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
*
*        lo_entity_property1 = lo_entity->get_property( 'Duration' ).
*        lo_sap_annotation1 = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
*        lo_sap_annotation1->add(
*          iv_key      = 'aggregation-role'
*          iv_value    = 'measure' ).
*
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/StartDate' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'StartDate' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
**
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/FinalConfirmation' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'FinalConfirmation' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
*
**        lo_ann_target = vocab_anno_model->create_annotations_target( 'DayDisplay/GeneratedId' ).
**        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
**
**        lo_entity_property = lo_entity->get_property( 'GeneratedId' ).
**        lo_sap_annotation = lo_entity_property->/iwbep/if_mgw_odata_annotatabl~create_annotation( iv_annotation_namespace =  /iwbep/if_mgw_med_odata_types=>gc_sap_namespace ).
**        lo_sap_annotation->add(
**          iv_key      = 'aggregation-role'
**          iv_value    = 'dimension' ).
**
**

      CATCH /iwbep/cx_mgw_med_exception INTO DATA(lo_ex) ##NEEDED.
    ENDTRY.
  ENDMETHOD.


  method DEFINE_ANNO_CHANGELOG.
  endmethod.


  METHOD define_anno_detail_page.
    DATA: lo_ann_target  TYPE REF TO /iwbep/if_mgw_vocan_ann_target.   " Vocabulary Annotation Target
    DATA: lo_annotation  TYPE REF TO /iwbep/if_mgw_vocan_annotation.   " Vocabulary Annotation
    DATA: lo_collection  TYPE REF TO /iwbep/if_mgw_vocan_collection.   " Vocabulary Annotation Collection
    DATA: lo_property    TYPE REF TO /iwbep/if_mgw_vocan_property.     " Vocabulary Annotation Property
    DATA: lo_record      TYPE REF TO /iwbep/if_mgw_vocan_record.       " Vocabulary Annotation Record
    DATA: lo_simp_value  TYPE REF TO /iwbep/if_mgw_vocan_simple_val.   " Vocabulary Annotation Simple Value
    DATA: lo_reference   TYPE REF TO /iwbep/if_mgw_vocan_reference.    " Vocabulary Annotation Reference
    DATA lt_ui_dynamic_page_config TYPE /evora/if_ea_ui_customizing=>tyt_detailedpage_config.
    DATA lt_ui_datapoint_config TYPE /evora/if_ea_ui_customizing=>tyt_datapoint_config.

    lo_reference = vocab_anno_model->create_vocabulary_reference( iv_vocab_id = '/IWBEP/VOC_UI' iv_vocab_version = '0001').
    lo_reference->create_include( iv_namespace = 'com.sap.vocabularies.UI.v1' iv_alias = 'UI' ).


*    RETURN.
    TRY .

*       get the entity and it's expanded properties
        DATA(lo_entity) = model->get_entity_type( iv_entity_name = 'Approval' ) ##NO_TEXT.
        DATA(lt_props) = lo_entity->get_properties(  ).

      CATCH /iwbep/cx_mgw_med_exception.
        RETURN.
    ENDTRY.
*    DATA(lt_ui_dyanamic_page_config) =
    /evora/cl_ea_service_facade=>get_ui_customizing( )->get_detailed_page_config(
      EXPORTING
        iv_internal_combo_key   = 'EVOTIME-TCONFIRM'    " EvoApproval: Business Scenario & Object Type Key Combination
      IMPORTING
        et_detailed_page_config = lt_ui_dynamic_page_config
    ).

    /evora/cl_ea_service_facade=>get_ui_customizing( )->get_datapoint_config(
      EXPORTING
        iv_internal_combo_key = 'EVOTIME-TCONFIRM'    " EvoApproval: Business Scenario & Object Type Key Combination
      IMPORTING
        et_datapoint_config   = lt_ui_datapoint_config
    ).
    SORT:  lt_ui_dynamic_page_config BY field_position,
           lt_ui_datapoint_config    BY field_position.

    lo_ann_target = vocab_anno_model->create_annotations_target( 'Approval' ) ##NO_TEXT.
    lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )  ##NO_TEXT.

*       annotation for table columns
    lo_annotation = lo_ann_target->create_annotation( iv_term = 'com.sap.vocabularies.UI.v1.Facets' ).
    DATA(lo_ui_line_collection) = lo_annotation->create_collection( ).

*       annotation for selection fields
    lo_record = lo_ui_line_collection->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.CollectionFacet' ).
    lo_record->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( |{ 'General' }| )  ##NO_TEXT..
    lo_property =  lo_record->create_property( 'Facets' ).
    lo_collection = lo_property->create_collection( ).
    DATA(lo_record_reffacet) = lo_collection->create_record( 'com.sap.vocabularies.UI.v1.ReferenceFacet' ).
    lo_record_reffacet->create_property( iv_property_name = 'Target' )->create_simple_value( )->set_path( |{ '@com.sap.vocabularies.UI.v1.FieldGroup#Group1' }| )  ##NO_TEXT.
*    lo_record_reffacet->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( |{ 'Form' }| )  ##NO_TEXT.
    lo_record_reffacet = lo_collection->create_record( 'com.sap.vocabularies.UI.v1.ReferenceFacet' ).
    lo_record_reffacet->create_property( iv_property_name = 'Target' )->create_simple_value( )->set_path( |{ '@com.sap.vocabularies.UI.v1.FieldGroup#Group2' }| )  ##NO_TEXT.
*    lo_record_reffacet->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( |{ 'Form' }| )  ##NO_TEXT.
    lo_record_reffacet = lo_collection->create_record( 'com.sap.vocabularies.UI.v1.ReferenceFacet' ).
    lo_record_reffacet->create_property( iv_property_name = 'Target' )->create_simple_value( )->set_path( |{ '@com.sap.vocabularies.UI.v1.FieldGroup#Group3' }| )  ##NO_TEXT.
*    lo_record_reffacet->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( |{ 'Form' }| )  ##NO_TEXT.
    lo_record->create_annotation( iv_term = 'Org.OData.Core.V1.Description' )->create_simple_value( )->set_string( iv_value = |{ 'Form' }| ).


    lo_record_reffacet = lo_ui_line_collection->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.ReferenceFacet' ).
    lo_record_reffacet->create_property( iv_property_name = 'Target' )->create_annotation( iv_term = '@com.sap.vocabularies.UI.v1.LineItem' ).
    lo_record_reffacet->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( iv_value = 'Date' ).
    lo_record_reffacet->create_annotation( iv_term = 'Org.OData.Core.V1.Description' )->create_simple_value( )->set_string( iv_value = 'Date' ).

    lo_record_reffacet = lo_ui_line_collection->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.ReferenceFacet' ).
    lo_record_reffacet->create_property( iv_property_name = 'Target' )->create_annotation( iv_term = '@com.sap.vocabularies.UI.v1.LineItem' ).
    lo_record_reffacet->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( iv_value = 'Log' ).
    lo_record_reffacet->create_annotation( iv_term = 'Org.OData.Core.V1.Description' )->create_simple_value( )->set_string( iv_value = 'Log' ).


    DATA(lo_annotation_fieldgroup1) = lo_ann_target->create_annotation(
                                     iv_term       = 'com.sap.vocabularies.UI.v1.FieldGroup'
                                     iv_qualifier  = 'Group1'
                                 ).
    DATA(lo_collection_general1) = lo_annotation_fieldgroup1->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.FieldGroupType' )->create_property( iv_property_name = 'Data' )->create_collection( ).

    DATA(lo_annotation_fieldgroup2) = lo_ann_target->create_annotation(
                                     iv_term       = 'com.sap.vocabularies.UI.v1.FieldGroup'
                                     iv_qualifier  = 'Group2'
                                 ).
    DATA(lo_collection_general2) = lo_annotation_fieldgroup2->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.FieldGroupType' )->create_property( iv_property_name = 'Data' )->create_collection( ).


    DATA(lo_annotation_fieldgroup3) = lo_ann_target->create_annotation(
                                     iv_term       = 'com.sap.vocabularies.UI.v1.FieldGroup'
                                     iv_qualifier  = 'Group3'
                                 ).
    DATA(lo_collection_general3) = lo_annotation_fieldgroup3->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.FieldGroupType' )->create_property( iv_property_name = 'Data' )->create_collection( ).

    LOOP AT lt_ui_dynamic_page_config ASSIGNING FIELD-SYMBOL(<ls_dynamic_page_config>).
      ASSIGN lt_props[ name = <ls_dynamic_page_config>-ui_facet_field ] TO FIELD-SYMBOL(<ls_props>).
      IF sy-subrc = 0.
        DATA(lv_ui_column) = <ls_dynamic_page_config>-field_position MOD 3.
        CASE ( <ls_dynamic_page_config>-field_position MOD 3 ). "lv_ui_column.
          WHEN 1.
            lo_collection_general1->create_record(
                 iv_record_type = 'com.sap.vocabularies.UI.v1.DataField'
             )->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path( iv_value = |{ <ls_props>-external_name }| ).
          WHEN 2.
            lo_collection_general2->create_record(
                 iv_record_type = 'com.sap.vocabularies.UI.v1.DataField'
             )->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path( iv_value = |{ <ls_props>-external_name }| ).
          WHEN 0.
            lo_collection_general3->create_record(
                 iv_record_type = 'com.sap.vocabularies.UI.v1.DataField'
             )->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path( iv_value = |{ <ls_props>-external_name }| ).
          WHEN OTHERS.
        ENDCASE.

      ENDIF.
    ENDLOOP.

*

*    DATA(lo_property_data) = lo_annotation_fieldgroup1->create_record( iv_record_type = 'UI.FieldGroupType' )->create_property( iv_property_name = 'Data' ).
*    DATA(lo_collection_general) = lo_property_data->create_collection( ).
*    lo_collection_general->create_record( iv_record_type = 'UI.DataField' )->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path( iv_value = 'RequestID' ).
    .


    DATA(lo_annotation_header) = lo_ann_target->create_annotation( iv_term = 'com.sap.vocabularies.UI.v1.HeaderFacets' ).
    DATA(lo_collection_header) = lo_annotation_header->create_collection( ).
    LOOP AT lt_ui_datapoint_config ASSIGNING FIELD-SYMBOL(<ls_ui_datapoint_config>). "'@UI.DataPoint#Status'
      ASSIGN lt_props[ name = <ls_ui_datapoint_config>-technical_fieldname ] TO <ls_props>.
      IF sy-subrc = 0.
        lo_collection_header->create_record( iv_record_type = 'com.sap.vocabularies.UI.ReferenceFacet' )->create_property( iv_property_name = 'Target' )->create_simple_value( )->set_annotation_path( iv_value = |@UI.DataPoint#{
          <ls_ui_datapoint_config>-technical_fieldname }| ).
        DATA(lo_annotation_datapoint) = lo_ann_target->create_annotation(
                                      iv_term       = 'com.sap.vocabularies.UI.v1.DataPoint'
                                      iv_qualifier  = |{ <ls_ui_datapoint_config>-technical_fieldname }|
                                  ).
        DATA(lo_datapoint_record) = lo_annotation_datapoint->create_record(
            iv_record_type = 'com.sap.vocabularies.UI.v1.DataPointType'
        ).
        lo_datapoint_record->create_property( iv_property_name = 'Title' )->create_simple_value( )->set_string( iv_value = |{ <ls_props>-external_name }| ).
        lo_datapoint_record->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path( iv_value = |{ <ls_props>-external_name }| ).
        IF <ls_ui_datapoint_config>-long_description IS NOT INITIAL.
          lo_datapoint_record->create_property( iv_property_name = 'LongDescription' )->create_simple_value( )->set_path( iv_value = |{ <ls_ui_datapoint_config>-long_description }| ).
        ENDIF.
      ENDIF.
    ENDLOOP.

*    create_labeled_element( iv_label_name = 'Qualifier' )->create_simple_value( )->set_string( iv_value = 'General' ).
*    lo_collection_header->create_record( iv_record_type = 'UI.ReferenceFacet' )->create_property( iv_property_name = 'Target' )->create_simple_value( )->set_annotation_path( iv_value = '@UI.DataPoint#Product' ).

    DATA(lo_annotation_heaerinfo) = lo_ann_target->create_annotation( iv_term = 'com.sap.vocabularies.UI.v1.HeaderInfo' ).
    DATA(lo_reord_header) = lo_annotation_heaerinfo->create_record( iv_record_type = 'com.sap.vocabularies.UI.v1.HeaderInfoType' ).
    lo_reord_header->create_property( iv_property_name = 'TypeName' )->create_simple_value( )->set_string( iv_value = 'Title' ).
    lo_reord_header->create_property( iv_property_name = 'TypeNamePlural' )->create_simple_value( )->set_string( iv_value = 'appTitle' ).
*    ui detail page header title
    DATA(lo_record_datafield) = lo_reord_header->create_property( iv_property_name = 'Title' )->create_record(
        iv_record_type = 'com.sap.vocabularies.UI.v1.DataField'
    ).
    lo_record_datafield->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( iv_value = 'Request Description' ).
    lo_record_datafield->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path(
      iv_value = /evora/cl_ea_utilities=>get_global_parameter_value(
                     iv_internal_combo_key = 'EVOTIME-TCONFIRM'
                     iv_global_parameter   = 'UI_DETAIL_PAGE_HEADER_TITLE'
                 ) ).
*    ui detail page header descrption
    lo_record_datafield = lo_reord_header->create_property( iv_property_name = 'Description' )->create_record(
        iv_record_type = 'com.sap.vocabularies.UI.v1.DataField'
    ).
    lo_record_datafield->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( iv_value = 'Order Description' ).
    lo_record_datafield->create_property( iv_property_name = 'Value' )->create_simple_value( )->set_path(
      iv_value = /evora/cl_ea_utilities=>get_global_parameter_value(
                     iv_internal_combo_key = 'EVOTIME-TCONFIRM'
                     iv_global_parameter   = 'UI_DETAIL_PAGE_HEADER_DESC'
                 ) ).

  ENDMETHOD.


  METHOD define_anno_plant.
    DATA(lo_ann_target) = vocab_anno_model->create_annotations_target( 'WorkCenter/Plant' ).
    lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
    DATA(lo_annotation) = lo_ann_target->create_annotation( iv_term = 'COMMON.Text' ).
    lo_annotation->create_simple_value( )->set_path( 'Name1' ).
    lo_annotation->create_simple_value( )->set_path( 'Name2' ).
    DATA(lo_annotation_valuelist) = lo_ann_target->create_annotation( iv_term       = 'COMMON.ValueList' ).
    DATA(lo_record) = lo_annotation_valuelist->create_record( iv_record_type = 'COMMON.ValueListType' ).
    lo_record->create_property( iv_property_name = 'Label' )->create_simple_value( )->set_string( iv_value = |{ TEXT-l05 }| )  ##NO_TEXT.
    lo_record->create_property( iv_property_name = 'CollectionPath' )->create_simple_value( )->set_string( iv_value = 'PlantSet' ).
    lo_record->create_property( iv_property_name = 'CollectionRoot' )->create_simple_value( )->set_string( iv_value = ' ' ).
    lo_record->create_property( iv_property_name = 'SearchSupported' )->create_simple_value( )->set_boolean( iv_value = abap_false ).
    DATA(lo_collection_valuelist_param) = lo_record->create_property( iv_property_name = 'Parameters' )->create_collection( ) ##NO_TEXT.
    DATA(lo_record_valuelist_out) = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterInOut' ).
*        lo_record_valuelist_out->create_property( iv_property_name = 'LocalDataProperty' )->create_simple_value( )->set_property_path( iv_value = 'EFFORT_UNIT' ).
    lo_record_valuelist_out->create_property( iv_property_name = 'LocalDataProperty' )->create_simple_value( )->set_property_path( iv_value = 'Plant' ).
    lo_record_valuelist_out->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'PlantCode' )  ##NO_TEXT.
    DATA(lo_record_valuelist_display) = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterDisplayOnly' ).
    lo_record_valuelist_display->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'Name1' )  ##NO_TEXT.
    lo_record_valuelist_display = lo_collection_valuelist_param->create_record( iv_record_type = 'COMMON.ValueListParameterDisplayOnly' ).
    lo_record_valuelist_display->create_property( iv_property_name = 'ValueListProperty' )->create_simple_value( )->set_string( iv_value = 'Name2' )  ##NO_TEXT.
  ENDMETHOD.


  method DEFINE_ANNO_STATUS.
        DATA(lo_ann_target) = vocab_anno_model->create_annotations_target( 'Status/UserStatusDesc' ).
        lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
        DATA(lo_annotation) = lo_ann_target->create_annotation( iv_term = 'COMMON.Label' ).
        lo_annotation->create_simple_value( )->set_string( |{ TEXT-l06 }| ).
  endmethod.


  method DEFINE_ANNO_UNIT.
    DATA: lo_ann_target  TYPE REF TO /iwbep/if_mgw_vocan_ann_target.   " Vocabulary Annotation Target
    DATA: lo_annotation  TYPE REF TO /iwbep/if_mgw_vocan_annotation.   " Vocabulary Annotation

*   add additional annotations for special fields
    lo_ann_target = vocab_anno_model->create_annotations_target( 'DurationUnit/Msehi' ).
    lo_ann_target->set_namespace_qualifier( 'com.evorait.evotime' )    ##NO_TEXT.
    lo_annotation = lo_ann_target->create_annotation( iv_term = 'COMMON.Text' ).
    lo_annotation->create_simple_value( )->set_path( 'Msehl' ).

  endmethod.


  method DEFINE_ANNO_WORKCENTER.

  endmethod.
ENDCLASS.
