class /EVORA/CL_ET2_MAIN_DPC_EXT definition
  public
  inheriting from /EVORA/CL_ET2_MAIN_DPC
  create public .

public section.

  methods REPORT_MESSAGES
    raising
      /IWBEP/CX_MGW_BUSI_EXCEPTION .

  methods /IWBEP/IF_MGW_APPL_SRV_RUNTIME~EXECUTE_ACTION
    redefinition .
  methods /IWBEP/IF_MGW_APPL_SRV_RUNTIME~PATCH_ENTITY
    redefinition .
  methods /IWBEP/IF_MGW_CORE_SRV_RUNTIME~CHANGESET_BEGIN
    redefinition .
  methods /IWBEP/IF_MGW_CORE_SRV_RUNTIME~CHANGESET_END
    redefinition .
  methods /IWBEP/IF_MGW_CORE_SRV_RUNTIME~CHANGESET_PROCESS
    redefinition .
  methods IF_SADL_GW_QUERY_CONTROL~SET_QUERY_OPTIONS
    redefinition .
protected section.

  methods APPROVALSET_GET_ENTITY
    redefinition .
  methods APPROVALSET_GET_ENTITYSET
    redefinition .
  methods APPROVALSET_UPDATE_ENTITY
    redefinition .
  methods CHANGELOGSET_GET_ENTITYSET
    redefinition .
  methods DAYDISPLAYSET_GET_ENTITY
    redefinition .
  methods DAYDISPLAYSET_GET_ENTITYSET
    redefinition .
  methods DURATIONUNITSET_GET_ENTITYSET
    redefinition .
  methods DYNAMICTILESET_GET_ENTITY
    redefinition .
  methods PLANTSET_GET_ENTITYSET
    redefinition .
  methods STATUSSET_GET_ENTITYSET
    redefinition .
  methods WORKCENTERSET_GET_ENTITYSET
    redefinition .
private section.

  data GV_ODATA_MAX_RECORDS type I .
ENDCLASS.



CLASS /EVORA/CL_ET2_MAIN_DPC_EXT IMPLEMENTATION.


  METHOD /iwbep/if_mgw_appl_srv_runtime~execute_action.
    DATA: lo_action_handler TYPE REF TO /evora/cl_et2_odata_acton_hndl.
    DATA: ls_entity_approval TYPE /evora/cl_et2_main_mpc=>ts_approval.
    CASE iv_action_name.
      WHEN 'GetSystemInformation'.

        lo_action_handler = NEW /evora/cl_et2_odata_acton_hndl( ).
        lo_action_handler->get_system_information(
          IMPORTING
            es_system_information = DATA(ls_entity_system_info)
        ).

*         push the new record out
        copy_data_to_ref(
          EXPORTING
            is_data = ls_entity_system_info
          CHANGING
            cr_data = er_data
        ).

      WHEN 'ExecuteStatusFunction'.

        lo_action_handler = NEW /evora/cl_et2_odata_acton_hndl( ).
        lo_action_handler->execute_status_function(
          EXPORTING
            it_parameter = it_parameter    " table for name value pairs
          IMPORTING
            es_approval  = ls_entity_approval
        ).

*         push the new record out
        copy_data_to_ref(
          EXPORTING
            is_data = ls_entity_approval
          CHANGING
            cr_data = er_data
        ).

      WHEN  'DeleteTimeRecord'.

        lo_action_handler = NEW /evora/cl_et2_odata_acton_hndl( ).
        lo_action_handler->delete_time_approval_record(
          EXPORTING
            it_parameter = it_parameter    " table for name value pairs
          IMPORTING
            es_approval  = ls_entity_approval
        ).

*         push the new record out
        copy_data_to_ref(
          EXPORTING
            is_data = ls_entity_approval
          CHANGING
            cr_data = er_data
        ).

      WHEN OTHERS.
    ENDCASE.
    mo_context->get_message_container( )->add_messages_from_bapi( /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages( ) ).
  ENDMETHOD.


  METHOD /iwbep/if_mgw_appl_srv_runtime~patch_entity.
    TRY.
*        CALL METHOD super->/iwbep/if_mgw_appl_srv_runtime~patch_entity
*          EXPORTING
*            iv_entity_name          = iv_entity_name
*            iv_entity_set_name      = iv_entity_set_name
*            iv_source_name          = iv_source_name
*            io_data_provider        = io_data_provider
*            it_key_tab              = it_key_tab
*            it_navigation_path      = it_navigation_path
*            io_tech_request_context = io_tech_request_context
*          IMPORTING
*            er_entity               = er_entity.
*      CATCH /iwbep/cx_mgw_busi_exception .
*      CATCH /iwbep/cx_mgw_tech_exception .
    ENDTRY.
  ENDMETHOD.


  METHOD /iwbep/if_mgw_core_srv_runtime~changeset_begin.
*   use local update mode
    SET UPDATE TASK LOCAL.

*   indicate direct processing of all operations
    CLEAR cv_defer_mode.

  ENDMETHOD.


  METHOD /iwbep/if_mgw_core_srv_runtime~changeset_end.

    DATA ls_return TYPE bapiret2    ##NEEDED.

*   check for messages that have been collected
    TRY.
        IF /evora/cl_ea_utilities=>has_eax(
          /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages(
            iv_delete = abap_false )
          ) = abap_false.
          TRY .
*       if there are no errors so far we can attempt to save
              /evora/cl_ea_service_facade=>save( ).

        /evora/cl_ea_core_int_manager=>get_instance(
            iv_internal_combo_key = 'EVOTIME-TCONFIRM' )->push_messages(
*            iv_simulation_mode =
        ).
*          CATCH /evora/cx_ea_api_exception.    " .
            CATCH /evora/cx_ea_api_exception.    " .
*       if something goes terribly wrong then rollback the changes
              CALL FUNCTION 'BAPI_TRANSACTION_ROLLBACK'
                IMPORTING
                  return = ls_return.
          ENDTRY.
        ELSE.
*       already errors found - rollback everything
          /evora/cl_ea_service_facade=>cancel( ).
        ENDIF.
      CATCH cx_root INTO DATA(lo_ex)    ##NEEDED    ##CATCH_ALL.
*       if something goes terribly wrong then rollback the changes
        CALL FUNCTION 'BAPI_TRANSACTION_ROLLBACK'
          IMPORTING
            return = ls_return.
    ENDTRY.

**   send out all collected messages
    report_messages( ).

  ENDMETHOD.


  METHOD /iwbep/if_mgw_core_srv_runtime~changeset_process.

  ENDMETHOD.


  METHOD approvalset_get_entity.
    DATA: lr_data  TYPE REF TO data.
*          lv_value TYPE /evora/ea_e_guid.



    CLEAR:  es_response_context,
            er_entity.

    TRY .

        DATA(rv_internal_combo_key) = /evora/cl_ea_service_facade=>get_ready_to_run(
                                     iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                     iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                     ).

        DATA(lo_data_buffer) = /evora/cl_ea_service_facade=>get_data_buffer( iv_internal_combo_key = rv_internal_combo_key ).

**       check if we do come from a navigation - then we can't apply the normal keytab logic
*        IF iv_source_name <> iv_entity_name.
*          IF iv_source_name = 'Changelog'.

*        ELSE.
        LOOP AT it_key_tab INTO DATA(ls_key_tab).
*          er_entity-generated_id = ls_key_tab-value.
          DATA(lv_requestid) = CONV /evora/et2_e_genarate_id( ls_key_tab-value+14(12) ).
*          DATA(LV_VALUE) = CONV /EVORA/EA_E_GUID( ls_key_tab-value ).
          DATA(lo_instance) = lo_data_buffer->query_single(
                                  it_conditions              = VALUE #( ( technical_fieldname = 'REQUEST_ID'
                                                                        filter = VALUE #( ( sign = /evora/if_et2_constants=>cs_range_sign-including
                                                                                          option = /evora/if_et2_constants=>cs_range_option-equals
                                                                                          low  = CONV /evora/et2_e_genarate_id( ls_key_tab-value+14(12) ) ) ) ) )
*                                  it_sorting                 =
*                                  is_paging                  =
                              ).
*                                CATCH /evora/cx_ea_api_exception.  "
*          DATA(lo_instance) = lo_data_buffer->get_by_key( iv_object_key = lv_value ).
        ENDLOOP.
*        ENDIF.


        DATA(lo_line_type) = lo_data_buffer->get_line_type( ).
        CREATE DATA lr_data TYPE HANDLE lo_line_type.
        ASSIGN lr_data->* TO FIELD-SYMBOL(<ls_data>).

        lo_instance->get_details(
          IMPORTING
            es_data = <ls_data>
        ).

      CATCH /evora/cx_ea_api_exception.
        CLEAR er_entity.
        mo_context->get_message_container( )->add_messages_from_bapi( /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages( ) ).
        RETURN.
    ENDTRY.

*      Get Odata handler based on the customizing
    DATA(lo_odata_handler) =
      /evora/cl_ea_service_facade=>get_ui_customizing( )->get_odata_handler(
        iv_internal_combo_key = rv_internal_combo_key
    ).
*      Move the backend data to frontend data
    /evora/cl_et2_odata_utility=>gv_entity_call = 'X'.
    lo_odata_handler->map_to_odata_entity(
      EXPORTING
        iv_internal_combo_key = rv_internal_combo_key
        is_data               = <ls_data>    " Backend data
      IMPORTING
        es_entity             = er_entity    " Frontend data
    ).
    er_entity-generated_id = ls_key_tab-value.
    clear /evora/cl_et2_odata_utility=>gv_entity_call.

  ENDMETHOD.


  METHOD approvalset_get_entityset.

    DATA: lt_sorting               TYPE /evora/ea_tt_sort_cond,
          ls_paging                TYPE /evora/et2_s_paging,
          lr_data                  TYPE REF TO data,
          lv_count                 TYPE i,
          lv_max_rows              TYPE i,
          lv_top                   TYPE i,
          lo_/iwbep/cl_mgw_request TYPE REF TO /iwbep/cl_mgw_request.

    FIELD-SYMBOLS: <lt_entityset> TYPE ANY TABLE.

    REFRESH:  et_entityset.
    CLEAR     es_response_context.

*    below logic is as part of analytical table - to show the total Effort in the UI
*    below logic to determine whether we are just sending summation details or all the data
    lo_/iwbep/cl_mgw_request ?= io_tech_request_context.
    LOOP AT lo_/iwbep/cl_mgw_request->get_request_details( )-select_params INTO DATA(ls_select_params).
      CASE ls_select_params.
        WHEN 'Duration'
          OR 'DurationUnit'
          OR 'PersonNumber'
          OR 'RequestID'
          OR 'BookingDate'.
*        WHEN 'PersonNumber'.
*        WHEN 'PersonNumber'.
*        WHEN 'RequestID'.
        WHEN OTHERS.
*          need to send all the details
          DATA(lv_data_call) = abap_true.
          EXIT.
      ENDCASE.
    ENDLOOP.

    TRY .
        DATA(lv_internal_combo_key) = /evora/cl_ea_service_facade=>get_ready_to_run(
                                        iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                        iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                        ).

        IF io_tech_request_context->get_skiptoken( ) IS NOT INITIAL.
          ls_paging-offset = io_tech_request_context->get_skiptoken( ).
        ELSEIF io_tech_request_context->get_skip( ) IS NOT INITIAL.
*          for the first run, skip = 0
*          when we scroll down, for the next run, assume that skip will have 127
*          now we don't need to fetch 127 records, just need to fetch remaining records
*          so, in total if we have 155 records, 155-127 = 28 records need to be fetched
*          if we set the offset as 127, then from 127, it will fetch 29 records
*          in order to avoid that, and to fetch 28 records from 128, we are adding 1 to skip value
          ls_paging-offset = io_tech_request_context->get_skip( ) + 1.
        ELSE.
          CLEAR ls_paging.
        ENDIF.
        gv_odata_max_records = /evora/cl_ea_utilities=>get_global_parameter_value(
                           iv_internal_combo_key = lv_internal_combo_key
                           iv_global_parameter   = 'MAX_RECORDS'
                       ).
        IF gv_odata_max_records IS INITIAL.
          gv_odata_max_records = /evora/if_ea_constants=>cv_max_rows.
        ENDIF.
        lv_top = io_tech_request_context->get_top( ).
        IF ( lv_top = 0 )
          OR ( lv_top > gv_odata_max_records ).
          lv_max_rows = gv_odata_max_records.
        ELSE.
          lv_max_rows = lv_top.
        ENDIF.


        DATA(lo_data_buffer) = /evora/cl_ea_service_facade=>get_data_buffer( lv_internal_combo_key ).

**       convert the odata filter conditions back to the data buffer filter conditions
        DATA(lt_filter_select_options) = io_tech_request_context->get_filter( )->get_filter_select_options( ).
        DATA(lt_conditions) = /evora/cl_et2_odata_utility=>convert_odata_2_internal_flt( lt_filter_select_options ).
*
**       respect the sort ordering if requested
        DATA(lt_order_by) = io_tech_request_context->get_orderby( ).
        IF lt_order_by IS NOT INITIAL.
          LOOP AT lt_order_by ASSIGNING FIELD-SYMBOL(<ls_order_by>).
            ASSIGN lt_sorting[ fieldname = <ls_order_by>-property ] TO FIELD-SYMBOL(<ls_sorting>).
            IF sy-subrc <> 0.
              APPEND INITIAL LINE TO lt_sorting ASSIGNING <ls_sorting>.
              <ls_sorting>-fieldname           = <ls_order_by>-property.
              <ls_sorting>-technical_fieldname = <ls_order_by>-property.
            ENDIF.
            IF <ls_order_by>-order = 'asc' ##NO_TEXT.
              <ls_sorting>-ascending = abap_true.
            ENDIF.
          ENDLOOP.
        ELSE.
          lt_sorting = VALUE #(
            ( fieldname           = /evora/if_et2_timeconfirm=>cs_field_names-request_id
              technical_fieldname = /evora/if_et2_timeconfirm=>cs_field_names-request_id
              ascending           = abap_true )
          ).
        ENDIF.

*     pass select to API layer
        IF io_tech_request_context->has_count( ) = abap_true.
          lo_data_buffer->query(
            EXPORTING
              it_conditions            = lt_conditions
              it_sorting               = lt_sorting
              iv_nr_of_rows_requested  = abap_true
            IMPORTING
              ev_nr_of_rows            = lv_count
          ).
        ELSE.

          lo_data_buffer->query(
            EXPORTING
              it_conditions            = lt_conditions
              it_sorting               = lt_sorting
              is_paging                = ls_paging
              iv_max_rows              = lv_max_rows
              iv_nr_of_rows_requested  = abap_false
            IMPORTING
              er_data                  = lr_data
          ).



*          o with standard behaviour to have analytical table data ready
          super->approvalset_get_entityset(
            EXPORTING
              iv_entity_name               = iv_entity_name
              iv_entity_set_name           = iv_entity_set_name
              iv_source_name               = iv_source_name
              it_filter_select_options     = COND #( WHEN lt_filter_select_options IS NOT INITIAL
                                                     THEN /evora/cl_et2_odata_utility=>convert_data_to_ext_format(
                                                              ir_data                  = lr_data
                                                              iv_fieldname             = 'REQUEST_GUID'
                                                          ) )   " Table of select options
              is_paging                    = is_paging    " Paging structure
              it_key_tab                   = it_key_tab    " Table for name value pairs
              it_navigation_path           = it_navigation_path    " Table of navigation paths
              it_order                     = it_order    " The sorting order
              iv_filter_string             = |( Guid eq '0AA10FE57E901EE98C9C4672D03AA4EB' )| "iv_filter_string    " Table for name value pairs
              iv_search_string             = iv_search_string
              io_tech_request_context      = io_tech_request_context
            IMPORTING
              et_entityset                 = DATA(lt_entityset)    " Returning data
              es_response_context          = es_response_context
          ).

          IF lv_data_call = abap_false.
*          if all the data is not required then send only summation details
            et_entityset = lt_entityset.
            RETURN.
          ENDIF.

*          lo_data_buffer->query(
*            EXPORTING
*              it_conditions            = lt_conditions
*              it_sorting               = lt_sorting
*              is_paging                = ls_paging
*              iv_max_rows              = lv_max_rows
*              iv_nr_of_rows_requested  = abap_false
*            IMPORTING
*              er_data                  = lr_data
*          ).
        ENDIF.
      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex)   ##NEEDED.
        CLEAR et_entityset.
        mo_context->get_message_container( )->add_messages_from_bapi( /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages( ) ).
        RETURN.
    ENDTRY.

    IF io_tech_request_context->has_count( ) = abap_false.
      ASSIGN lr_data->* TO <lt_entityset>.
*      Get Odata handler based on the customizing
      DATA(lo_odata_handler) =
        /evora/cl_ea_service_facade=>get_ui_customizing( )->get_odata_handler(
          iv_internal_combo_key = lv_internal_combo_key
      ).

*      Move the backend data to frontend data
      lo_odata_handler->map_to_odata_entity_set(
        EXPORTING
          iv_internal_combo_key  = lv_internal_combo_key
          it_data                = <lt_entityset>
        IMPORTING
          et_entityset           = et_entityset
      ).

      LOOP AT et_entityset ASSIGNING FIELD-SYMBOL(<ls_entityset>).
        ASSIGN lt_entityset[ request_guid = <ls_entityset>-request_guid ] TO FIELD-SYMBOL(<ls_temp_entityset>).
        IF sy-subrc = 0.
*          just map generated_id to prepare analytical data
          <ls_entityset>-generated_id = <ls_temp_entityset>-generated_id.
        ENDIF.
      ENDLOOP.

    ELSE.
      es_response_context-count = lv_count.
    ENDIF.

  ENDMETHOD.


  METHOD approvalset_update_entity.
    DATA ls_data TYPE /evora/cl_et2_main_mpc=>ts_approval.
    DATA lv_message TYPE bapi_msg.
    DATA lo_data_provider TYPE REF TO /iwbep/if_mgw_entry_provider.

    TRY.
*   Create the line type of approval
        io_data_provider->read_entry_data(
          IMPORTING
            es_data = ls_data
        ).
        "Start Time Stamp
        ls_data-start_timestamp = /evora/cl_ea_utilities=>convert_date_time_to_timestamp(
                              iv_date      = ls_data-start_date
                              iv_time      = ls_data-start_time
                              iv_timezone  = /evora/if_ea_constants=>cv_timezone_utc
                          ).
        "End Time Stamp
        ls_data-end_timestamp = /evora/cl_ea_utilities=>convert_date_time_to_timestamp(
                            iv_date      = ls_data-end_date
                            iv_time      = ls_data-end_time
                            iv_timezone  = /evora/if_ea_constants=>cv_timezone_utc
                        ).
        DATA(lv_internal_combo_key) = /evora/cl_ea_service_facade=>get_ready_to_run(
                                        iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                        iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                      ).
        "get Odata handler
        DATA(lo_odata_handler) =
          /evora/cl_ea_service_facade=>get_ui_customizing( )->get_odata_handler(
            iv_internal_combo_key = lv_internal_combo_key
        ).
        "convert data to Core data compatible format
        " Especially with Anlytical table view, convert the time to single Unit of measure
        "else analytical table sum will fail
*        lo_odata_handler->map_odata_to_core(
*          IMPORTING
*            iv_internal_combo_key = lv_internal_combo_key    " EvoApproval: Business Scenario & Object Type Key Combination
*          CHANGING
*            cs_data               = ls_data
*        ).
        lo_odata_handler->map_odata_to_core(
          CHANGING
            cs_data = ls_data
        ).
*        ASSIGN COMPONENT 'REQUEST_GUID' OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<lv_guid>).
        DATA(lo_data_buffer) = /evora/cl_ea_service_facade=>get_data_buffer( iv_internal_combo_key = lv_internal_combo_key ).
        DATA(lo_approval_object) = CAST /evora/if_ea_object( lo_data_buffer->get_by_key( iv_object_key = ls_data-request_guid ) ).

        IF lo_approval_object IS BOUND.
          DATA(lv_request_description) = lo_approval_object->get_description( ).
          DATA(lv_request_id)  = lo_approval_object->get_request_id( ).
          DATA(ls_objkey_info) = lo_approval_object->update( ls_data ).

          IF ls_objkey_info-operation_success = abap_false.
            MESSAGE e004(/evora/ea_msg_cls)
            WITH lv_request_id
            INTO lv_message.
            /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_sy_msg( ).
          ELSE.
            MESSAGE s005(/evora/ea_msg_cls)
              WITH lv_request_id
                   lv_request_description
              INTO lv_message.
            /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_sy_msg( ).
          ENDIF.
        ENDIF.
      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex).
        /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_exception( lo_ex ).
        mo_context->get_message_container( )->add_messages_from_bapi( /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages( ) ).
    ENDTRY.


*      Move the backend data to frontend data
    /evora/cl_et2_odata_utility=>gv_entity_call = 'X'.
    lo_odata_handler->map_to_odata_entity(
      EXPORTING
        iv_internal_combo_key = lv_internal_combo_key
        is_data               = ls_data    " Backend data
      IMPORTING
        es_entity             = er_entity    " Frontend data
      ).
    CLEAR /evora/cl_et2_odata_utility=>gv_entity_call.
  ENDMETHOD.


  METHOD changelogset_get_entityset.
    DATA lt_sorting     TYPE /evora/ea_tt_sort_cond.
    DATA lv_count       TYPE i.
    DATA lv_max_rows    TYPE i.
    DATA lr_data        TYPE REF TO data.
    DATA lt_entityset   TYPE /evora/cl_et2_main_mpc=>tt_changelog.


    REFRESH:  et_entityset.
    CLEAR     es_response_context.
    TRY .
        DATA(lv_internal_combo_key) = /evora/cl_ea_service_facade=>get_ready_to_run(
                                        iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                        iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                      ).

        DATA(lo_data_buffer) = /evora/cl_ea_service_facade=>get_data_buffer( lv_internal_combo_key ).

**       convert the odata filter conditions back to the data buffer filter conditions
*        need to check whether we are using it
        DATA(lt_conditions) = /evora/cl_et2_odata_utility=>convert_odata_2_internal_flt(
                                io_tech_request_context->get_filter( )->get_filter_select_options( ) ).

        IF it_key_tab IS NOT INITIAL AND iv_source_name = 'Approval'.
          lt_conditions = CORRESPONDING #(
                            BASE ( lt_conditions )
                              VALUE /evora/ea_tt_filter_cond(
                                FOR <ls_key_tab> IN it_key_tab
                                  WHERE ( name = 'GeneratedId' )
                                  technical_fieldname = /evora/if_et2_timeconfirm=>cs_field_names-request_guid
                                  filter = VALUE #( (
                                              sign   = /evora/if_et2_constants=>cs_range_sign-including
                                              option = /evora/if_et2_constants=>cs_range_option-equals
                                              low    = lo_data_buffer->query_single(
                                                         it_conditions = VALUE #( ( technical_fieldname = /evora/if_et2_timeconfirm=>cs_field_names-request_id
                                                                                    filter = VALUE #( ( sign   = /evora/if_et2_constants=>cs_range_sign-including
                                                                                                        option = /evora/if_et2_constants=>cs_range_option-equals
                                                                                                        low    = CONV /evora/ea_e_request_id( <ls_key_tab>-value+14(12) )
                                                                                                     ) )
                                                                         ) )
                                                       )->get_key( )
                                         	 ) )
                            ( ) ) )  .
        ENDIF.

*     pass select to API layer
        IF io_tech_request_context->has_count( ) = abap_true.
          lo_data_buffer->query_change_log(
            EXPORTING
              it_conditions            = lt_conditions
              it_sorting               = lt_sorting
              iv_count_only            = abap_true
              iv_object_class          = /evora/if_et2_constants=>cv_change_log
            IMPORTING
              ev_nr_of_rows            = lv_count
          ).
        ELSE.
          lo_data_buffer->query_change_log(
            EXPORTING
              it_conditions            = lt_conditions
              it_sorting               = lt_sorting
              iv_object_class          = /evora/if_et2_constants=>cv_change_log
              iv_count_only            = abap_false
            IMPORTING
              er_data                  = lr_data
          ).
        ENDIF.
      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex)   ##NEEDED.
        CLEAR et_entityset.
        mo_context->get_message_container( )->add_messages_from_bapi( /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages( ) ).
        RETURN.
    ENDTRY.

    IF io_tech_request_context->has_count( ) = abap_false.
      IF lr_data IS NOT INITIAL.
        /evora/cl_et2_odata_utility=>prepare_change_log(
          EXPORTING
            iv_internal_combo_key = lv_internal_combo_key
            ir_data               = lr_data
          IMPORTING
            et_ui_change_log      = et_entityset
        ).
      ENDIF.
    ELSE.
      es_response_context-count = lv_count.
    ENDIF.

  ENDMETHOD.


  METHOD daydisplayset_get_entity.
    TRY.
        super->daydisplayset_get_entity(
          EXPORTING
            iv_entity_name          = iv_entity_name
            iv_entity_set_name      = iv_entity_set_name
            iv_source_name          = iv_source_name
            it_key_tab              = it_key_tab
            io_request_object       = io_request_object
            io_tech_request_context = io_tech_request_context
            it_navigation_path      = it_navigation_path
          IMPORTING
            er_entity               = er_entity
            es_response_context     = es_response_context ).
      CATCH /iwbep/cx_mgw_busi_exception .
      CATCH /iwbep/cx_mgw_tech_exception .
    ENDTRY.
  ENDMETHOD.


  METHOD daydisplayset_get_entityset.
    TRY.
        super->daydisplayset_get_entityset(
          EXPORTING
            iv_entity_name           = iv_entity_name
            iv_entity_set_name       = iv_entity_set_name
            iv_source_name           = iv_source_name
            it_filter_select_options = it_filter_select_options
            is_paging                = is_paging
            it_key_tab               = it_key_tab
            it_navigation_path       = it_navigation_path
            it_order                 = it_order
            iv_filter_string         = iv_filter_string
            iv_search_string         = iv_search_string
            io_tech_request_context  = io_tech_request_context
          IMPORTING
            et_entityset             = et_entityset
            es_response_context      = es_response_context ).
      CATCH /iwbep/cx_mgw_busi_exception .
      CATCH /iwbep/cx_mgw_tech_exception .
    ENDTRY.
  ENDMETHOD.


  METHOD durationunitset_get_entityset.
*-------------------------------------------------------------
*  Data declaration
*-------------------------------------------------------------
    DATA lo_filter TYPE  REF TO /iwbep/if_mgw_req_filter.
    DATA lt_filter_select_options TYPE /iwbep/t_mgw_select_option.
    DATA lv_filter_str TYPE string.
    DATA lv_max_hits TYPE i.
    DATA ls_paging TYPE /iwbep/s_mgw_paging.
    DATA ls_converted_keys LIKE LINE OF et_entityset.
    DATA ls_message TYPE bapiret2.
    DATA lt_selopt TYPE ddshselops.
    DATA ls_selopt LIKE LINE OF lt_selopt.
    DATA ls_filter TYPE /iwbep/s_mgw_select_option.
    DATA ls_filter_range TYPE /iwbep/s_cod_select_option.
    DATA lr_msehl LIKE RANGE OF ls_converted_keys-msehl.
    DATA ls_msehl LIKE LINE OF lr_msehl.
    DATA lr_msehi LIKE RANGE OF ls_converted_keys-msehi.
    DATA ls_msehi LIKE LINE OF lr_msehi.
    DATA lt_result_list TYPE /iwbep/if_sb_gendpc_shlp_data=>tt_result_list.
    DATA lv_next TYPE i VALUE 1.
    DATA ls_entityset LIKE LINE OF et_entityset.
    DATA ls_result_list_next LIKE LINE OF lt_result_list.
    DATA ls_result_list LIKE LINE OF lt_result_list.

*-------------------------------------------------------------
*  Map the runtime request to the Search Help select option - Only mapped attributes
*-------------------------------------------------------------
* Get all input information from the technical request context object
* Since DPC works with internal property names and runtime API interface holds external property names
* the process needs to get the all needed input information from the technical request context object
* Get filter or select option information
    lo_filter = io_tech_request_context->get_filter( ).
    lt_filter_select_options = lo_filter->get_filter_select_options( ).
    lv_filter_str = lo_filter->get_filter_string( ).

* Check if the supplied filter is supported by standard gateway runtime process
    IF  lv_filter_str            IS NOT INITIAL
    AND lt_filter_select_options IS INITIAL.
      " If the string of the Filter System Query Option is not automatically converted into
      " filter option table (lt_filter_select_options), then the filtering combination is not supported
      " Log message in the application log
      me->/iwbep/if_sb_dpc_comm_services~log_message(
        EXPORTING
          iv_msg_type   = 'E'
          iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
          iv_msg_number = 025 ).
      " Raise Exception
      RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
        EXPORTING
          textid = /iwbep/cx_mgw_tech_exception=>internal_error.
    ENDIF.

* Get key table information
    io_tech_request_context->get_converted_source_keys(
      IMPORTING
        es_key_values  = ls_converted_keys ).

    ls_paging-top = io_tech_request_context->get_top( ).
    ls_paging-skip = io_tech_request_context->get_skip( ).

    " Calculate the number of max hits to be fetched from the function module
    " The lv_max_hits value is a summary of the Top and Skip values
    IF ls_paging-top > 0.
      lv_max_hits = is_paging-top + is_paging-skip.
    ENDIF.

* Maps filter table lines to the Search Help select option table
    LOOP AT lt_filter_select_options INTO ls_filter.

      CASE ls_filter-property.
        WHEN 'MSEHL'.              " Equivalent to 'Msehl' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_msehl ).

          LOOP AT lr_msehl INTO ls_msehl.
            ls_selopt-sign = ls_msehl-sign.
            ls_selopt-option = ls_msehl-option.
            ls_selopt-low = ls_msehl-low.
            ls_selopt-high = ls_msehl-high.
            ls_selopt-shlpfield = 'MSEHL'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_EFFORT_UNITS'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.
        WHEN 'MSEHI'.              " Equivalent to 'Msehi' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_msehi ).

          LOOP AT lr_msehi INTO ls_msehi.
            ls_selopt-sign = ls_msehi-sign.
            ls_selopt-option = ls_msehi-option.
            ls_selopt-low = ls_msehi-low.
            ls_selopt-high = ls_msehi-high.
            ls_selopt-shlpfield = 'MSEHI'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_EFFORT_UNITS'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.

        WHEN OTHERS.
          " Log message in the application log
          me->/iwbep/if_sb_dpc_comm_services~log_message(
            EXPORTING
              iv_msg_type   = 'E'
              iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
              iv_msg_number = 020
              iv_msg_v1     = ls_filter-property ).
          " Raise Exception
          RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
            EXPORTING
              textid = /iwbep/cx_mgw_tech_exception=>internal_error.
      ENDCASE.
    ENDLOOP.
    ls_selopt-sign = /evora/if_et2_constants=>cs_range_sign-including.
    ls_selopt-option = /evora/if_et2_constants=>cs_range_option-equals.
    ls_selopt-low = sy-langu.
    ls_selopt-shlpfield = 'SPRAS'.
    ls_selopt-shlpname = '/EVORA/ET2_SH_EFFORT_UNITS'.
    APPEND ls_selopt TO lt_selopt.
    CLEAR ls_selopt.
*-------------------------------------------------------------
*  Call to Search Help get values mechanism
*-------------------------------------------------------------
* Get search help values

*    DATA(lo_sh_data) = /iwbep/cl_sb_shlp_data_factory=>get_sh_data_obj( ).
*
*    lo_sh_data->/iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
*      EXPORTING
*        iv_shlp_name  = '/EVORA/ET2_SH_EFFORT_UNITS'
*        iv_maxrows  = lv_max_hits
*        iv_sort = 'X'
*        iv_call_shlt_exit = 'X'
*        it_selopt = lt_selopt
*      IMPORTING
*        et_return_list = lt_result_list
*        es_message = ls_message ).
    me->/iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
      EXPORTING
        iv_shlp_name = '/EVORA/ET2_SH_EFFORT_UNITS'
        iv_maxrows = lv_max_hits
        iv_sort = 'X'
        iv_call_shlt_exit = 'X'
        it_selopt = lt_selopt
      IMPORTING
        et_return_list = lt_result_list
        es_message = ls_message ).

*-------------------------------------------------------------
*  Map the Search Help returned results to the caller interface - Only mapped attributes
*-------------------------------------------------------------
    IF ls_message IS NOT INITIAL.
* Call RFC call exception handling
      me->/iwbep/if_sb_dpc_comm_services~rfc_save_log(
        EXPORTING
          is_return      = ls_message
          iv_entity_type = iv_entity_name
          it_key_tab     = it_key_tab ).
    ENDIF.

    CLEAR et_entityset.

    LOOP AT lt_result_list INTO ls_result_list
      WHERE record_number > ls_paging-skip.

      " Move SH results to GW request responce table
      lv_next = sy-tabix + 1. " next loop iteration
      CASE ls_result_list-field_name.
        WHEN 'MSEHI'.
          ls_entityset-msehi = ls_result_list-field_value.
        WHEN 'MSEHL'.
          ls_entityset-msehl = ls_result_list-field_value.
      ENDCASE.

      " Check if the next line in the result list is a new record
      READ TABLE lt_result_list INTO ls_result_list_next INDEX lv_next.
      IF sy-subrc <> 0
      OR ls_result_list-record_number <> ls_result_list_next-record_number.
        " Save the collected SH result in the GW request table
        APPEND ls_entityset TO et_entityset.
        CLEAR: ls_result_list_next, ls_entityset.
      ENDIF.

    ENDLOOP.

  ENDMETHOD.


  method DYNAMICTILESET_GET_ENTITY.

    DATA: lv_lookup_title TYPE /evora/ea_e_dyn_tile_title,
          lo_provider     TYPE REF TO /evora/if_ea_dynamic_tile.

    CLEAR: er_entity,
           es_response_context .

*   go by the title as a key
    IF it_key_tab IS INITIAL.
      RETURN.
    ENDIF.

    LOOP AT it_key_tab ASSIGNING FIELD-SYMBOL(<ls_key_tab>).
      IF <ls_key_tab>-name = 'title'.
        lv_lookup_title = <ls_key_tab>-value.
        EXIT.
      ENDIF.
    ENDLOOP.

    IF lv_lookup_title IS INITIAL.
      RETURN.
    ENDIF.

*   go for customizing to find the proper handler
    DATA(lv_provider_class) =
      /evora/cl_ea_service_facade=>get_ui_customizing( )->get_dyn_tile_provider(
        iv_title              = lv_lookup_title
        iv_internal_combo_key = 'EVOTIME-TCONFIRM'
    ).

    IF lv_provider_class IS INITIAL.
      RETURN.
    ENDIF.

    TRY.
        CREATE OBJECT lo_provider TYPE (lv_provider_class).
        er_entity = lo_provider->get_dynamic_tile_data( iv_title              = lv_lookup_title
                                                        iv_internal_combo_key = 'EVOTIME-TCONFIRM' ).
      CATCH cx_sy_move_cast_error ##NO_HANDLER.
    ENDTRY.



  endmethod.


  METHOD if_sadl_gw_query_control~set_query_options.
    TRY.
        super->if_sadl_gw_query_control~set_query_options(
          EXPORTING
            iv_entity_set    = iv_entity_set
            io_query_options = io_query_options ).
      CATCH cx_sadl_gw_contract_violation .
      CATCH /iwbep/cx_mgw_busi_exception .
      CATCH /iwbep/cx_mgw_tech_exception .
    ENDTRY.
    "set Effort as sum
    io_query_options->set_aggregation( VALUE #(
          ( element = 'EFFORT' alias = 'EFFORT' type = if_sadl_gw_query_options=>co_aggregation_type-sum )
          ) ).

  ENDMETHOD.


  METHOD plantset_get_entityset.
*-------------------------------------------------------------
*  Data declaration
*-------------------------------------------------------------
    DATA lo_filter TYPE  REF TO /iwbep/if_mgw_req_filter.
    DATA lt_filter_select_options TYPE /iwbep/t_mgw_select_option.
    DATA lv_filter_str TYPE string.
    DATA lv_max_hits TYPE i.
    DATA ls_paging TYPE /iwbep/s_mgw_paging.
    DATA ls_converted_keys LIKE LINE OF et_entityset.
    DATA ls_message TYPE bapiret2.
    DATA lt_selopt TYPE ddshselops.
    DATA ls_selopt LIKE LINE OF lt_selopt.
    DATA ls_filter TYPE /iwbep/s_mgw_select_option.
    DATA ls_filter_range TYPE /iwbep/s_cod_select_option.
    DATA lr_name2 LIKE RANGE OF ls_converted_keys-name2.
    DATA ls_name2 LIKE LINE OF lr_name2.
    DATA lr_name1 LIKE RANGE OF ls_converted_keys-name1.
    DATA ls_name1 LIKE LINE OF lr_name1.
    DATA lr_werks LIKE RANGE OF ls_converted_keys-werks.
    DATA ls_werks LIKE LINE OF lr_werks.
    DATA lt_result_list TYPE /iwbep/if_sb_gendpc_shlp_data=>tt_result_list.
    DATA lv_next TYPE i VALUE 1.
    DATA ls_entityset LIKE LINE OF et_entityset.
    DATA ls_result_list_next LIKE LINE OF lt_result_list.
    DATA ls_result_list LIKE LINE OF lt_result_list.

*-------------------------------------------------------------
*  Map the runtime request to the Search Help select option - Only mapped attributes
*-------------------------------------------------------------
* Get all input information from the technical request context object
* Since DPC works with internal property names and runtime API interface holds external property names
* the process needs to get the all needed input information from the technical request context object
* Get filter or select option information
    lo_filter = io_tech_request_context->get_filter( ).
    lt_filter_select_options = lo_filter->get_filter_select_options( ).
    lv_filter_str = lo_filter->get_filter_string( ).

* Check if the supplied filter is supported by standard gateway runtime process
    IF  lv_filter_str            IS NOT INITIAL
    AND lt_filter_select_options IS INITIAL.
      " If the string of the Filter System Query Option is not automatically converted into
      " filter option table (lt_filter_select_options), then the filtering combination is not supported
      " Log message in the application log
      me->/iwbep/if_sb_dpc_comm_services~log_message(
        EXPORTING
          iv_msg_type   = 'E'
          iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
          iv_msg_number = 025 ).
      " Raise Exception
      RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
        EXPORTING
          textid = /iwbep/cx_mgw_tech_exception=>internal_error.
    ENDIF.

* Get key table information
    io_tech_request_context->get_converted_source_keys(
      IMPORTING
        es_key_values  = ls_converted_keys ).

    ls_paging-top = io_tech_request_context->get_top( ).
    ls_paging-skip = io_tech_request_context->get_skip( ).

    " Calculate the number of max hits to be fetched from the function module
    " The lv_max_hits value is a summary of the Top and Skip values
    IF ls_paging-top > 0.
      lv_max_hits = is_paging-top + is_paging-skip.
    ENDIF.

* Maps filter table lines to the Search Help select option table
    LOOP AT lt_filter_select_options INTO ls_filter.

      CASE ls_filter-property.
        WHEN 'NAME2'.              " Equivalent to 'Name2' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_name2 ).

          LOOP AT lr_name2 INTO ls_name2.
            ls_selopt-sign = ls_name2-sign.
            ls_selopt-option = ls_name2-option.
            ls_selopt-low = ls_name2-low.
            ls_selopt-high = ls_name2-high.
            ls_selopt-shlpfield = 'NAME2'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_PLANT'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.
        WHEN 'NAME1'.              " Equivalent to 'Name1' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_name1 ).

          LOOP AT lr_name1 INTO ls_name1.
            ls_selopt-sign = ls_name1-sign.
            ls_selopt-option = ls_name1-option.
            ls_selopt-low = ls_name1-low.
            ls_selopt-high = ls_name1-high.
            ls_selopt-shlpfield = 'NAME1'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_PLANT'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.
        WHEN 'WERKS'.              " Equivalent to 'PlantCode' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_werks ).

          LOOP AT lr_werks INTO ls_werks.
            ls_selopt-sign = ls_werks-sign.
            ls_selopt-option = ls_werks-option.
            ls_selopt-low = ls_werks-low.
            ls_selopt-high = ls_werks-high.
            ls_selopt-shlpfield = 'WERKS'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_PLANT'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.

        WHEN OTHERS.
          " Log message in the application log
          me->/iwbep/if_sb_dpc_comm_services~log_message(
            EXPORTING
              iv_msg_type   = 'E'
              iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
              iv_msg_number = 020
              iv_msg_v1     = ls_filter-property ).
          " Raise Exception
          RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
            EXPORTING
              textid = /iwbep/cx_mgw_tech_exception=>internal_error.
      ENDCASE.
    ENDLOOP.
    ls_selopt-sign = /evora/if_et2_constants=>cs_range_sign-including.
    ls_selopt-option = /evora/if_et2_constants=>cs_range_option-equals.
    ls_selopt-low = sy-langu.
    ls_selopt-shlpfield = 'SPRAS'.
    ls_selopt-shlpname = '/EVORA/ET2_SH_PLANT'.
    APPEND ls_selopt TO lt_selopt.
    CLEAR ls_selopt.
*-------------------------------------------------------------
*  Call to Search Help get values mechanism
*-------------------------------------------------------------
*    DATA(lo_sh_data) = /iwbep/cl_sb_shlp_data_factory=>get_sh_data_obj( ).
*
*    lo_sh_data->/iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
*      EXPORTING
*        iv_shlp_name  = '/EVORA/ET2_SH_PLANT'
*        iv_maxrows  = lv_max_hits
*        iv_sort = 'X'
*        iv_call_shlt_exit = 'X'
*        it_selopt = lt_selopt
*      IMPORTING
*        et_return_list = lt_result_list
*        es_message = ls_message ).
* Get search help values
    /iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
      EXPORTING
        iv_shlp_name = '/EVORA/ET2_SH_PLANT'
        iv_maxrows = lv_max_hits
        iv_sort = 'X'
        iv_call_shlt_exit = 'X'
        it_selopt = lt_selopt
      IMPORTING
        et_return_list = lt_result_list
        es_message = ls_message ).

*-------------------------------------------------------------
*  Map the Search Help returned results to the caller interface - Only mapped attributes
*-------------------------------------------------------------
    IF ls_message IS NOT INITIAL.
* Call RFC call exception handling
      me->/iwbep/if_sb_dpc_comm_services~rfc_save_log(
        EXPORTING
          is_return      = ls_message
          iv_entity_type = iv_entity_name
          it_key_tab     = it_key_tab ).
    ENDIF.

    CLEAR et_entityset.

    LOOP AT lt_result_list INTO ls_result_list
      WHERE record_number > ls_paging-skip.

      " Move SH results to GW request responce table
      lv_next = sy-tabix + 1. " next loop iteration
      CASE ls_result_list-field_name.
        WHEN 'WERKS'.
          ls_entityset-werks = ls_result_list-field_value.
        WHEN 'NAME1'.
          ls_entityset-name1 = ls_result_list-field_value.
        WHEN 'NAME2'.
          ls_entityset-name2 = ls_result_list-field_value.
      ENDCASE.

      " Check if the next line in the result list is a new record
      READ TABLE lt_result_list INTO ls_result_list_next INDEX lv_next.
      IF sy-subrc <> 0
      OR ls_result_list-record_number <> ls_result_list_next-record_number.
        " Save the collected SH result in the GW request table
        APPEND ls_entityset TO et_entityset.
        CLEAR: ls_result_list_next, ls_entityset.
      ENDIF.

    ENDLOOP.

  ENDMETHOD.


  METHOD report_messages.

    TRY.
*       check for context is required as this is the source of the message container
        IF mo_context IS BOUND.

*         push all messages from message buffer into odata message container
          /iwbep/if_mgw_conv_srv_runtime~get_message_container( )->add_messages_from_bapi(
            it_bapi_messages          = /evora/cl_ea_service_facade=>get_message_buffer( )->get_messages( )
            iv_determine_leading_msg  = /iwbep/if_message_container=>gcs_leading_msg_search_option-none
            iv_add_to_response_header = abap_true ).

        ENDIF.
      CATCH cx_sy_ref_is_initial ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.


  METHOD statusset_get_entityset.

*-------------------------------------------------------------
*  Data declaration
*-------------------------------------------------------------
    DATA lo_filter TYPE  REF TO /iwbep/if_mgw_req_filter.
    DATA lt_filter_select_options TYPE /iwbep/t_mgw_select_option.
    DATA lv_filter_str TYPE string.
    DATA lv_max_hits TYPE i.
    DATA ls_paging TYPE /iwbep/s_mgw_paging.
    DATA ls_converted_keys LIKE LINE OF et_entityset.
    DATA ls_message TYPE bapiret2.
    DATA lt_selopt TYPE ddshselops.
    DATA ls_selopt LIKE LINE OF lt_selopt.
    DATA ls_filter TYPE /iwbep/s_mgw_select_option.
    DATA ls_filter_range TYPE /iwbep/s_cod_select_option.
    DATA lr_txt30 LIKE RANGE OF ls_converted_keys-txt30.
    DATA ls_txt30 LIKE LINE OF lr_txt30.
    DATA lr_txt04 LIKE RANGE OF ls_converted_keys-txt04.
    DATA ls_txt04 LIKE LINE OF lr_txt04.
    DATA lt_result_list TYPE /iwbep/if_sb_gendpc_shlp_data=>tt_result_list.
    DATA lv_next TYPE i VALUE 1.
    DATA ls_entityset LIKE LINE OF et_entityset.
    DATA ls_result_list_next LIKE LINE OF lt_result_list.
    DATA ls_result_list LIKE LINE OF lt_result_list.

*-------------------------------------------------------------
*  Map the runtime request to the Search Help select option - Only mapped attributes
*-------------------------------------------------------------
* Get all input information from the technical request context object
* Since DPC works with internal property names and runtime API interface holds external property names
* the process needs to get the all needed input information from the technical request context object
* Get filter or select option information
    lo_filter = io_tech_request_context->get_filter( ).
    lt_filter_select_options = lo_filter->get_filter_select_options( ).
    lv_filter_str = lo_filter->get_filter_string( ).

* Check if the supplied filter is supported by standard gateway runtime process
    IF  lv_filter_str            IS NOT INITIAL
    AND lt_filter_select_options IS INITIAL.
      " If the string of the Filter System Query Option is not automatically converted into
      " filter option table (lt_filter_select_options), then the filtering combination is not supported
      " Log message in the application log
      me->/iwbep/if_sb_dpc_comm_services~log_message(
        EXPORTING
          iv_msg_type   = 'E'
          iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
          iv_msg_number = 025 ).
      " Raise Exception
      RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
        EXPORTING
          textid = /iwbep/cx_mgw_tech_exception=>internal_error.
    ENDIF.

* Get key table information
    io_tech_request_context->get_converted_source_keys(
      IMPORTING
        es_key_values  = ls_converted_keys ).

    ls_paging-top = io_tech_request_context->get_top( ).
    ls_paging-skip = io_tech_request_context->get_skip( ).

    " Calculate the number of max hits to be fetched from the function module
    " The lv_max_hits value is a summary of the Top and Skip values
    IF ls_paging-top > 0.
      lv_max_hits = is_paging-top + is_paging-skip.
    ENDIF.

* Maps filter table lines to the Search Help select option table
    LOOP AT lt_filter_select_options INTO ls_filter.

      CASE ls_filter-property.
        WHEN 'TXT30'.              " Equivalent to 'UserStatusDesc' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_txt30 ).

          LOOP AT lr_txt30 INTO ls_txt30.
            ls_selopt-sign = ls_txt30-sign.
            ls_selopt-option = ls_txt30-option.
            ls_selopt-low = ls_txt30-low.
            ls_selopt-high = ls_txt30-high.
            ls_selopt-shlpfield = 'TXT30'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_USERSTATUS_TEXTS'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.
        WHEN 'TXT04'.              " Equivalent to 'UserStatus' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_txt04 ).

          LOOP AT lr_txt04 INTO ls_txt04.
            ls_selopt-sign = ls_txt04-sign.
            ls_selopt-option = ls_txt04-option.
            ls_selopt-low = ls_txt04-low.
            ls_selopt-high = ls_txt04-high.
            ls_selopt-shlpfield = 'TXT04'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_USERSTATUS_TEXTS'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.

        WHEN OTHERS.
          " Log message in the application log
          me->/iwbep/if_sb_dpc_comm_services~log_message(
            EXPORTING
              iv_msg_type   = 'E'
              iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
              iv_msg_number = 020
              iv_msg_v1     = ls_filter-property ).
          " Raise Exception
          RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
            EXPORTING
              textid = /iwbep/cx_mgw_tech_exception=>internal_error.
      ENDCASE.
    ENDLOOP.
    ls_selopt-sign = /evora/if_et2_constants=>cs_range_sign-including.
    ls_selopt-option = /evora/if_et2_constants=>cs_range_option-equals.
    ls_selopt-low = sy-langu.
    ls_selopt-shlpfield = 'SPRAS'.
    ls_selopt-shlpname = '/EVORA/ET2_SH_USERSTATUS_TEXTS'.
    APPEND ls_selopt TO lt_selopt.
    CLEAR ls_selopt.
    "get internal combo key
    /evora/cl_ea_service_facade=>get_customizing( )->get_internal_combo_key(
      EXPORTING
        iv_business_scenario   = /evora/if_et2_constants=>cv_business_scenario  " EvoTime: Config key
        iv_object_type         = /evora/if_et2_constants=>cs_object_types-t_confirm
      RECEIVING
        rv_internal_combo_key  = DATA(lv_internal_combo_key)     " Parameter values
    ).
    "get status profile
    /evora/cl_ea_service_facade=>get_customizing( )->get_status_profile(
      EXPORTING
        iv_internal_combo_key   = lv_internal_combo_key  " EvoTime: Config key
      RECEIVING
        rv_status_profile       = DATA(lv_status_profile)     " Parameter values
    ).

    IF sy-subrc = 0.
      ls_selopt-sign = /evora/if_et2_constants=>cs_range_sign-including.
      ls_selopt-option = /evora/if_et2_constants=>cs_range_option-equals.
      ls_selopt-low = lv_status_profile.
      ls_selopt-shlpfield = 'STSMA'.
      ls_selopt-shlpname = '/EVORA/ET2_SH_USERSTATUS_TEXTS'.
      APPEND ls_selopt TO lt_selopt.
      CLEAR ls_selopt.
    ENDIF.
*-------------------------------------------------------------
*  Call to Search Help get values mechanism
*-------------------------------------------------------------
*    DATA(lo_sh_data) = /iwbep/cl_sb_shlp_data_factory=>get_sh_data_obj( ).
*
*    lo_sh_data->/iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
*      EXPORTING
*        iv_shlp_name  = '/EVORA/ET2_SH_USERSTATUS_TEXTS'
*        iv_maxrows  = lv_max_hits
*        iv_sort = 'X'
*        iv_call_shlt_exit = 'X'
*        it_selopt = lt_selopt
*      IMPORTING
*        et_return_list = lt_result_list
*        es_message = ls_message ).
* Get search help values
    /iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
      EXPORTING
        iv_shlp_name = '/EVORA/ET2_SH_USERSTATUS_TEXTS'
        iv_maxrows = lv_max_hits
        iv_sort = 'X'
        iv_call_shlt_exit = 'X'
        it_selopt = lt_selopt
      IMPORTING
        et_return_list = lt_result_list
        es_message = ls_message ).

*-------------------------------------------------------------
*  Map the Search Help returned results to the caller interface - Only mapped attributes
*-------------------------------------------------------------
    IF ls_message IS NOT INITIAL.
* Call RFC call exception handling
      me->/iwbep/if_sb_dpc_comm_services~rfc_save_log(
        EXPORTING
          is_return      = ls_message
          iv_entity_type = iv_entity_name
          it_key_tab     = it_key_tab ).
    ENDIF.

    CLEAR et_entityset.

    LOOP AT lt_result_list INTO ls_result_list
      WHERE record_number > ls_paging-skip.

      " Move SH results to GW request responce table
      lv_next = sy-tabix + 1. " next loop iteration
      CASE ls_result_list-field_name.
        WHEN 'TXT04'.
          ls_entityset-txt04 = ls_result_list-field_value.
        WHEN 'TXT30'.
          ls_entityset-txt30 = ls_result_list-field_value.
      ENDCASE.

      " Check if the next line in the result list is a new record
      READ TABLE lt_result_list INTO ls_result_list_next INDEX lv_next.
      IF sy-subrc <> 0
      OR ls_result_list-record_number <> ls_result_list_next-record_number.
        " Save the collected SH result in the GW request table
        APPEND ls_entityset TO et_entityset.
        CLEAR: ls_result_list_next, ls_entityset.
      ENDIF.

    ENDLOOP.



  ENDMETHOD.


  METHOD workcenterset_get_entityset.
*-------------------------------------------------------------
*  Data declaration
*-------------------------------------------------------------
    DATA lo_filter TYPE  REF TO /iwbep/if_mgw_req_filter.
    DATA lt_filter_select_options TYPE /iwbep/t_mgw_select_option.
    DATA lv_filter_str TYPE string.
    DATA lv_max_hits TYPE i.
    DATA ls_paging TYPE /iwbep/s_mgw_paging.
    DATA ls_converted_keys LIKE LINE OF et_entityset.
    DATA ls_message TYPE bapiret2.
    DATA lt_selopt TYPE ddshselops.
    DATA ls_selopt LIKE LINE OF lt_selopt.
    DATA ls_filter TYPE /iwbep/s_mgw_select_option.
    DATA ls_filter_range TYPE /iwbep/s_cod_select_option.
    DATA lr_ktext LIKE RANGE OF ls_converted_keys-ktext.
    DATA ls_ktext LIKE LINE OF lr_ktext.
    DATA lr_arbpl LIKE RANGE OF ls_converted_keys-arbpl.
    DATA ls_arbpl LIKE LINE OF lr_arbpl.
    DATA lr_werks LIKE RANGE OF ls_converted_keys-werks.
    DATA ls_werks LIKE LINE OF lr_werks.
    DATA lt_result_list TYPE /iwbep/if_sb_gendpc_shlp_data=>tt_result_list.
    DATA lv_next TYPE i VALUE 1.
    DATA ls_entityset LIKE LINE OF et_entityset.
    DATA ls_result_list_next LIKE LINE OF lt_result_list.
    DATA ls_result_list LIKE LINE OF lt_result_list.

*-------------------------------------------------------------
*  Map the runtime request to the Search Help select option - Only mapped attributes
*-------------------------------------------------------------
* Get all input information from the technical request context object
* Since DPC works with internal property names and runtime API interface holds external property names
* the process needs to get the all needed input information from the technical request context object
* Get filter or select option information
    lo_filter = io_tech_request_context->get_filter( ).
    lt_filter_select_options = lo_filter->get_filter_select_options( ).
    lv_filter_str = lo_filter->get_filter_string( ).

* Check if the supplied filter is supported by standard gateway runtime process
    IF  lv_filter_str            IS NOT INITIAL
    AND lt_filter_select_options IS INITIAL.
      " If the string of the Filter System Query Option is not automatically converted into
      " filter option table (lt_filter_select_options), then the filtering combination is not supported
      " Log message in the application log
      me->/iwbep/if_sb_dpc_comm_services~log_message(
        EXPORTING
          iv_msg_type   = 'E'
          iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
          iv_msg_number = 025 ).
      " Raise Exception
      RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
        EXPORTING
          textid = /iwbep/cx_mgw_tech_exception=>internal_error.
    ENDIF.

* Get key table information
    io_tech_request_context->get_converted_source_keys(
      IMPORTING
        es_key_values  = ls_converted_keys ).

    ls_paging-top = io_tech_request_context->get_top( ).
    ls_paging-skip = io_tech_request_context->get_skip( ).

    " Calculate the number of max hits to be fetched from the function module
    " The lv_max_hits value is a summary of the Top and Skip values
    IF ls_paging-top > 0.
      lv_max_hits = is_paging-top + is_paging-skip.
    ENDIF.

* Maps filter table lines to the Search Help select option table
    LOOP AT lt_filter_select_options INTO ls_filter.

      CASE ls_filter-property.
        WHEN 'KTEXT'.              " Equivalent to 'WorkCenterDesc' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_ktext ).

          LOOP AT lr_ktext INTO ls_ktext.
            ls_selopt-sign = ls_ktext-sign.
            ls_selopt-option = ls_ktext-option.
            ls_selopt-low = ls_ktext-low.
            ls_selopt-high = ls_ktext-high.
            ls_selopt-shlpfield = 'KTEXT'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_WORKCENTER'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.
        WHEN 'ARBPL'.              " Equivalent to 'WorkCenterCode' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_arbpl ).

          LOOP AT lr_arbpl INTO ls_arbpl.
            ls_selopt-sign = ls_arbpl-sign.
            ls_selopt-option = ls_arbpl-option.
            ls_selopt-low = ls_arbpl-low.
            ls_selopt-high = ls_arbpl-high.
            ls_selopt-shlpfield = 'ARBPL'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_WORKCENTER'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.
        WHEN 'WERKS'.              " Equivalent to 'Plant' property in the service
          lo_filter->convert_select_option(
            EXPORTING
              is_select_option = ls_filter
            IMPORTING
              et_select_option = lr_werks ).

          LOOP AT lr_werks INTO ls_werks.
            ls_selopt-sign = ls_werks-sign.
            ls_selopt-option = ls_werks-option.
            ls_selopt-low = ls_werks-low.
            ls_selopt-high = ls_werks-high.
            ls_selopt-shlpfield = 'WERKS'.
            ls_selopt-shlpname = '/EVORA/ET2_SH_WORKCENTER'.
            APPEND ls_selopt TO lt_selopt.
            CLEAR ls_selopt.
          ENDLOOP.

        WHEN OTHERS.
          " Log message in the application log
          me->/iwbep/if_sb_dpc_comm_services~log_message(
            EXPORTING
              iv_msg_type   = 'E'
              iv_msg_id     = '/IWBEP/MC_SB_DPC_ADM'
              iv_msg_number = 020
              iv_msg_v1     = ls_filter-property ).
          " Raise Exception
          RAISE EXCEPTION TYPE /iwbep/cx_mgw_tech_exception
            EXPORTING
              textid = /iwbep/cx_mgw_tech_exception=>internal_error.
      ENDCASE.
    ENDLOOP.
    ls_selopt-sign = /evora/if_et2_constants=>cs_range_sign-including.
    ls_selopt-option = /evora/if_et2_constants=>cs_range_option-equals.
    ls_selopt-low = sy-langu.
    ls_selopt-shlpfield = 'SPRAS'.
    ls_selopt-shlpname = '/EVORA/ET2_SH_USERSTATUS_TEXTS'.
    APPEND ls_selopt TO lt_selopt.
    CLEAR ls_selopt.
*-------------------------------------------------------------
*  Call to Search Help get values mechanism
*-------------------------------------------------------------
* Get search help values
*    me->/iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
*      EXPORTING
*        iv_shlp_name      = '/EVORA/ET2_SH_WORKCENTER'    " Name of a Search Help
*        iv_maxrows        = lv_max_hits "9999
*        iv_sort           = 'X' "SPACE    " DD: truth value
*        iv_call_shlt_exit = 'X'    " DD: truth value
*        it_selopt         = lt_selopt    " Selection Options for Search Helps
*      IMPORTING
*        et_return_list    = lt_result_list
*        es_message        = ls_message    " Return Parameter
*    ).

*    DATA(lo_sh_data) = /iwbep/cl_sb_shlp_data_factory=>get_sh_data_obj( ).
*
*    lo_sh_data->/iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
*      EXPORTING
*        iv_shlp_name  = '/EVORA/ET2_SH_WORKCENTER'
*        iv_maxrows  = lv_max_hits
*        iv_sort = 'X'
*        iv_call_shlt_exit = 'X'
*        it_selopt = lt_selopt
*      IMPORTING
*        et_return_list = lt_result_list
*        es_message = ls_message ).
    me->/iwbep/if_sb_gendpc_shlp_data~get_search_help_values(
      EXPORTING
        iv_shlp_name = '/EVORA/ET2_SH_WORKCENTER'
        iv_maxrows = lv_max_hits
        iv_sort = 'X'
        iv_call_shlt_exit = 'X'
        it_selopt = lt_selopt
      IMPORTING
        et_return_list = lt_result_list
        es_message = ls_message ).

*-------------------------------------------------------------
*  Map the Search Help returned results to the caller interface - Only mapped attributes
*-------------------------------------------------------------
    IF ls_message IS NOT INITIAL.
* Call RFC call exception handling
      me->/iwbep/if_sb_dpc_comm_services~rfc_save_log(
        EXPORTING
          is_return      = ls_message
          iv_entity_type = iv_entity_name
          it_key_tab     = it_key_tab ).
    ENDIF.

    CLEAR et_entityset.

    LOOP AT lt_result_list INTO ls_result_list
      WHERE record_number > ls_paging-skip.

      " Move SH results to GW request responce table
      lv_next = sy-tabix + 1. " next loop iteration
      CASE ls_result_list-field_name.
        WHEN 'WERKS'.
          ls_entityset-werks = ls_result_list-field_value.
        WHEN 'ARBPL'.
          ls_entityset-arbpl = ls_result_list-field_value.
        WHEN 'KTEXT'.
          ls_entityset-ktext = ls_result_list-field_value.
      ENDCASE.

      " Check if the next line in the result list is a new record
      READ TABLE lt_result_list INTO ls_result_list_next INDEX lv_next.
      IF sy-subrc <> 0
      OR ls_result_list-record_number <> ls_result_list_next-record_number.
        " Save the collected SH result in the GW request table
        APPEND ls_entityset TO et_entityset.
        CLEAR: ls_result_list_next, ls_entityset.
      ENDIF.
    ENDLOOP.
    SORT et_entityset BY werks arbpl.
    DELETE ADJACENT DUPLICATES FROM et_entityset COMPARING werks arbpl.
  ENDMETHOD.
ENDCLASS.
