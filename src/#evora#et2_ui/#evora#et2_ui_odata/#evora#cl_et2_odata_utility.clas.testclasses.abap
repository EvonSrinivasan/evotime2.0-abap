*"* use this source file for your ABAP unit test classes
CLASS ltc_utilities DEFINITION DEFERRED.
CLASS /evora/cl_et2_odata_utility DEFINITION LOCAL FRIENDS ltc_utilities.
CLASS ltc_utilities DEFINITION FOR TESTING DURATION MEDIUM RISK LEVEL HARMLESS.
  PUBLIC SECTION.

  PROTECTED SECTION.

  PRIVATE SECTION.

    "! test method for date and time conversion
    METHODS get_status_description FOR TESTING.

    "! test method to get name of user
    METHODS get_name_of_user FOR TESTING .

ENDCLASS.
CLASS ltc_utilities IMPLEMENTATION.

  METHOD get_status_description.
    DATA(lv_status_description) = /evora/cl_et2_odata_utility=>get_status_description(
                                     iv_status_profile = 'EVORA_ET'
                                     iv_status         = 'E0001'
                                   ).
    cl_abap_unit_assert=>assert_equals(
           act              = lv_status_description
           exp              = 'Mobile Booked'
           msg              = |Invalid status determination - Valid status check failed|
           level            = if_aunit_constants=>fatal
           quit             = if_aunit_constants=>method
       ).

  ENDMETHOD.

  METHOD get_name_of_user.

    cl_abap_unit_assert=>assert_not_initial(
        act              = /evora/cl_et2_odata_utility=>get_name_of_user( iv_username = sy-uname )
        msg              = |Name is not maintained in SU01|
        level            = if_aunit_constants=>fatal
        quit             = if_aunit_constants=>method
    ).

  ENDMETHOD.

ENDCLASS.
