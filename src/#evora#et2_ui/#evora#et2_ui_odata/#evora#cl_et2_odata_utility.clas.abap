class /EVORA/CL_ET2_ODATA_UTILITY definition
  public
  final
  create public .

public section.

  types:
    BEGIN OF tys_change_log,
        /evora/ea_e_guid TYPE /evora/et2_e_request_guid,
        cdhdr            TYPE cdhdr,
        cdpos_tab        TYPE cdpos_tab,
*      STANDARD TABLE OF cdpos WITH EMPTY KEY WITH NON-UNIQUE SORTED KEY obj_chgnr COMPONENTS objectclas objectid changenr,
        jcds_t           TYPE STANDARD TABLE OF jcds WITH EMPTY KEY WITH NON-UNIQUE SORTED KEY admin_fields COMPONENTS usnam udate utime inact,
      END OF tys_change_log .
  types:
    tyt_change_log TYPE STANDARD TABLE OF tys_change_log .
  types TYS_UI_CHANGE_LOG type /EVORA/ET2_S_UI_CHANGELOG_FLDS .
  types:
    tyt_ui_change_log TYPE STANDARD TABLE OF tys_ui_change_log .

  class-data GV_ENTITY_CALL type BOOLE_D .

  class-methods CONVERT_ODATA_2_INTERNAL_FLT
    importing
      !IT_FILTER_SELECT_OPTIONS type /IWBEP/T_MGW_SELECT_OPTION
    returning
      value(RT_FILTER_CONDITIONS) type /EVORA/EA_TT_FILTER_COND .
  class-methods PREPARE_CHANGE_LOG
    importing
      !IV_INTERNAL_COMBO_KEY type /EVORA/EA_E_INTERNAL_COMBO_KEY
      !IR_DATA type DATA
    exporting
      !ET_UI_CHANGE_LOG type TYT_UI_CHANGE_LOG .
  class-methods GET_STATUS_DESCRIPTION
    importing
      !IV_STATUS_PROFILE type J_STSMA
      !IV_STATUS type J_ESTAT
    returning
      value(RV_STATUS_DESCRIPTION) type J_TXT30 .
  class-methods GET_NAME_OF_USER
    importing
      !IV_USERNAME type CDUSERNAME
    returning
      value(RV_USERNAME) type /EVORA/ET2_E_USERNAME .
  class-methods GET_FIELD_LABEL_MEDIUM_TEXT
    importing
      !IV_FIELDNAME type DFIES-FIELDNAME
      !IV_TABNAME type DDOBJNAME
    returning
      value(RV_FIELD_LABEL) type SCRTEXT_M .
  class-methods CONVERT_ODATA_TO_EXT_FORMAT
    importing
      !IT_FILTER_CONDITIONS type /EVORA/EA_TT_FILTER_COND
    returning
      value(RT_FILTER_SELECT_OPTIONS) type /IWBEP/T_MGW_SELECT_OPTION .
  class-methods CONVERT_DATA_TO_FILTER_COND
    importing
      !IR_DATA type ref to DATA
      !IV_FIELDNAME type FIELDNAME
    returning
      value(RT_CONDITIONS) type /EVORA/EA_TT_FILTER_COND .
  class-methods CONVERT_DATA_TO_EXT_FORMAT
    importing
      !IR_DATA type ref to DATA
      !IV_FIELDNAME type FIELDNAME
    returning
      value(RT_FILTER_SELECT_OPTIONS) type /IWBEP/T_MGW_SELECT_OPTION .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS /EVORA/CL_ET2_ODATA_UTILITY IMPLEMENTATION.


  METHOD CONVERT_DATA_TO_EXT_FORMAT.

    FIELD-SYMBOLS: <lt_data> TYPE ANY TABLE.
    FIELD-SYMBOLS: <lt_filter> TYPE /IWBEP/T_COD_SELECT_OPTIONS. "/EVORA/EA_TT_FILTER_LINE.


    ASSIGN ir_data->* TO <lt_data>.

    IF <lt_data> IS NOT INITIAL.
      rt_filter_select_options = VALUE #( ( property = iv_fieldname ) ).
*      rt_conditions = VALUE #( ( technical_fieldname = iv_fieldname
*                                 fieldname           = iv_fieldname  ) ).
      ASSIGN rt_filter_select_options[ 1 ]-select_options to <lt_filter>.
      IF sy-subrc <> 0.
        RETURN.
      ENDIF.
    ENDIF.

    LOOP AT <lt_data> ASSIGNING FIELD-SYMBOL(<ls_data>).
      ASSIGN COMPONENT iv_fieldname OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<lv_filter_value>).
      INSERT INITIAL LINE INTO TABLE <lt_filter> ASSIGNING FIELD-SYMBOL(<ls_filter>).
*      APPEND INITIAL LINE TO <lt_filter> ASSIGNING FIELD-SYMBOL(<ls_filter>).
      <ls_filter>-sign = 'I'.
      <ls_filter>-option = 'EQ'.
      <ls_filter>-low  = <lv_filter_value>.
    ENDLOOP.

  ENDMETHOD.


  METHOD convert_data_to_filter_cond.

    FIELD-SYMBOLS: <lt_data> TYPE ANY TABLE.
    FIELD-SYMBOLS: <lt_filter> TYPE /EVORA/EA_TT_FILTER_LINE.


    ASSIGN ir_data->* TO <lt_data>.

    IF <lt_data> IS NOT INITIAL.
      rt_conditions = VALUE #( ( technical_fieldname = iv_fieldname
                                 fieldname           = iv_fieldname  ) ).
      ASSIGN rt_conditions[ 1 ]-filter to <lt_filter>.
      IF sy-subrc <> 0.
        RETURN.
      ENDIF.
    ENDIF.

    LOOP AT <lt_data> ASSIGNING FIELD-SYMBOL(<ls_data>).
      ASSIGN COMPONENT iv_fieldname OF STRUCTURE <ls_data> TO FIELD-SYMBOL(<lv_filter_value>).
      INSERT INITIAL LINE INTO TABLE <lt_filter> ASSIGNING FIELD-SYMBOL(<ls_filter>).
*      APPEND INITIAL LINE TO <lt_filter> ASSIGNING FIELD-SYMBOL(<ls_filter>).
      <ls_filter>-sign = 'I'.
      <ls_filter>-option = 'EQ'.
      <ls_filter>-low  = <lv_filter_value>.
    ENDLOOP.

  ENDMETHOD.


  METHOD convert_odata_2_internal_flt.
    CLEAR rt_filter_conditions.

    LOOP AT it_filter_select_options ASSIGNING FIELD-SYMBOL(<ls_filter_select_options>).
      READ TABLE rt_filter_conditions ASSIGNING FIELD-SYMBOL(<ls_condition>)
        WITH KEY fieldname = <ls_filter_select_options>-property    ##WARN_OK.
      IF sy-subrc <> 0.
        APPEND INITIAL LINE TO rt_filter_conditions ASSIGNING <ls_condition>.
        <ls_condition>-fieldname           = <ls_filter_select_options>-property.
        <ls_condition>-technical_fieldname = <ls_filter_select_options>-property.
      ENDIF.
      CLEAR <ls_condition>-filter.
      <ls_condition>-filter = <ls_filter_select_options>-select_options.
    ENDLOOP.

  ENDMETHOD.


  METHOD CONVERT_ODATA_TO_EXT_FORMAT.

    refresh rt_filter_select_options.

*    CLEAR rt_filter_conditions.
*
*    LOOP AT it_filter_select_options ASSIGNING FIELD-SYMBOL(<ls_filter_select_options>).
*      READ TABLE rt_filter_conditions ASSIGNING FIELD-SYMBOL(<ls_condition>)
*        WITH KEY fieldname = <ls_filter_select_options>-property    ##WARN_OK.
*      IF sy-subrc <> 0.
*        APPEND INITIAL LINE TO rt_filter_conditions ASSIGNING <ls_condition>.
*        <ls_condition>-fieldname           = <ls_filter_select_options>-property.
*        <ls_condition>-technical_fieldname = <ls_filter_select_options>-property.
*      ENDIF.
*      CLEAR <ls_condition>-filter.
*      <ls_condition>-filter = <ls_filter_select_options>-select_options.
*    ENDLOOP.

  ENDMETHOD.


  METHOD get_field_label_medium_text.

    /evora/cl_ea_utilities=>get_field_label_info(
      EXPORTING
        iv_fieldname     = iv_fieldname    " Field Name
        iv_tabname       = iv_tabname    " Name of ABAP Dictionary Object
      IMPORTING
        ev_field_label_m = rv_field_label    " Medium Field Label
    ).

  ENDMETHOD.


  METHOD get_name_of_user.

    DATA: ls_address TYPE bapiaddr3,
          lt_return  TYPE bapiret2_t.

    CALL FUNCTION 'BAPI_USER_GET_DETAIL'
      EXPORTING
        username = iv_username
      IMPORTING
        address  = ls_address
      TABLES
        return   = lt_return.

    rv_username =     |{ ls_address-firstname } { ls_address-lastname }|.

  ENDMETHOD.


  METHOD get_status_description.
    CHECK ( iv_status_profile IS NOT INITIAL ) AND ( iv_status IS NOT INITIAL ).
    SELECT txt30 FROM tj30t
      INTO rv_status_description
      WHERE stsma = iv_status_profile
      AND   estat = iv_status
      AND   spras = sy-langu.
    ENDSELECT.
  ENDMETHOD.


  METHOD prepare_change_log.

    DATA: lt_change_log    TYPE tyt_change_log,
          lt_ui_change_log TYPE tyt_ui_change_log.

    FIELD-SYMBOLS : <lt_data>  TYPE ANY TABLE.

    REFRESH: et_ui_change_log.

    "get Changelog descriptions
    /evora/cl_ea_service_facade=>get_ui_customizing( )->get_uichangelog_configuration(
      EXPORTING
        iv_internal_combo_key  = iv_internal_combo_key
      IMPORTING
        et_uichange_log_config = DATA(lt_ui_changelog_config)
     ).

    "get status descriptions
    DATA(lt_status_description) = /evora/cl_ea_utilities=>get_estatus_code_descriptions(
                                      "get Status profile
                                      iv_status_profile = /evora/cl_ea_service_facade=>get_customizing( )->get_status_profile(
                                          iv_internal_combo_key = iv_internal_combo_key )
                                     ).
    ASSIGN ir_data->* TO <lt_data>.
    IF sy-subrc = 0.
      IF <lt_data> IS NOT INITIAL.

        GET RUN TIME FIELD DATA(lv_ts1).
        MOVE-CORRESPONDING <lt_data> TO lt_change_log .

        DATA(lv_status_label) = get_field_label_medium_text(
                                                               iv_fieldname   = 'STAT'
                                                               iv_tabname     = 'JCDS'
                                                           ).
        DATA(lt_all_jcds) = lt_change_log[ 1 ]-jcds_t.
        et_ui_change_log = VALUE #( FOR <ls_change_log1> IN lt_change_log INDEX INTO lv_header_tabix
                                    LET lt_ref_cdpos1 = COND #( WHEN lv_header_tabix > 1
                                                                THEN lt_change_log[ lv_header_tabix - 1 ]-cdpos_tab
                                                              )

                                        lv_temp_newvalue = REDUCE #( INIT lv_result TYPE string
                                                            FOR <ls_cdpos1> IN <ls_change_log1>-cdpos_tab
                                                            WHERE ( tabname NE /evora/if_et2_constants=>cv_change_log )

                                                            LET
                                                              lv_no_change = COND #( WHEN line_exists( lt_ref_cdpos1[ fname = <ls_cdpos1>-fname
                                                                                                                  value_new = <ls_cdpos1>-value_new ] )
                                                                                     THEN abap_true
                                                                                     ELSE abap_false )

                                                              lv_temp_result = COND #( WHEN lv_no_change = abap_false
                                                                                        AND line_exists( lt_ui_changelog_config[ fieldname = <ls_cdpos1>-fname
                                                                                                                         hide_in_changelog = abap_false ] )
                                                                                       THEN
                                                                                       |{
                                                                                            get_field_label_medium_text(
                                                                                                iv_fieldname   = <ls_cdpos1>-fname
                                                                                                iv_tabname     = <ls_cdpos1>-tabname
                                                                                            ) } {
                                                                                            /evora/if_et2_constants=>cv_field_value_sparator } {
                                                                                            SWITCH #( <ls_cdpos1>-fname
                                                                                                  WHEN 'START_DATE' THEN /evora/cl_ea_utilities=>convert_date_to_user_format(
                                                                                                                             iv_date = CONV d( <ls_cdpos1>-value_new ) )
                                                                                                  WHEN 'END_DATE'   THEN /evora/cl_ea_utilities=>convert_date_to_user_format(
                                                                                                                             iv_date = CONV d( <ls_cdpos1>-value_new ) )
                                                                                                  WHEN 'START_TIME' THEN /evora/cl_ea_utilities=>convert_time_to_user_format(
                                                                                                                             iv_time = CONV t( <ls_cdpos1>-value_new ) )
                                                                                                  WHEN 'END_TIME'   THEN /evora/cl_ea_utilities=>convert_time_to_user_format(
                                                                                                                             iv_time = CONV t( <ls_cdpos1>-value_new ) )
                                                                                                  ELSE <ls_cdpos1>-value_new
                                                                                             ) }|
*                                                                                             no need to display
*                                                                                       ELSE lv_temp_result
                                                                                       )

                                                              IN

                                                              NEXT
                                                              lv_result = COND #( WHEN lv_no_change = abap_false
                                                                                  THEN COND #( WHEN lv_result IS NOT INITIAL
                                                                                               THEN |{
                                                                                                      lv_result } {
                                                                                                      cl_abap_char_utilities=>newline } {
                                                                                                      lv_temp_result
                                                                                                    }|
                                                                                               ELSE lv_temp_result )
                                                                                  ELSE lv_result
                                                                                )
                                                           )
                                                               IN
                                    (
                                    newvalue =  COND #( WHEN line_exists( lt_all_jcds[ KEY admin_fields COMPONENTS usnam = <ls_change_log1>-cdhdr-username
                                                                                                                   udate = <ls_change_log1>-cdhdr-udate
                                                                                                                   utime = <ls_change_log1>-cdhdr-utime
                                                                                                                   inact = abap_false ] )
                                                        THEN |{ lv_temp_newvalue } {
                                                                cl_abap_char_utilities=>newline } {
                                                                lv_status_label } {
                                                                /evora/if_et2_constants=>cv_field_value_sparator } {
                                                                  lt_status_description[ estat =
                                                                      <ls_change_log1>-jcds_t[ KEY admin_fields COMPONENTS usnam = <ls_change_log1>-cdhdr-username
                                                                                                                           udate = <ls_change_log1>-cdhdr-udate
                                                                                                                           utime = <ls_change_log1>-cdhdr-utime
                                                                                                                           inact = abap_false ]-stat ]-txt30 } |
                                                        ELSE lv_temp_newvalue )
                                    username = get_name_of_user( iv_username = <ls_change_log1>-cdhdr-username )
                                    changeid         = |{ <ls_change_log1>-cdhdr-objectid } { <ls_change_log1>-cdhdr-changenr }|
                                    change_indicator = <ls_change_log1>-cdhdr-change_ind
                                    change_object    = <ls_change_log1>-cdhdr-objectclas
                                    request_guid     = <ls_change_log1>-cdhdr-objectid
                                    logdate          = <ls_change_log1>-cdhdr-udate
                                    logtime          = <ls_change_log1>-cdhdr-utime
                                    change_datetime = /evora/cl_ea_utilities=>convert_date_time_to_timestamp(
                                                          iv_date      = <ls_change_log1>-cdhdr-udate
                                                          iv_time      = <ls_change_log1>-cdhdr-utime
                                                          iv_timezone  = sy-zonlo
                                                      )

                  )
                  ( newvalue = space ) "Extra line
               ).

        lt_ui_change_log = VALUE #( LET ls_cdhdr = lt_change_log[ 1 ]-cdhdr
        IN
                                    FOR <ls_jcds> IN lt_all_jcds INDEX INTO lv_header_index
                                          WHERE ( inact = abap_false )
                                          (
                                            newvalue = COND #( WHEN line_exists( lt_change_log[ cdhdr-username = <ls_jcds>-usnam
                                                                                                cdhdr-udate    = <ls_jcds>-udate
                                                                                                cdhdr-utime    = <ls_jcds>-utime ] )
                                                                THEN abap_false
                                                                ELSE |{
                                                                       lv_status_label } {
                                                                       /evora/if_et2_constants=>cv_field_value_sparator } {
                                                                       lt_status_description[ estat = <ls_jcds>-stat ]-txt30 }| )
                                             change_indicator = COND #( WHEN lv_header_index > 1
                                                                        THEN 'U'
                                                                        ELSE 'I' )
                                             username = get_name_of_user( iv_username = <ls_jcds>-usnam )
                                             request_guid  = ls_cdhdr-objectid
                                             changeid      = |{ ls_cdhdr-objectid } { <ls_jcds>-stat }|
                                             logdate       = <ls_jcds>-udate
                                             logtime       = <ls_jcds>-utime
                                             change_datetime = /evora/cl_ea_utilities=>convert_date_time_to_timestamp(
                                                             iv_date      = <ls_jcds>-udate
                                                             iv_time      = <ls_jcds>-utime
                                                             iv_timezone  = sy-zonlo
                                                         )
                                           ) ).

        APPEND LINES OF lt_ui_change_log TO et_ui_change_log.
        DELETE et_ui_change_log WHERE newvalue IS INITIAL.
        GET RUN TIME FIELD DATA(lv_ts2).
*        code backup taken for old convention
      ENDIF.
    ENDIF.

    GET RUN TIME FIELD DATA(lv_ts3).

*    DELETE et_ui_change_log  WHERE newvalue = space.
    SORT et_ui_change_log BY changeid.
    DELETE ADJACENT DUPLICATES FROM et_ui_change_log COMPARING changeid.
    SORT et_ui_change_log BY logdate logtime.
    DELETE ADJACENT DUPLICATES FROM et_ui_change_log COMPARING logdate logtime.

  ENDMETHOD.
ENDCLASS.
