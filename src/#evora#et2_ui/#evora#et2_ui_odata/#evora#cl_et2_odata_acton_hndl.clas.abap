class /EVORA/CL_ET2_ODATA_ACTON_HNDL definition
  public
  final
  create public .

public section.

  types:
    BEGIN OF tys_input,
      request_guid TYPE /evora/et2_e_request_guid,
      function     TYPE  /evora/ea_e_functions,
    END OF tys_input .

  methods GET_SYSTEM_INFORMATION
    exporting
      !ES_SYSTEM_INFORMATION type /EVORA/CL_ET2_MAIN_MPC_EXT=>TS_SYSTEMINFORMATION .
  methods EXECUTE_STATUS_FUNCTION
    importing
      !IT_PARAMETER type /IWBEP/T_MGW_NAME_VALUE_PAIR
    exporting
      !ES_APPROVAL type /EVORA/CL_ET2_MAIN_MPC=>TS_APPROVAL .
  methods MAP_PARAMETER
    importing
      !IT_PARAMETER type /IWBEP/T_MGW_NAME_VALUE_PAIR
    returning
      value(RS_INPUT) type TYS_INPUT .
  methods DELETE_TIME_APPROVAL_RECORD
    importing
      !IT_PARAMETER type /IWBEP/T_MGW_NAME_VALUE_PAIR
    exporting
      !ES_APPROVAL type /EVORA/CL_ET2_MAIN_MPC=>TS_APPROVAL .
protected section.
private section.

  constants:
    BEGIN OF CS_INPUT_PARAMETERS,
    request_guid TYPE STRING VALUE 'RequestGuid',
    Function     TYPE STRING VALUE 'Function',
   END OF CS_INPUT_PARAMETERS .
ENDCLASS.



CLASS /EVORA/CL_ET2_ODATA_ACTON_HNDL IMPLEMENTATION.


  METHOD delete_time_approval_record.
    DATA lr_data              TYPE REF TO data.
    DATA lv_message           TYPE bapi_msg.

*   map the parameter from the function import into a structure
    DATA(ls_input_data) = map_parameter( it_parameter ).
    TRY.
*   Create the line type of approval
        DATA(lv_internal_combo_key) = /evora/cl_ea_service_facade=>get_ready_to_run(
                                                  iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                                  iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                                  ).
        DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( iv_internal_combo_key = lv_internal_combo_key ).
        CREATE DATA lr_data TYPE HANDLE lo_line_type.
        ASSIGN lr_data->* TO FIELD-SYMBOL(<ls_approval_details>).

*   move the data over
        MOVE-CORRESPONDING ls_input_data TO <ls_approval_details>.

*   ensure clean state for the result

        CLEAR es_approval.

        DATA(lo_data_buffer) = /evora/cl_ea_service_facade=>get_data_buffer( iv_internal_combo_key = lv_internal_combo_key ).
        DATA(lo_time_approval_object) = CAST /evora/if_ea_object( lo_data_buffer->get_by_key( iv_object_key = ls_input_data-request_guid ) ).

        IF lo_time_approval_object IS BOUND.
          DATA(lv_request_description) = lo_time_approval_object->get_description( ).
          DATA(lv_request_id) = lo_time_approval_object->get_request_id( ).
          DATA(ls_objkey_info) = lo_time_approval_object->delete( ).
          IF ls_objkey_info-operation_success = abap_false.
            MESSAGE e002(/evora/ea_msg_cls)
            WITH lv_request_id
            INTO lv_message.
            /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_sy_msg( ).
          ELSE.
            MESSAGE s003(/evora/ea_msg_cls)
             WITH lv_request_id
                  lv_request_description
             INTO lv_message.
            /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_sy_msg( ).
          ENDIF.
        ENDIF.
      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex).
        /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_exception( lo_ex ).
    ENDTRY.


  ENDMETHOD.


  METHOD execute_status_function.
    DATA lr_data              TYPE REF TO data.
    DATA lv_message           TYPE bapi_msg.

*   map the parameter from the function import into a structure
    TRY .
        DATA(ls_input_data) = map_parameter( it_parameter ).
        DATA(lv_internal_combo_key) = /evora/cl_ea_service_facade=>get_ready_to_run(
                                             iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                             iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                             ).

        DATA(lo_line_type) = /evora/cl_ea_rtts_service=>get_line_type( iv_internal_combo_key = lv_internal_combo_key ).
        CREATE DATA lr_data TYPE HANDLE lo_line_type.
        ASSIGN lr_data->* TO FIELD-SYMBOL(<ls_data>).



        DATA(lo_time_approval_object) = /evora/cl_ea_service_facade=>get_data_buffer(
        iv_internal_combo_key = lv_internal_combo_key )->get_by_key( iv_object_key = ls_input_data-request_guid ).

        DATA(lv_success) = lo_time_approval_object->execute_function(
                           iv_function = ls_input_data-function    " EvoTime: Functions for setting status
                       ).
        IF lv_success = abap_true.
          lo_time_approval_object->get_details(
            IMPORTING
              es_data                    = <ls_data>
          ).
          DATA(lv_approval_description) = lo_time_approval_object->get_description( ).
          DATA(lv_function_name) =
            /evora/cl_ea_service_facade=>get_customizing( )->get_status_function_name(
              iv_internal_combo_key = lv_internal_combo_key
              iv_function = ls_input_data-function
          ).
          MESSAGE s001(/evora/ea_msg_cls) WITH lv_approval_description lv_function_name INTO lv_message.
          /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_sy_msg( ).


*         Get Odata handler based on the customizing
          DATA(lo_odata_handler) = /evora/cl_ea_service_facade=>get_ui_customizing( )->get_odata_handler(
              iv_internal_combo_key = lv_internal_combo_key
          ).

*         Move the backend data to frontend data
          lo_odata_handler->map_to_odata_entity(
            EXPORTING
              iv_internal_combo_key = lv_internal_combo_key
              is_data               = <ls_data>    " Backend data
            IMPORTING
              es_entity             = es_approval    " Frontend data
          ).
        ELSE.
*       ensure all is reset
          /evora/cx_ea_api_exception=>raise(
            is_textid = /evora/cx_ea_api_exception=>cs_object_function_not_avail
            iv_var1   = |{ lv_internal_combo_key }|
            iv_var2   = |{ lo_time_approval_object->get_description( ) }|
          ).

        ENDIF.

      CATCH /evora/cx_ea_api_exception INTO DATA(lo_ex).
        /evora/cl_ea_service_facade=>get_message_buffer( )->add_message_from_exception( io_exception = lo_ex ).
        RETURN.
    ENDTRY.
  ENDMETHOD.


  METHOD get_system_information.

    DATA: lt_return  TYPE bapiret2_t,
          ls_address TYPE bapiaddr3,
          lv_uname   TYPE bapibname-bapibname.

    lv_uname = sy-uname.
    CALL FUNCTION 'BAPI_USER_GET_DETAIL'
      EXPORTING
        username = lv_uname
      IMPORTING
        address  = ls_address
      TABLES
        return   = lt_return.

    es_system_information = CORRESPONDING #( ls_address ).
    es_system_information-username = lv_uname.

*   system id and client information
    es_system_information-systemid = sy-sysid.
    es_system_information-client   = sy-mandt.

*   derive the core version from the static constant
    es_system_information-application_core_version = /evora/if_et2_constants=>cv_et2_version.

*
TRY.
    es_system_information-enable_delete = /evora/cl_ea_service_facade=>get_authority_check(
         /evora/cl_ea_service_facade=>get_ready_to_run(
                                          iv_business_scenario = /evora/if_et2_constants=>cv_business_scenario
                                          iv_object_type       = /evora/if_et2_constants=>cs_object_types-t_confirm
                                          ) )->check_for_delete( ) .
  CATCH /evora/cx_ea_api_exception.
ENDTRY.

*    AUTHORITY-CHECK OBJECT '/EVORA/ET2' ID 'ACTVT' FIELD '06'.
*    IF sy-subrc = 0.
*      es_system_information-enable_delete = abap_false.
*    ENDIF.

*    Get server path
    cl_wd_utilities=>construct_wd_url(
      EXPORTING
        application_name              = /evora/if_ea_constants=>cv_wd_url_application_name    " Application
      IMPORTING
        out_host                      = DATA(lv_host)    " Host Name
        out_port                      = DATA(lv_port)    " Port Number
    ).
    es_system_information-server_path = |{ lv_host }:{ lv_port }|.

  ENDMETHOD.


  METHOD map_parameter.
    READ TABLE it_parameter ASSIGNING FIELD-SYMBOL(<ls_parameter>) WITH KEY name = cs_input_parameters-request_guid.
    IF sy-subrc = 0.
      rs_input-request_guid = <ls_parameter>-value.
    ENDIF.

    READ TABLE it_parameter ASSIGNING <ls_parameter> WITH KEY name = cs_input_parameters-function.
    IF sy-subrc = 0.
      rs_input-function = <ls_parameter>-value.
    ENDIF.
  ENDMETHOD.
ENDCLASS.
